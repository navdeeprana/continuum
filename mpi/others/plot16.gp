set term qt noraise
set size ratio 1
set pm3d map
load '~/bin/custom.pal'
set xrange [0:400]
set yrange [0:400]
set cbrange [0:1]
unset colorbox
do for [i=0:1000:1]{
sp	'data/0/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/1/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/2/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/3/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/4/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/5/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/6/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/7/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/8/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/9/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/10/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/11/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/12/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/13/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/14/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		'data/15/snap.'.i u 1:2:($3/$4) w pm3d t '' 
	pause 0.5
}
