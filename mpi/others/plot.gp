set term qt noraise
load '~/bin/custom.pal'
set size ratio 1
set pm3d map
set xrange [0:400]
set yrange [0:400]
set cbrange [0:1]
unset colorbox
do for [i=0:1000:1]{
	sp 'data/0/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		 'data/1/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		 'data/2/snap.'.i u 1:2:($3/$4) w pm3d t '' ,\
		 'data/3/snap.'.i u 1:2:($3/$4) w pm3d t '' 
	pause 0.5
}
