#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <mpi.h>
#define NPROCX (4)
#define NPROCY (1)
#define NPROC  (NPROCX*NPROCY)

#define NTX (100) // total grid points in x
#define NTY (400) // total grid points in y
#define NT (NTX*NTY)

#define NGX (NTX/NPROCX) // total x-grid points per thread
#define NGY (NTY/NPROCY)
#define NG  (NGX*NGY)

#define LENX (1.e0)
#define LENY (1.e0)
#define DX (LENX/NTX)
#define DY (LENY/NTY)
#define INFOO (1)
#include"src/define.c"
#include"src/mpisetup.c"
  void
linear_innoculation (int me,uint ngrid[2],uint pad[2],double *rho)
{
  int i, j, idx;
  for (j = 0; j < NGY; j++) {
    for (i = pad[0]; i < NGX+pad[0]; i++) {
      idx = i + j*ngrid[0];
      rho[idx] = me+1;
    }   
  }
}

	int
main (int argc, char **argv)
{
	int NP, me;
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);
	if(NPROCX != NP){
		perror("Put right number of cores, mf.\n");
		exit(1);
	}
	uint ngrid[2],pad[2],origin[2];
	set_grid(NP,me,ngrid,pad);
	calculate_origin(me,origin);
	uint ngtotal = ngrid[0]*ngrid[1];
 	printf("%d\t%d\t%d\t%d\n",me,ngrid[0],ngrid[1],ngtotal);	
	double *rho = calloc (ngtotal, sizeof (double));
	double *leftpad = calloc(NGY, sizeof(double));
	double *rightpad = calloc(NGY, sizeof(double));

	linear_innoculation(me,ngrid,pad,rho);
	char name[32];
	sprintf(name,"snap.%d",me);
	FILE *fid = fopen(name,"wb");
	uint i,j,truei,idx;
	for(j=0;j<NGY;j++){
		for(i=pad[0];i<ngrid[0]-pad[1];i++){
			idx = j*ngrid[0] + i;
			truei = i-pad[0]+origin[0]*NGX;
			
			fprintf(fid,"%d\t%d\t%g\n",truei,j,rho[idx]);
		}
		fprintf(fid,"\n");
	}
	fprintf(fid,"\n\n");
	//fwrite(rho,sizeof(double),ngtotal,fid);
	synchronize_state(NP,me,ngrid,pad,rho,leftpad,rightpad);
	for(i=0;i<ngrid[0];i++){
		fprintf(fid,"%d\t\t",i);
		for(j=0;j<ngrid[1];j++){
			idx = j*ngrid[0] + i;
			fprintf(fid,"%1.4f\t",rho[idx]);
		}
		fprintf(fid,"\n",i);
	}
	fwrite(rho,sizeof(double),ngtotal,fid);
	fflush(fid);
	fclose(fid);
	MPI_Finalize();
	return 0;
}
