#define NONLINEAR_DIFFUSIVITY

#include"parameters.h"
#include"src/define.c"
#include"src/mpisetup.c"
#include"src/advance.c"
#include"src/data.c"
#include"src/derivatives.c"
#include"src/diagnostics.c"
#include"src/housekeeping.c"
#include"src/stochastic.c"

	int
main (int argc, char **argv)
{
#ifndef MPI
	perror ("Not compiled using MPI. Aborting.\n");
	exit (1);
#endif
	int NP, me;
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);

	/* GSL Initialization */

	srand (time (NULL) + me);
	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	gsl_rng_set (r1, rand ());
	gsl_rng_set (r2, rand ());

	uint i, j, idx, fr;
	char folder[32];
	FILE **fin = malloc (sizeof (FILE *) * 4);
	housekeeping (me, fin, folder);
	double dt, cbx, cby, cfx, cfy;
	calculate_time_step (&cbx, &cby, &cfx, &cfy, &dt, &fr);
	fprintf (fin[0], "DBAC = %g INFOO=%g\tTIME STEP=%g\tFR=%d\n", DBAC,
			(double) INFOO, dt, fr);
	if (me == 0) {
		printf ("DBAC = %g INFOO=%g\tTIME STEP=%g\tFR=%d\n", DBAC, (double) INFOO,
				dt, fr);
	}
	uint2 ngrid, pad;
	uint ngtotal = set_grid (NP, me, ngrid, pad);
	uint2 origin;
	calculate_origin (me, origin);
	uint2 range = { ngrid[0] - 1, ngrid[1] - 1 };
	fprintf (fin[0], "Rank = %d\tngx = %d\t ngy=%d\n", me, ngrid[0], ngrid[1]);

	double *rho = calloc (ngtotal, sizeof (double));
	double *drho = calloc (ngtotal, sizeof (double));
	double *bac = calloc (ngtotal, sizeof (double));
	double *dbac = calloc (ngtotal, sizeof (double));
	double *food = calloc (ngtotal, sizeof (double));
	double *dfood = calloc (ngtotal, sizeof (double));
	double *leftpad = calloc (ngrid[1], sizeof (double));
	double *rightpad = calloc (ngrid[1], sizeof (double));

	double *localheight = calloc (NTX, sizeof (double));
	double *height = calloc (NTX, sizeof (double));
#ifdef NONLINEAR_DIFFUSIVITY
	double *gbacx = calloc (ngtotal, sizeof (double));
	double *gbacy = calloc (ngtotal, sizeof (double));
	double *gfoox = calloc (ngtotal, sizeof (double));
	double *gfooy = calloc (ngtotal, sizeof (double));
	double *dbacp = calloc (ngtotal, sizeof (double));
	double *drhop = calloc (ngtotal, sizeof (double));
	uint flag=0;
#endif

	linear_innoculation (ngrid, pad, rho, bac, food);
	save_snap (ngrid, pad, origin, folder,0, bac, rho);
	double lambda = 2.0e0 / (SIGMA * SIGMA * dt);
	double2 local = { 0, 0 }, global = { 0, 0 };	// stores carrying capacity and food concentration.
	car_cap (ngrid, pad, bac, food, rho, local);
	MPI_Reduce (local, global, 2, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	uint itime = 0;
	fprintf (fin[0], "sim_time\t\tfood left\t\trun_time\n");
	double start = MPI_Wtime();
	while (itime * dt <= SIMTIME) {
		if (itime % (fr) == 0) {
			car_cap (ngrid, pad, bac, food, rho, local);
			MPI_Reduce (local, global, 2, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

			find_height (me, ngrid, pad, bac, localheight);
			MPI_Reduce (localheight, height, NTX, MPI_DOUBLE, MPI_SUM, 0,
					MPI_COMM_WORLD);
			if (me == 0) {
				global[0] /= NP;
				global[1] /= NP;
				fprintf (fin[1], "%g\t%g\t%g\t%g\n", dt * itime, global[0], global[1],
						statistics(height));
				fflush (fin[1]);
			}
			
			if (me == 0 && (global[0] > 0.99 || global[1] < 0.01)) {
				fprintf (fin[0], "Simulation Box full.\n");
				MPI_Abort (MPI_COMM_WORLD, 1);
				break;
			}
			fprintf (fin[0], "%g\t\t%2.3f\t\t%g\n", itime * dt, local[1],MPI_Wtime()-start);
			fflush (fin[0]);
		}
		if (itime % (fr * 4) == 0) {
			//save_snap_binary (ngtotal,folder,bac, rho);
			save_snap (ngrid, pad, origin, folder,itime*dt, bac, rho);
		}
		if (itime % (fr * 20) == 0) {
			save_snap_binary_restart (ngtotal,folder,bac, rho);
		}
		stochastic (ngrid, r1, r2, lambda, bac, rho);
		synchronize_state_sendrecv (NP, me, ngrid, pad, bac, leftpad, rightpad);
		synchronize_state_sendrecv (NP, me, ngrid, pad, rho, leftpad, rightpad);
		for (i = 0; i < ngrid[0]; i++) {
			for (j = 0; j < ngrid[1]; j++) {
				idx = INDEX (j, i, ngrid[1]);
				food[idx] = rho[idx] - bac[idx];
			}
		}
		laplacian (ngrid, cfx, cfy, food, dfood);
		laplacian (ngrid, cbx, cby, bac, dbac);
#ifdef NONLINEAR_DIFFUSIVITY
		gradient (ngrid, bac, gbacx, gbacy);
		gradient (ngrid, food, gfoox, gfooy);
		/*div(D*C*grad(B)) */
		for (i = 0; i < ngrid[0]; i++) {
			for (j = 0; j < ngrid[1]; j++) {
				idx = INDEX (j, i, ngrid[1]);
				dbac[idx] =
					food[idx] * dbac[idx] + DBAC * (gbacx[idx] * gfoox[idx] +
							gbacy[idx] * gfooy[idx]);
			}
		}
#endif
		for (i = 1; i < range[0]; i++) {
			for (j = 1; j < range[1]; j++) {
				idx = INDEX (j, i, ngrid[1]);
				drho[idx] = dbac[idx] + dfood[idx];
				dbac[idx] += GAMMA * bac[idx] * (rho[idx] - bac[idx]);
			}
		}
#ifdef NONLINEAR_DIFFUSIVITY
		adam_bashford_update (ngrid, dt, bac, dbac, dbacp, flag);
		adam_bashford_update (ngrid, dt, rho, drho, drhop, flag);
		flag = 1;
#else
		euler_update (ngrid, dt, bac, dbac);
		euler_update (ngrid, dt, rho, drho);
#endif
		set_xboundary (ngrid, bac, 1);
		set_xboundary (ngrid, rho, 1);
		if (me == 0) {
			set_left_boundary (ngrid, bac, 1);
			set_left_boundary (ngrid, rho, 1);
		}
		if (me == NP - 1) {
			set_right_boundary (ngrid, bac, 1);
			set_right_boundary (ngrid, rho, 1);
		}
		itime++;
	}
	free (rho);
	free (drho);
	free (food);
	free (dfood);
	free (bac);
	free (dbac);
	fcloseall ();
	printf ("EOS. Clean exit on rank %d\n.", me);
#ifdef MPI
	MPI_Finalize ();
#endif
	return 0;
}
