#ifndef __parameters_h__
#define __parameters_h__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <mpi.h>
#define NPROCX (4)
#define NPROCY (1)
#define NPROC  (NPROCX*NPROCY)

#define NTX (200) // total grid points in x
#define NTY (200) // total grid points in y
#define NT (NTX*NTY)

#define NGX (NTX/NPROCX) // total x-grid points per thread
#define NGY (NTY/NPROCY)
#define NG  (NGX*NGY)

#define LENX (8.e0)
#define LENY (8.e0)
#define DX (LENX/NTX)
#define DY (LENY/NTY)

#define SIMTIME (1.e4)

#define DFOO  (1.e-1)
#define DBAC  (5.e-4)

#define GAMMA (1.e0)
#define SIGMA (5.e-2)

#define DTFAC (1.e-1)
#define INTERVEL (1)

#define EPSILON (1.e-4)
#define SKIP (1)
#ifndef VID
#define VID 0
#endif

#endif
