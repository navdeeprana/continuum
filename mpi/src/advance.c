void
set_xboundary (uint2 ngrid,double *array, uint flag)
{
	uint i, idx;
	if (flag==0) {
		for (i = ngrid[0]; i--;) {
			idx = i*ngrid[1];
			array[idx] = ZERO;
			idx = (i+1)*ngrid[1]-1;
			array[idx] = ZERO;
		}
	}
	else {
		for (i = ngrid[0]; i--;) {
			idx = i*ngrid[1];
			array[idx] = array[idx+1];
			idx = (i+1)*ngrid[1]-1;
			array[idx] = array[idx-1];
		}
	}
}
	void
set_left_boundary (uint2 ngrid,double *array, uint flag)
{
	uint i;
	if(flag==0){
		for (i =0; i<ngrid[1];i++) {
			array[i] = ZERO;
		}
	}
	else{
		for (i =0; i<ngrid[1];i++) {
			array[i] = array[i + ngrid[1]];
		}
	}
}
	void
set_right_boundary (uint2 ngrid,double *array, uint flag)
{
	uint i,idx;
	uint ngtotal = ngrid[0]*ngrid[1];
	if(flag==0){
		for (i =0; i<ngrid[1];i++) {
			idx = ngtotal - 1 - i;
			array[idx] = ZERO;
		}
	}
	else{
		for (i =0; i<ngrid[1];i++) {
			idx = ngtotal - 1 - i;
			array[idx] = array[idx-ngrid[1]];
		}
	}
}

	void
euler_update (uint2 ngrid,double dt, double *rho, double *drho)
{
	uint i, j, idx;
	for (i = 1;i<ngrid[0];i++) {
		for (j = 1; j<ngrid[1];j++) {
			idx = INDEX (j, i,ngrid[1]);
			rho[idx] += dt * drho[idx];
		}
	}
}

	void
adam_bashford_update (uint2 ngrid,double dt, double *rho, double *drho, double *drhop,uint flag)
{
	uint i, j, idx;
	if (flag==0){
		for (i = 1;i<ngrid[0];i++) {
			for (j = 1; j<ngrid[1];j++) {
				idx = INDEX (j, i,ngrid[1]);
				rho[idx] += dt * drho[idx];
				drhop[idx] = drho[idx];
			}
		}
	}
	else{
		for (i = 1;i<ngrid[0];i++) {
			for (j = 1; j<ngrid[1];j++) {
				idx = INDEX (j, i,ngrid[1]);
				rho[idx] += 0.5e0*dt * (3.e0*drho[idx]-drhop[idx]);
				drhop[idx] = drho[idx];
			}
		}
	}
}

