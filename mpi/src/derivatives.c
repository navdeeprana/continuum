void
gradient (uint2 ngrid,double *rho, double *gradx, double *grady)
{
	uint i, j, idx;
	for (i = 1;i<ngrid[0];i++) {
		for (j = 1; j<ngrid[1];j++) {
			idx = INDEX (j, i,ngrid[1]);
			gradx[idx] = (rho[idx + ngrid[1]] - rho[idx - ngrid[1]]) / (2.e0 * DX);
			grady[idx] = (rho[idx + 1] - rho[idx - 1]) / (2.e0 * DY);
		}
	}
}

	void
divergence (uint2 ngrid,double *rhox, double *rhoy, double *div)
{
	uint i, j, idx;
	for (i = 1;i<ngrid[0];i++) {
		for (j = 1; j<ngrid[1];j++) {
			idx = INDEX (j, i,ngrid[1]);
			div[idx] = (rhox[idx + ngrid[1]] - rhox[idx - ngrid[1]]) / (2.e0 * DX)
				+ (rhoy[idx + 1] - rhoy[idx - 1]) / (2.e0 * DY);
		}
	}
}

	void
laplacian (uint2 ngrid,double constx, double consty, double *rho, double *diff)
{
	uint i, j, idx;
	for (i = 1;i<ngrid[0];i++) {
		for (j = 1; j<ngrid[1];j++) {
			idx = INDEX (j, i,ngrid[1]);
			diff[idx] =
				constx * (rho[idx + ngrid[1]] + rho[idx - ngrid[1]] - 2.e0 * rho[idx]) +
				consty * (rho[idx + 1] + rho[idx - 1] - 2.e0 * rho[idx]);
		}
	}
}
	void
gradient_isotropic (uint2 ngrid,double *rho, double *gradx, double *grady)
{
	uint i, j, idx;
	uint n1, n2, n3, n4;
	uint2 range = {ngrid[0]-1,ngrid[1]-1};

	for (i = 1;i<range[0];i++) {
		for (j = 1; j<range[1];j++) {
			idx = INDEX (j, i,ngrid[1]);
			n1 = INDEX (j + 1,i + 1, ngrid[1]);
			n2 = INDEX (j + 1,i - 1, ngrid[1]);
			n3 = INDEX (j - 1,i - 1, ngrid[1]);
			n4 = INDEX (j - 1,i + 1, ngrid[1]);
			gradx[idx] =	(4.e0 * (rho[idx + 1] - rho[idx - 1]) +
					(rho[n1] - rho[n2] - rho[n3] + rho[n4])) / (12.e0 * DX);
			grady[idx] =	(4.e0 * (rho[idx + NGX] - rho[idx - NGX]) +
					(rho[n1] + rho[n2] - rho[n3] - rho[n4])) / (12.e0 * DY);
		}
	}
}
	void
divergence_isotropic (uint2 ngrid,double *rhox, double *rhoy, double *div)
{
	uint i, j, idx;
	uint n1, n2, n3, n4;
	uint2 range = {ngrid[0]-1,ngrid[1]-1};

	for (i = 1;i<range[0];i++) {
		for (j = 1; j<range[1];j++) {
			idx = INDEX (j, i,ngrid[1]);
			n1 = INDEX (j + 1,i + 1, ngrid[1]);
			n2 = INDEX (j + 1,i - 1, ngrid[1]);
			n3 = INDEX (j - 1,i - 1, ngrid[1]);
			n4 = INDEX (j - 1,i + 1, ngrid[1]);
			div[idx] =
				(4.e0 * (rhox[idx + 1] - rhox[idx - 1]) +
				 (rhox[n1] - rhox[n2] - rhox[n3] + rhox[n4])) / (12.e0 * DX)
				+ (4.e0 * (rhoy[idx + NGX] - rhoy[idx - NGX]) +
						(rhoy[n1] + rhoy[n2] - rhoy[n3] - rhoy[n4])) / (12.e0 * DY);
		}
	}
}
	void
laplacian_isotropic (uint2 ngrid,double constx, double consty, double *rho, double *diff)
{
	uint i, j, idx;
	uint n1, n2, n3, n4;
	uint2 range = {ngrid[0]-1,ngrid[1]-1};

	for (i = 1;i<range[0];i++) {
		for (j = 1; j<range[1];j++) {
			idx = INDEX (j, i,ngrid[1]);
			n1 = INDEX (j + 1,i + 1, ngrid[1]);
			n2 = INDEX (j + 1,i - 1, ngrid[1]);
			n3 = INDEX (j - 1,i - 1, ngrid[1]);
			n4 = INDEX (j - 1,i + 1, ngrid[1]);
			diff[idx] =
				(constx / 12.e0) * (10.e0 *
						(rho[idx + 1] + rho[idx - 1] - 2.e0 * rho[idx]) +
						(rho[n1] + rho[n2] + rho[n3] + rho[n4] -
						 2.e0 * rho[idx + NGX] - 2.e0 * rho[idx - NGX]));
			diff[idx] +=
				(consty / 12.e0) * (10.e0 *
						(rho[idx + NGX] + rho[idx - NGX] -
						 2.e0 * rho[idx]) + (rho[n1] + rho[n2] + rho[n3] +
							 rho[n4] - 2.e0 * rho[idx +
							 1] -
							 2.e0 * rho[idx - 1]));
		}
	}
}
