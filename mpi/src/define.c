#define ZERO (0.e0)
#define PRECISION (1.e-10)
#define PI (M_PI)
#define EMPTY (-1)

#define SQUARE(x) ((x)*(x))
#define CUBE(x) ((x)*(x)*(x))
#define INDEX(i,j,NGX) ((j)*(NGX)+(i))

#define MIN(X,Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))

#define DBG_MSG printf("DONE on %d\n",me)
#define DBG_MSG_0 if(me==0){printf("DONE.\n");}
typedef unsigned int uint;
typedef int int2[2];
typedef int int3[3];

typedef uint uint2[2];
typedef uint uint3[3];

typedef double double2[2];
typedef double double3[2];
