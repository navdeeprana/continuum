	void
housekeeping (int me, FILE ** fin,char *folder)
{
	struct stat st = { 0 };
	
	sprintf (folder, "data");
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);
	
	char name[64];
	sprintf (name, "%s/log.%d", folder, (int) me);
	fin[0] = fopen (name,"w");
	
	sprintf (folder, "data/%d", me);
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);
	sprintf (name, "%s/stat.%d", folder, (int) me);
	fin[1] = fopen (name,"w");
}

	void
calculate_time_step(double *cbx, double *cby, double *cfx,
		double *cfy, double *dt, uint *fr)
{
	*cbx = DBAC / SQUARE(DX);
	*cby = DBAC / SQUARE(DY);
	*cfx = DFOO / SQUARE(DX);
	*cfy = DFOO / SQUARE(DY);

	double alpha = MAX(MAX(cbx[0],cby[0]),MAX(cfx[0],cfy[0]));
#ifdef NONLINEAR_DIFFUSIVITY
	*dt = DTFAC /(INFOO*alpha);
	printf("Non Linear diffusivity, scaling time step with food.\n");
#else
	*dt = DTFAC /(alpha);
#endif
	*fr = (int) ceil (1.e0 / (INTERVEL * dt[0]));
}
	void
linear_innoculation (uint2 ngrid,uint2 pad,double *rho, double *bac, double *food)
{
	uint i, j, idx;
	for (i = pad[0]; i <ngrid[0]-pad[1] ; i++) {
		for (j = ngrid[1]; j--;) {
			idx = INDEX(j,i,ngrid[1]);
			bac[idx] = 0.5e0 * (1.e0 - tanh (32.e0 * DY * (j - ngrid[1] / 20.e0)));
			food[idx] = fabs (INFOO * (1.e0 - bac[idx]));
			rho[idx] = bac[idx] + food[idx];
		}
	}
}
	void
gaussian_innoculation (uint2 origin,uint2 ngrid,uint2 pad,double *rho, double *bac, double *food)
{
	uint i, j, idx;
	for (i = pad[0]; i <ngrid[0]-pad[1] ; i++) {
		for (j = ngrid[1]; j--;) {
			idx = INDEX(j,i,ngrid[1]);
			double x = (i - pad[0] - NTX / 2 + origin[0]*NGX) * DX;
      double y = (j - pad[1] - NTY / 2 + origin[1]*NGY) * DY;
      bac[idx] = exp (-24 * (x * x + y * y));
			food[idx] = fabs (INFOO * (1.e0 - bac[idx]));
			rho[idx] = bac[idx] + food[idx];
		}
	}
}
/*
	void
load_initial_state_from_file(int load_time,double *rho, double *bac, double *food, FILE * fid)
{
	int i, j, idx;
	double tempbac = ZERO, tempfood = ZERO;
	printf ("Loading Snap saved at %d\n", load_time);
	while (!feof (fid)) {
		fscanf (fid, "%d\t%d\t%lg\t%lg\n", &i, &j, &tempbac, &tempfood);
		idx = INDEX (i, j);
		bac[idx] = tempbac;
		food[idx] = tempfood;
		rho[idx] = bac[idx] + food[idx];
	}
}
*/
