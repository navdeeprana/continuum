	int
check_processors(int NP)
{
	int temp;
	temp = sqrt((double) NP);
	if(temp*temp != NP){
		perror("Number of processors must be a perfect square\n");
		return 1;
	}
	else
		return 0;
}

	uint
calculate_grid_memory(int NP)
{
	uint NPX = sqrt((double) NP); // number of processors in x-direction
	uint NPY = sqrt((double) NP); // in y.
	return (NTX/NPX + 1)*(NTY/NPY+1); // basic memory chunk required for all processors.
}
	void
calculate_origin(int me,uint2 origin)
{
	origin[0] = me;
	origin[1] = 0;
}

uint
set_grid(int NP,int me, uint2 ngrid, uint2 pad)
{
	/* For 0th process, only right pad exists.*/
	if(me==0){
		ngrid[0] = NGX + 1;
		ngrid[1] = NGY;
		pad[0] = 0;
		pad[1] = 1;
	}
	/* For last process, only left pad exists.*/
	else if(me==NP-1){
		ngrid[0] = NGX + 1;
		ngrid[1] = NGY;
		pad[0] = 1;
		pad[1] = 0;
	}
	else{
		ngrid[0] = NGX + 2;
		ngrid[1] = NGY;
		pad[0] = 1;
		pad[1] = 1;
	}
	return ngrid[0]*ngrid[1];
}	

void
synchronize_state(int NP,int me, uint2 ngrid,uint2 pad,double *rho, double *leftpad, double *rightpad)
{
	uint i,idx,ng;
	ng = (ngrid[0]-2)*ngrid[1];
	/* Copy elements to the memory pads.*/
	if(pad[0] == 1){
		for(i=0;i<ngrid[1];i++){
			leftpad[i] = rho[i+ngrid[1]];
		}
	}
	if(pad[1] == 1){
		for(i=0;i<ngrid[1];i++){
			idx = ng+i;
			rightpad[i] = rho[idx];
		}
	}
	
	/*Send Pads off. Left one goes to previous processor
	 * and right one goes to the next processor. Notice
	 * the tags. */
	uint lefttag = 0, righttag=1;	
	if(me != 0){
		MPI_Send(leftpad,NGY,MPI_DOUBLE,me-1,lefttag,MPI_COMM_WORLD);
	}
	if(me != NP-1){
		MPI_Send(rightpad,NGY,MPI_DOUBLE,me+1,righttag,MPI_COMM_WORLD);
	}
	
	/* Recieve pads. Left one comes from previos processor
	 * and right one comes from next processor.*/
	
	ng = (ngrid[0]-1)*ngrid[1];
	if(me != 0){
		MPI_Recv(leftpad,ngrid[1],MPI_DOUBLE,me-1,righttag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		for(i=0;i<ngrid[1];i++){
			rho[i] = leftpad[i];
		}
	}
	if(me != NP-1){
		MPI_Recv(rightpad,ngrid[1],MPI_DOUBLE,me+1,lefttag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		for(i=0;i<ngrid[1];i++){
			idx = ng+i;
			rho[idx] =  rightpad[i];
		}
	}
}

void
synchronize_state_sendrecv(int NP,int me, uint2 ngrid,uint2 pad,double *rho, double *leftpad, double *rightpad)
{
	uint i,idx,ng;
	uint src, dest;
	MPI_Status status;
	/* This function uses MPI_Sendrecv instead of MPI_Send,MPI_Recv.
	 * First make all processors send rightpad to next processors,
	 * which then will be recieved by next processors as leftpads. */
	
	/* Copy elements to the right memory pad.*/
	ng = (ngrid[0]-2)*ngrid[1];
	if(pad[1] == 1){
		for(i=0;i<ngrid[1];i++){
			idx = ng+i;
			rightpad[i] = rho[idx];
		}
	}
	dest = (me != NP-1) ? me+1 : MPI_PROC_NULL; 
	src  = (me != 0) ? me-1 : MPI_PROC_NULL; 
	MPI_Sendrecv(rightpad,ngrid[1],MPI_DOUBLE,dest,0,leftpad,ngrid[1],MPI_DOUBLE,src,0,MPI_COMM_WORLD,&status);
	if(me != 0){
		for(i=0;i<ngrid[1];i++){
			rho[i] = leftpad[i];
		}
	}

	/* Copy elements to the left memory pad.*/
	if(pad[0] == 1){
		for(i=0;i<ngrid[1];i++){
			leftpad[i] = rho[i+ngrid[1]];
		}
	}
	dest = (me != 0) ? me-1 : MPI_PROC_NULL; 
	src  = (me != NP-1) ? me+1 : MPI_PROC_NULL; 
	MPI_Sendrecv(leftpad,ngrid[1],MPI_DOUBLE,dest,1,rightpad,ngrid[1],MPI_DOUBLE,src,1,MPI_COMM_WORLD,&status);
	
	ng = (ngrid[0]-1)*ngrid[1];
	if(me != NP-1){
		for(i=0;i<ngrid[1];i++){
			idx = ng+i;
			rho[idx] =  rightpad[i];
		}
	}
}
