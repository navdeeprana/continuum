	void
file_grid_double_normalised (FILE * fid,uint2 ngrid, uint2 pad,uint2 origin,double *arr1, double *arr2,
		double *arr3)
{
	uint i, j, idx,truei;
	 for(j=0;j<NGY;j++){
    for(i=pad[0];i<ngrid[0]-pad[1];i++){
      idx = INDEX(i,j,ngrid[0]);
      truei = i-pad[0]+origin[0]*NGX;
			fprintf (fid, "%3d\t%3d\t%1.6f\t%1.6f\n", truei, j,
					arr1[idx] / arr3[idx], arr2[idx] / arr3[idx]);
    }
    fprintf(fid,"\n");
  }
	fflush (fid);
}

	void
file_grid_double_normalised_reduced (FILE * fid,uint2 ngrid, uint2 pad,uint2 origin,double *arr1, double *arr2,
		double *arr3)
{
	uint i, j, idx,truei;
	 for(j=0;j<NGY;j=j+SKIP){
    for(i=pad[0];i<ngrid[0]-pad[1];i=i+SKIP){
      idx = INDEX(i,j,ngrid[0]);
      truei = i-pad[0]+origin[0]*NGX;
			fprintf (fid, "%3d\t%3d\t%1.6f\t%1.6f\n", truei, j,
					arr1[idx] / arr3[idx], arr2[idx] / arr3[idx]);
    }
    fprintf(fid,"\n");
  }
	fflush (fid);
}


	void
file_grid_double (FILE * fid,uint2 ngrid, uint2 pad,uint2 origin,double *arr1, double *arr2)
{
	uint i, j, idx,truei;
	for (i = pad[0]; i <ngrid[0]-pad[1] ; i++) {
		for (j = ngrid[1]; j--;) {
			idx = INDEX(j,i,ngrid[1]);
      truei = i-pad[0]+origin[0]*NGX;
			fprintf (fid, "%3d\t%3d\t%g\t%g\n", truei, j, arr1[idx], arr2[idx]);
		}
		fprintf (fid, "\n");
	}
	fprintf (fid, "\n");
	fflush (fid);
}
	void
save_snap (uint2 ngrid, uint2 pad,uint2 origin,char folder[32],double t,double *bac,double *rho)
{
	static uint fc = 0;
	char name[64];
	sprintf(name,"%s/snap.%d",folder,fc);
	FILE *fid = fopen(name,"w");
	fprintf(fid,"# time = %g\n\n",t);
	file_grid_double(fid,ngrid,pad,origin, bac,rho);
	fclose (fid);
	fc++;
}

	void
save_snap_binary (uint ngtotal,char folder[32],double *bac,double *rho)
{
	static uint fc = 0;
	char name[64];
	FILE *fid;
	sprintf(name,"%s/bac_snap_b.%d",folder,fc);
	fid = fopen(name,"wb");
	fwrite(bac,sizeof(double),ngtotal,fid);
	fclose (fid);
	
	sprintf(name,"%s/rho_snap_b.%d",folder,fc);
	fid = fopen(name,"wb");
	fwrite(rho,sizeof(double),ngtotal,fid);
	fclose (fid);
	fc++;
}
	void
save_snap_binary_restart (uint ngtotal,char folder[32],double *bac,double *rho)
{
	char name[64];
	FILE *fid;
	static uint fc = 1;
	if(fc==1) {
		fc=0;
	}
	else if(fc==0){
		fc=1;
	}
	sprintf(name,"%s/bac_snap_b.%d",folder,fc);
	fid = fopen(name,"wb");
	fwrite(bac,sizeof(double),ngtotal,fid);
	fclose (fid);

	sprintf(name,"%s/rho_snap_b.%d",folder,fc);
	fid = fopen(name,"wb");
	fwrite(rho,sizeof(double),ngtotal,fid);
	fclose (fid);
}
/*
	void
save_restart_snapshot (FILE * fid, char *folder, uint itime, uint sr, uint me,
		uint *count, double *bac, double *food)
{
	if (itime % sr == 0 && itime > 0) {
		char name[64];
		sprintf (name, "%s/snapshot.%d_%d", folder, me, itime);
		fid = fopen (name, "w");
		file_grid_double (fid, bac, food);
		if (me == 0)
			printf ("snap saved at itime = %d\n", itime);
		fclose (fid);
		*count = 0;
	}
}
*/
