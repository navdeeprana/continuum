
	void
car_cap (uint2 ngrid,uint2 pad,double *bac, double *food, double *rho,double2 local)
{
	uint i,j,idx;
	uint ngtotal = (ngrid[0]-pad[0]-pad[1])*ngrid[1];
	local[0] = ZERO;
	local[1] = ZERO;
	for (i = pad[0]; i <ngrid[0]-pad[1] ; i++) {
		for (j = ngrid[1]; j--;) {
			idx = INDEX(j,i,ngrid[1]);
			local[0] += bac[idx] / rho[idx];
			local[1] += food[idx];
		}
	}
	local[0] = local[0]/ngtotal;
	local[1] = local[1]/ngtotal;
}

	void
find_height (int me,uint2 ngrid,uint2 pad, double *bac, double *height)
{
	uint i, j, idx,count = me*NGX;
	for (i = pad[0]; i < ngrid[0]-pad[1]; i++) {
		for (j =ngrid[1]; j--;) {
			idx = INDEX(j,i,ngrid[1]);
			if (bac[idx] >= 5.e-2) {
				height[count] = j * DY;
				count++;
				break;
			}
		}
	}
}
	double
statistics(double *height)
{
	uint i;
	double mean=ZERO,variance=ZERO;
	for(i=0;i<NTX;i++){
		mean += height[i];
	}
	mean /= NTX;
	for (i = 0; i < NTX; i++)
		variance += (mean - height[i]) * (mean - height[i]);

	variance /= NTX;
	return sqrt (variance);
}


/*
	double
hull (FILE * fid, double *bac, bool * grid, double bmin)
{
	int p, q, rdx;
	static double old;
	for (p = 0; p < NGY; p++) {
		for (q = 0; q < NGX; q++) {
			rdx = INDEX (q, p);
			if (bac[rdx] >= bmin)
				grid[rdx] = 1;
			else
				grid[rdx] = 0;
		}
	}
	int i = 0, j = NGY;
	int iold = 0, jold = 0;
	int idx = INDEX (i, j);
	int count = 0, length = 0, check = 0;
	while (check == 0) {
		j--;
		idx = INDEX (i, j);
		if (grid[idx] == 1) {
			check = (grid[idx + NGX] == 1 || grid[idx - NGX] == 1
					|| grid[idx + 1] == 1);
		}
	}
#ifdef PRINT_HULL
	fprintf (fid, "%d\t%d\n", i, j);
#endif
	length++;
	count++;

	unsigned short int im;
	unsigned short int move[] = { 0, 1, 2, 3 };
	while (i < NGX - 1 && count < NG) {
		iold = i;
		jold = j;
		idx = INDEX (i, j);
		im = 0, check = 0;
		while (im < 4) {
			if (move[im] == 0 && grid[idx + 1] == 1) {
				i++;
				move[0] = 3;
				move[1] = 0;
				move[2] = 1;
				move[3] = 2;
				break;
			}
			else if (move[im] == 1 && grid[idx - NGX] == 1) {
				j--;
				move[0] = 0;
				move[1] = 1;
				move[2] = 2;
				move[3] = 3;
				break;
			}
			else if (move[im] == 2 && grid[idx - 1] == 1) {
				i--;
				move[0] = 1;
				move[1] = 2;
				move[2] = 3;
				move[3] = 0;
				break;
			}
			else if (move[im] == 3 && grid[idx + NGX] == 1) {
				j++;
				move[0] = 2;
				move[1] = 3;
				move[2] = 0;
				move[3] = 1;
				break;
			}
			im++;
		}
		if (i != iold || j != jold) {
#ifdef PRINT_HULL
			fprintf (fid, "%d\t%d\n", i, j);
#endif
			length++;
		}
		count++;
	}
	do {
		idx = INDEX (i, j);
#ifdef PRINT_HULL
		fprintf (fid, "%d\t%d\n", i, j);
#endif
		length++;
		count++;
		j++;
	} while (grid[idx + NGX] == 1);
#ifdef PRINT_HULL
	fprintf (fid, "\n\n");
	fflush (fid);
#endif
	if (i == NGX - 1)
		old = (DX * length) / LENX;
	
	return old;
}
*/
