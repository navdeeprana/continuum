#!/bin/bash
#SBATCH --job-name L=32,N=3200
#SBATCH --time 48:00:00
#SBATCH --nodes 4
#SBATCH -n 80
`#SBATCH --output $1.out`
#module load mpi/gcc/mvapich2/2.1.0
module load mpi/gcc/openmpi/2.0.1
#make
mpirun -np 80 $1

