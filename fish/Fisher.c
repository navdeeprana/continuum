#include <gsl/gsl_randist.h>
#include "parameters.h"
	void
file_double (FILE * fid, double factor, double *arr1,double *arr2)
{
	int i;

	for (i = 0; i < ng; i++)
		fprintf (fid, "%g\t%g\t%g\n", factor * i, arr1[i],arr2[i]);

	fprintf (fid, "\n\n");
	fflush (fid);
}

	void
set_ini_cond (double *rho)
{
	int i;
	double dgrid = len / ng;
	for (i = 0; i < ng; i++) {
		rho[i] = 0.5e0 * (1.e0 - tanh (32.e0 * dgrid * (i - ng / 5)));
	}
}

	void
laplacian (double constant, double *rho, double *diff)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		diff[i] = constant * (rho[i + 1] + rho[i - 1] - 2.e0 * rho[i]);
}

	long long unsigned int
poisson_nrc (const gsl_rng * r, double xm)
{
	static double sq, alxm, g;
	double em, t, y;

	if (xm < 12.e0) {
		g = exp (-xm);
		em = -1.e0;
		t = 1.e0;
		do {
			++em;
			t *= gsl_rng_uniform (r);
		}
		while (t > g);
	}
	else {
		sq = sqrt (2.e0 * xm);
		alxm = log (xm);
		g = xm * alxm - lgamma (xm + 1.e0);
		do {
			do {
				y = tan (M_PI * gsl_rng_uniform (r));
				em = sq * y + xm;
			}
			while (em < 0.e0);
			em = floor (em);
			t = 0.9e0 * (1.e0 + y * y) * exp (em * alxm - lgamma (em + 1.e0) - g);
		}
		while (gsl_rng_uniform (r) > t);
	}
	return (long long unsigned int) em;
}

	double
gamma_nrc(const gsl_rng * r, long long unsigned int ia)
{
	int i;
	double am,e,s,v1,v2,x,y;
	if(ia<1){
		double u = gsl_rng_uniform_pos(r);
		return gamma_nrc(r,1.e0+ia)*pow(u,1.e0/ia);
	}
	if(ia<6){
		x=1.e0;
		for(i=1;i<=ia;i++){
			x *= gsl_rng_uniform(r);
			x = -log(x);
		}
	}
	else{
		do{
			do{
				do{
					v1 = 2.e0*gsl_rng_uniform(r) - 1.e0;
					v2 = 2.e0*gsl_rng_uniform(r) - 1.e0;
				}
				while(v1*v1+v2*v2 > 1.e0);
				y = v2/v1;
				am = ia-1;
				s = sqrt(2.0*am+1.e0);
				x = s*y + am;
			}
			while(x<=0.e0);
			e = (1.e0+y*y)*exp(am*log(x/am)-s*y);
		}
		while(gsl_rng_uniform(r) > e);
	}
	return x;
}

	double
stochastic (const gsl_rng * r1, const gsl_rng * r2,
		double rho, double lambda)
{
	if(rho == 0.e0){
		return 0.e0;
	}
	else{
		double poisson_mean = lambda * rho;
#ifdef NRC
		long long unsigned int n = poisson_nrc(r1,poisson_mean);
#else
		unsigned int n = gsl_ran_poisson (r1, poisson_mean);
#endif
		double randgamma = gsl_ran_gamma (r2, (double) n, 1);
		return randgamma / lambda;
	}
}
	void
set_bound (double *right, double *pright)
{
	*right = *pright;
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		rho[i] += dt * drho[i];
}

	double
car_cap (double dx, double *rho)
{
	int i;
	double sum = zero;
	for (i = 0; i < ng; i++)
		sum += rho[i];


	return sum/ng;
}
	void
makedir(int id)
{
	struct stat st = {0};
	if (stat("data", &st) == -1)
		mkdir("data", 0700);

	char name[32];
	sprintf(name,"data/%d",id);
	if (stat(name, &st) == -1)
		mkdir(name, 0700);
}
	int
main (int argc, char **argv)
{
	srand (time (NULL));

	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_mt19937;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	gsl_rng_set (r1, rand ());
	gsl_rng_set (r2, rand ());

	int i;
	double dx = len / ng;
	double constant = D / (dx * dx);
	double dt = dtfac / constant;

	int framerate = (int) ceil(1.e0 / (intervel * dt));
	printf ("%g\t%d\n", dt, framerate);

	double *rho = calloc (ng, sizeof (double));
	double *drho = calloc (ng, sizeof (double));
	double *rhodet = calloc (ng, sizeof (double));
	double *drhodet = calloc (ng, sizeof (double));
	makedir(VID);
	char folder[32];
	sprintf(folder,"data/%d",VID);
	char file[32];
	sprintf(file,"%s/snap",folder);
	FILE *fid1 = fopen (file, "w");
	sprintf(file,"%s/speed",folder);
	FILE *fid2 = fopen (file, "w");

	set_ini_cond (rho);
	set_ini_cond (rhodet);
	int itime = 0;
	double lambda = 2.e0 / (SIGMA * SIGMA * dt);
	printf("%g\n",lambda);
	double average=zero;

	while (itime * dt <= simtime) {
		if (itime % (framerate) == 0) {
			printf ("\r%d", itime / framerate);
			fflush (stdout);
			average=car_cap(dx,rho);
			fprintf (fid2, "%g\t%g\t%g\n", itime * dt,average,car_cap(dx,rhodet));
			file_double (fid1, dx, rho,rhodet);
			if(average > 0.99){
				printf("Max hit.\n");
				break;
			}
		}
		for (i = 1; i < ng - 1; i++) {
			if (rho[i] < 0.5e0)
				rho[i] = stochastic(r1, r2, rho[i], lambda);
			else
				rho[i] =
					1.e0 - stochastic (r1, r2, 1.e0 - rho[i],
							lambda);
		}
		laplacian (constant, rho, drho);
		laplacian (constant, rhodet, drhodet);
		for (i = 0; i < ng; i++){
			drho[i] += gamma* rho[i]*(1.e0-rho[i]);
			rho[i] += dt*drho[i];
			drhodet[i] += gamma* rhodet[i]*(1.e0-rhodet[i]);
			rhodet[i] += dt*drhodet[i];
		}
		set_bound (&rho[ng - 1], &rho[ng - 2]);
		set_bound (&rhodet[ng - 1], &rhodet[ng - 2]);
		itime++;
	}
	free (rho);
	free (rhodet);
	free (drho);
	free (drhodet);
	fcloseall ();
	return 0;
}
