args="-Wall -D_GNU_SOURCE -DDUMPSNAP -DNRC"
include="/usr/include"
lib="/usr/lib"
lib64="/usr/lib64"
flags="-lgsl -lcblas -lm -O3"

src="Fisher.c"

declare -a sigma=("5.e-8" "4.e-8" "3.e-8" "2.e-8" "1.e-8")
declare -a lambda=("1.e+19" "2.e+19" "3.e+19" "4.e+19" "5.e+19")
for i in {1..5}
do
	echo "gcc $args -D SIGMA=${sigma[$i-1]} -D VID=$i -I$include/gsl/ -I$include/atlas/ -L$lib -L$lib64/atlas $src $flags -o run.$i"
			gcc $args -D SIGMA=${sigma[$i-1]} -D VID=$i -I$include/gsl/ -I$include/atlas/ -L$lib -L$lib64/atlas $src $flags -o run.$i
done
exit
for i in {1..5}
do
	nohup ./run.$i > log.$i &!
done
