#set term postscript eps size 25,5 color "Helvetica" 40
set term png size 1280,1280 
set xrange [0:64]
set yrange [0:1]
set notics
f='data/'
#do for [i=0:1000:5]{
#	print i
#		set output "img/".i.".png"
#		set multiplot layout 5,1 rowsfirst
#		unset colorbox
#
#		p f.'1/snap' index i u 1:2 w l lt -1 lw 2 t ''
#
#		p f.'2/snap' index i u 1:2 w l lt -1 lw 2 t ''
#
#		p f.'3/snap' index i u 1:2 w l lt -1 lw 2 t ''
#
#		p f.'4/snap' index i u 1:2 w l lt -1 lw 2 t ''
#
#		p f.'5/snap' index i u 1:2 w l lt -1 lw 2 t ''
#
#		unset multiplot	
#		unset output
#}
reset
set term png size 1280,640 
set xrange [0:5000]
set output "img/speed.png"
p f.'1/speed' u 1:2 w l lw 2 t 'sigma = 1' , \
  f.'2/speed' u 1:2 w l lw 2 t 'sigma = 0.1' , \
  f.'3/speed' u 1:2 w l lw 2 t 'sigma = 0.05' , \
  f.'4/speed' u 1:2 w l lw 2 t 'sigma = 0.01' , \
  f.'5/speed' u 1:2 w l lw 2 t 'sigma = 0.001' , \
  f.'5/speed' u 1:3 every 20 w l lw 2 lt -1 t 'FISHER,MEAN FIELD' , \
