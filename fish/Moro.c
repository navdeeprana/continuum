#include <gsl/gsl_randist.h>
#include "parameters.h"
	void
file_double (FILE * fid, double factor, double *arr1, double *arr2)
{
	int i;

	for (i = 0; i < ng; i++)
		fprintf (fid, "%g\t%g\t%g\n", factor * i, arr1[i], arr2[i]);

	fprintf (fid, "\n\n");
	fflush (fid);
}

	void
set_ini_cond (double *rho)
{
	int i;
	double dgrid = len / ng;
	for (i = 0; i < ng; i++) {
		rho[i] = 0.5e0 * (1.e0 - tanh (16.e0 * dgrid * (i - ng / 5)));
	}
}

	void
laplacian (double constant, double *rho, double *diff)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		diff[i] = constant * (rho[i + 1] + rho[i - 1] - 2.e0 * rho[i]);
}

	double
stochastic (const gsl_rng * r1, const gsl_rng * r2, double k, double lambda)
{
	double poisson_mean = 0.5e0 * lambda;
	unsigned int n = gsl_ran_poisson (r1, poisson_mean);
	if (n == 0) {
		return 0.e0;
	}
	else {
		int i;
		double sum = 0.e0, temp;
		for (i = 1; i <= 2 * n; i++) {
			temp = gsl_ran_ugaussian (r2);
			sum += temp * temp;
		}
		return (0.5e0 * sum) / k;
	}
}

	void
set_bound (double *right, double *pright)
{
	*right = *pright;
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		rho[i] += dt * drho[i];
}

	double
car_cap (double dx, double *rho)
{
	int i;
	double sum = zero;
	for (i = 0; i < ng; i++)
		sum += rho[i];

	return dx * sum;
}

	int
main (int argc, char **argv)
{
	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	srand (time (NULL) + rand ());
	gsl_rng_set (r1, rand ());
	srand (time (NULL) + rand ());
	gsl_rng_set (r2, rand ());

	int i;
	double dx = len / ng;
	double constant = D / (dx * dx);
	double dt = dtfac / constant;

	int freq = (int) ((1.0 / (intvl * dt)));
	printf ("%g\t%d\n", dt, freq);

	double *rho = calloc (ng, sizeof (double));
	double *drho = calloc (ng, sizeof (double));
	double *rhodet = calloc (ng, sizeof (double));
	double *drhodet = calloc (ng, sizeof (double));
	int *flag = calloc (ng, sizeof (int));

	FILE *fid1 = fopen ("f_snap", "w");
	FILE *fid2 = fopen ("f_speed", "w");

	set_ini_cond (rho);
	set_ini_cond (rhodet);
	int itime = 0;
	double k = 2.0e0 * gamma / (sigma * sigma * (exp (gamma * dt) - 1));
	double lambda;
	while (itime * dt <= simtime) {
		if (itime % (freq) == 0) {
			printf ("\r%d", itime / freq);
			fflush (stdout);
			fprintf (fid2, "%g\t%g\t%g\n", itime * dt, car_cap (dx, rho),
					car_cap (dx, rhodet));
			file_double (fid1, dx, rho, rhodet);
		}
		for (i = 1; i < ng - 1; i++) {
			if (rho[i] < 0.5e0) {
 			lambda = k * exp (gamma * dt) * rho[i];
 			rho[i] = stochastic (r1, r2, k, lambda);
			}
			else {
				lambda = k * exp (gamma * dt) * (1.e0 - rho[i]);
				rho[i] = stochastic (r1, r2, k, lambda);
				flag[i] = 1;
			}
 	}
 	laplacian (constant, rho, drho);
 	laplacian (constant, rhodet, drhodet);
 	for (i = 0; i < ng; i++) {
			if (flag == 0) {
 			drho[i] += gamma * rho[i] *(1.e0- rho[i]);
 			rho[i] += dt * drho[i];
			}
			else {
				drho[i] += -gamma * (1.e0 + (1.e0 - rho[i]) * (1.e0 - rho[i]));
				rho[i] += dt * drho[i];
				rho[i] = 1.e0 - rho[i];
			}
			flag[i] = 0;
			drhodet[i] += gamma * rhodet[i] * (1.e0 - rhodet[i]);
			rhodet[i] += dt * drhodet[i];
		}

		set_bound (&rho[ng - 1], &rho[ng - 2]);
		set_bound (&rhodet[ng - 1], &rhodet[ng - 2]);
		itime++;
	}
	free (rho);
	free (rhodet);
	free (drhodet);
	fcloseall ();
	return 0;
}
