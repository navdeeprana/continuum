#set term png font ariel 20 size 1600,1600
set term X11 noraise
set yrange [0:1.1]
do for [i=0:1000:1]{
	p "data/".n."/snap" index i u 1:3 w l lt 1 lw 2,\
	  "data/".n."/snap" index i every 10 u 1:2 w p pt 7 ps 1 lt -1
	pause 0.05 
}   

