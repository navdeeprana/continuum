#ifndef __parameters_h__
#define __parameters_h__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

#define zero (0.0E0)
#define ng (4096)
#define len (256.E0)
#define simtime (1E4)

#define D (1.E0)
#define gamma (1.E0)
//#define SIGMA (1.E-1)

#define dtfac (1.E-1)
#define intervel (1)

#endif
