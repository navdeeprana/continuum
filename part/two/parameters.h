#define NCHIP (256)
#define NDALE (256)
#define NFOOD (8192)
#define NPAR (NCHIP+NDALE+NFOOD)

#define LENX (8.0e0)		// length of the box in x
#define LENY (4.0e0)		// length of the box in y

#define NCELLX (64)		// number of cells in x
#define NCELLY (32)		// number of cells in y:
#define NCELL (NCELLX*NCELLY)

#define DCHIP (1.e-4)
#define DDALE (1.e-4)
#define DFOOD (1.e-2)

#define RCHIP (1.e0)		// rate of turning to chip
#define RDALE (1.e0)		// rate of turning to dale

#define RFCHIP (1.e0)		// food eating rate
#define RFDALE (1.e0)		// food eating rate

#define NTIME (5E3)		// total time steps
#define DT (1.0e-2)		// size of time step
#define fraction (1.e-1)
#define frequency (50)
