set term X11 noraise size 200,400
set xrange [0:2]
set yrange [0:4]
set palette defined (0 "red", 1 "blue", 2 "black")
do for [i=0:400:1]{
		p 'data/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t ''
		pause 0.3
	}
pause -1
