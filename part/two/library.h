#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

#ifdef MPI
#include<mpi.h>
#endif

#define PI M_PI
#define ZERO 0.e0
#define ONE 1.e0
#define EMPTY -1

#include "parameters.h"

/* Define arrays , both integer and double */
typedef struct
{
  double x;
  double y;
} dvec2;

typedef struct
{
  int x;
  int y;
} ivec2;

typedef struct
{
  double x;
  double y;
  double z;
} dvec3;

typedef struct
{
  int x;
  int y;
  int z;
} ivec3;

typedef enum
{
  chip = 0,
  dale = 1,
  foe = 2,

} trip;

void
file_dvec_bool (FILE * fid, dvec2 * pos, trip * par)
{
  int i;
  for (i = 0; i < NPAR; i++)
    fprintf (fid, "%f\t%f\t%d\n", pos[i].x, pos[i].y, par[i]);

  fprintf (fid, "\n\n");
  fflush (fid);
}

void
set_system_density (int seed, dvec2 box, dvec2 * pos, trip * par)
{
  int i;
  double shift = fraction * box.x;
  double rem = (1.e0 - fraction) * box.x;
  srand48 (time (NULL) + seed);
  srand (time (NULL) + rand ());
  for (i = 0; i < NCHIP; i++) {
    srand48 (rand ());
    pos[i].x = shift * drand48 ();
    srand48 (rand ());
    pos[i].y = box.y * drand48 ();
    par[i] = chip;
  }
  for (i = NCHIP; i < NCHIP + NDALE; i++) {
    srand48 (rand ());
    pos[i].x = shift * drand48 ();
    srand48 (rand ());
    pos[i].y = box.y * drand48 ();
    par[i] = dale;
  }
  for (i = NCHIP + NDALE; i < NPAR; i++) {
    srand48 (rand ());
    pos[i].x = shift + rem * drand48 ();
    srand48 (rand ());
    pos[i].y = box.y * drand48 ();
    par[i] = foe;
  }
}

void
set_cell_position (ivec2 ncelld, dvec2 lcell, dvec2 * cellpos)
{
  int i, x, y;
  double dx = 0.5e0 * lcell.x;
  double dy = 0.5e0 * lcell.y;
  for (i = 0; i < NCELL; i++) {
    x = i % ncelld.x;
    y = floor (i / ncelld.x);
    cellpos[i].x = lcell.x * x + dx;
    cellpos[i].y = lcell.y * y + dy;
  }
}

void
print_progress (int index, int time)
{
  float prog = (float) index / time;

  if (index % frequency == 0) {
    printf ("\r%.2f%%", prog * 100);
    fflush (stdout);
  }
}

double
calculate_conc (FILE * fid, dvec2 * cellpos, ivec3 * type)
{
  int i;
  double total, cchip, cdale;
  double capacity = 0.e0;
  for (i = 0; i < NCELL; i++) {
    total = type[i].x + type[i].y + type[i].z;
    if (total != 0) {
      cchip = type[i].x / total;
      cdale = type[i].y / total;
    }
    else {
      cchip = 0;
      cdale = 0;
    }
    fprintf (fid, "%.2f\t%.2f\t%f\t%f\n", cellpos[i].x, cellpos[i].y, cchip,
	     cdale);
    capacity += cchip;
    capacity += cdale;
  }
  fprintf (fid, "\n\n");
  return capacity / NCELL;
}

void
histogram (ivec2 ncelld, dvec2 lcell, dvec2 * pos, trip * par,
	   int *loc, ivec3 * type)
{
  int i, cell;
  for (i = 0; i < NCELL; i++) {
    type[i].x = 0;
    type[i].y = 0;
    type[i].z = 0;
  }
  for (i = 0; i < NPAR; i++) {
    cell =
      (int) (floor (pos[i].x / lcell.x) +
	     ncelld.x * floor (pos[i].y / lcell.y));
    loc[i] = cell;
    if (par[i] == chip)
      type[cell].x++;
    else if (par[i] == dale)
      type[cell].y++;
    else
      type[cell].z++;

  }
}

void
reaction (ivec2 ncelld, dvec2 rate, dvec2 frate, trip * par,
	  int *loc, ivec3 * type)
{
  int i, cell;
  double r, prob;
  for (i = 0; i < NPAR; i++) {
    cell = loc[i];
		/*---------------A+B ==> A+A--------------*/
    prob = rate.x * type[cell].x * DT;
    srand48 (rand ());
    r = drand48 ();
    if (par[i] == dale && prob > r)
      par[i] = chip;

		/*---------------A+F ==> A+A--------------*/
    prob = frate.x * type[cell].x * DT;
    srand48 (rand ());
    r = drand48 ();
    if (par[i] == foe && prob > r)
      par[i] = chip;


		/*---------------A+B ==> B+B--------------*/
    prob = rate.y * type[cell].y * DT;
    srand48 (rand ());
    r = drand48 ();
    if (par[i] == chip && prob > r)
      par[i] = dale;

		/*---------------B+F ==> B+B--------------*/
    prob = frate.y * type[cell].y * DT;
    srand48 (rand ());
    r = drand48 ();
    if (par[i] == foe && prob > r)
      par[i] = dale;
  }
}

void
array_rand_std_norm (int nran, double *rands)
{
  srand48 (rand ());
  int idx = 0;
  double x, y, r2;
  while (idx < nran) {
    do {
      x = -1 + 2 * drand48 ();
      y = -1 + 2 * drand48 ();
      r2 = x * x + y * y;
    } while (r2 > 1.e0 || r2 == 0);
    rands[idx] = x * sqrt (-2.0 * log (r2) / r2);
    rands[idx + 1] = y * sqrt (-2.0 * log (r2) / r2);
    idx += 2;
  }
}

void
brownian_motion_2d (dvec3 step, dvec2 * array, trip * par, double *rands)
{
  int i, idx = 0;
  for (i = 0; i < NPAR; i++) {
    if (par[i] == chip) {
      array[i].x += step.x * rands[idx];
      array[i].y += step.x * rands[idx + 1];
    }
    if (par[i] == dale) {
      array[i].x += step.y * rands[idx];
      array[i].y += step.y * rands[idx + 1];
    }
    if (par[i] == foe) {
      array[i].x += step.z * rands[idx];
      array[i].y += step.z * rands[idx + 1];
    }
    idx += 2;
  }
}

void
set_asymm_boundary (dvec2 box, dvec2 * pos)
{
  int i;
  for (i = 0; i < NPAR; i++) {
    if (pos[i].x > box.x)
      pos[i].x = 2.e0 * box.x - pos[i].x;
    if (pos[i].x < 0)
      pos[i].x = fabs (pos[i].x);
    if (pos[i].y > box.y)
      pos[i].y = pos[i].y - box.y;
    if (pos[i].y < 0)
      pos[i].y = pos[i].y + box.y;
  }
}

void
makedir (int id,int flag)
{
  struct stat st = { 0 };
  if (stat ("data", &st) == -1)
    mkdir ("data", 0700);

  if(flag){
	  char name[32];
	  sprintf (name, "data/%d", id);
	  if (stat (name, &st) == -1)
		  mkdir (name, 0700);
  }
}
