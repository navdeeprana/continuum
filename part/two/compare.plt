set term X11 noraise size 600,600
set xrange [0:4]
set yrange [0:8]
XTICS = "set xtics('0' 0,'1' 1,'2' 2)"
YTICS = "set ytics('0' 0,'1' 1,'2' 2,'3' 3,'4' 4)"

NOXTICS = "set xtics('' 0,'' 1,'' 2);unset xlabel"
NOYTICS = "set ytics('' 0,'' 1,'' 2);unset ylabel"

set palette defined (0 "red",1 "blue", 2 "black")
do for [i=0:100:1]{
		unset colorbox
		set multiplot layout 1,2 rowsfirst
		
		set title 'DATA'
		@NOXTICS; @YTICS
		p 'data/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t ''
 
		set title 'DUMP'
		@XTICS; @NOYTICS
		p 'dump/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t ''
		
		unset multiplot
		pause 0.5
}
