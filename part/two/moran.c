#include"library.h"
int
main (int argc, char *argv[])
{
#ifdef MPI
  int NP, me;
  MPI_Init (&argc, &argv);
  MPI_Comm_size (MPI_COMM_WORLD, &NP);
  MPI_Comm_rank (MPI_COMM_WORLD, &me);
#else
  int me = 0;
#endif
  srand (time (NULL) + me);

  ivec2 ncelld;
  ncelld.x = NCELLX;
  ncelld.y = NCELLY;

  dvec2 box, lcell, rate, frate;

  box.x = (double) LENX;
  box.y = (double) LENY;

  lcell.x = box.x / ncelld.x;
  lcell.y = box.y / ncelld.y;

  rate.x = (double) RCHIP *(me + 1);
  rate.y = (double) RDALE *(me + 1);
  frate.x = (double) RFCHIP *(me + 1);
  frate.y = (double) RFDALE *(me + 1);

  dvec3 step;

  step.x = sqrt (2.e0 * DCHIP * DT);
  step.y = sqrt (2.e0 * DDALE * DT);
  step.z = sqrt (2.e0 * DFOOD * DT);

  dvec2 *pos = calloc (NPAR, sizeof (dvec2));
  trip *par = calloc (NPAR, sizeof (trip));
  int *loc = calloc (NPAR, sizeof (int));
  ivec3 *type = calloc (NCELL, sizeof (ivec3));

  dvec2 *cellpos = calloc (NCELL, sizeof (dvec2));

  double *rands = calloc (2 * NPAR, sizeof (double));

  int seed = rand ();
  set_system_density (seed, box, pos, par);
  set_cell_position (ncelld, lcell, cellpos);

  FILE *fid;
  char snap[32], name[64];
  /* You can get rid of the flag arg in makedir and leave it out of no mpi. But, you need to create data folder, to avoid segmentation fault. Automate all you can policy followed. */

#ifdef MPI
  makedir (me,1);
  sprintf (snap, "data/%d/snap", me);
#else
  makedir (me,0);
  sprintf (snap, "data/snap");
#endif
  int fc = 0;
  double capacity=ZERO;
  for (int i = 0; i < NTIME; i++) {
    print_progress (i, NTIME);
    if (i % frequency == 0) {
      sprintf (name, "%s.%d", snap, fc);
      fid = fopen (name, "w");
      file_dvec_bool (fid, pos, par);
      //capacity = calculate_conc (fid, cellpos, type);
      fclose (fid);
      fc++;
      if (capacity > 0.95) {
	printf ("\n Max carrying capacity hit.\n");
	break;
      }
    }
    histogram (ncelld, lcell, pos, par, loc, type);
    reaction (ncelld, rate, frate, par, loc, type);
    array_rand_std_norm (2 * NPAR, rands);
    brownian_motion_2d (step, pos, par, rands);
    set_asymm_boundary (box, pos);
  }
  free (par);
  free (pos);
  free (loc);
  free (type);
  free (cellpos);
  free (rands);
  printf ("EOS. Clean exit on rank %d\n", me);
#ifdef MPI
  MPI_Finalize ();
#endif
  return 0;
}
