#include"library.h"
	int
main (int argc, char *argv[])
{
#ifdef MPI
	int NP, me;
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);
#else
	int me = 0;
#endif
	srand (time (NULL) + me);

	ivec ncelld;
	ncelld.x = ncellx;
	ncelld.y = ncelly;

	dvec box, lcell, rate, step;

	box.x = (double) length_x;
	box.y = (double) length_y;

	lcell.x = box.x / ncelld.x;
	lcell.y = box.y / ncelld.y;

	rate.x = (double) rchip *(me + 1);
	rate.y = (double) rdale *(me + 1);

	step.x = sqrt (2.0e0 * dchip * dt);
	step.y = sqrt (2.e0 * ddale * dt);

	dvec *pos = calloc (npar, sizeof (dvec));
	bool *par = calloc (npar, sizeof (bool));
	int *location = calloc (npar, sizeof (int));
	ivec *type = calloc (ncell, sizeof (ivec));
	dvec *cellpos = calloc (ncell, sizeof (dvec));
	double *rands = calloc (2 * npar, sizeof (double));

	int seed = rand ();
	set_system_density (seed, box, pos, par);
	set_cell_position (ncelld, lcell, cellpos);

	FILE *fid;
	char snap[32], name[64];
#ifdef MPI
	makedir (me);
	sprintf (snap, "data/%d/snap", me);
#else
	sprintf (snap, "data/snap");
#endif
	int fc = 0;
	double capacity;
	int checkid = ncelld.x;
	int flag = 1;
	for (int i = 0; i < ntime; i++)
	{
		print_progress (i, ntime);
		if (i % frequency == 0)
		{
			sprintf (name, "%s.%d", snap, fc);
			fid = fopen (name, "w");
			capacity = calculate_conc (fid, cellpos, type);
			fclose (fid);
			fc++;
			if (capacity > 0.95)
			{
				printf ("\n Max carrying capacity hit.\n");
				break;
			}
		}
		histogram (ncelld, lcell, pos, par, location, type);
#ifdef OPTIMISE
		if (flag)
		{
			checkid = 0;
			flag = 0;
		}
		checkid = optimiser (ncelld, type, checkid);
#endif
		reaction (ncelld, rate, par, location, type, checkid);
		array_rand_std_norm (2 * npar, rands);
		brownian_motion_2d (0, step, pos, par, type, location, rands);
		set_asymm_boundary (box, pos);
	}
	free (par);
	free (pos);
	free (location);
	free (type);
	free (cellpos);
	free (rands);
	printf ("EOS. Clean exit on rank %d\n", me);
#ifdef MPI
	MPI_Finalize ();
#endif
	return 0;
}
