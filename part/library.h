
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

#ifdef MPI
#include<mpi.h>
#endif

#define pi M_PI
#define zero 0.e0
#define one 1.e0
#define empty -1
#include "parameters.h"

/* Define arrays , both integer and double */
typedef struct
{
  double x;
  double y;
} dvec;

typedef struct
{
  int x;
  int y;
} ivec;

typedef enum
{
  chip = 0,
  dale = 1,
} bool;

	void
set_system_density(int seed,dvec box,dvec *pos,bool *par)
{
	int i;
	double shift = fraction*box.x;
	double rem = (1.e0 - fraction)*box.x;
	srand48 (time (NULL) + seed);
	srand (time (NULL) + rand ());
	for (i = 0; i < nchip; i++) {
		srand48 (rand ());
		pos[i].x = shift * drand48 ();
		srand48 (rand ());
		pos[i].y = box.y * drand48 ();
		par[i] = chip;
	}
	for (i=nchip;i<npar;i++){
		srand48 (rand ());
		pos[i].x = shift + rem * drand48 ();
		srand48 (rand ());
		pos[i].y = box.y * drand48 ();
		par[i] = dale;
	}
}
	void
set_cell_position (ivec ncelld, dvec lcell, dvec * cellpos)
{
	int i, x, y;
	double dx = 0.5e0 * lcell.x;
	double dy = 0.5e0 * lcell.y;
	for (i = 0; i < ncell; i++) {
		x = i % ncelld.x;
		y = floor (i / ncelld.x);
		cellpos[i].x = lcell.x * x + dx;
		cellpos[i].y = lcell.y * y + dy;
	}
}
void
print_progress (int index,int time)
{
  float prog = (float) index / time;

  if (index % frequency == 0) {
    printf ("\r%.2f%%", prog * 100);
    fflush (stdout);
  }
}
	double
calculate_conc (FILE * fid,dvec * cellpos, ivec * type)
{
	int i;
	double total, cchip;
	double capacity=0.e0;
	for (i = 0; i < ncell; i++) {
		total = type[i].x + type[i].y;
		if (total != 0)
			cchip = type[i].x / total;
		else
			cchip = 0;
		fprintf (fid, "%.2f\t%.2f\t%f\n", cellpos[i].x, cellpos[i].y, cchip);
		capacity += cchip;
	}
	fprintf (fid, "\n\n");
	return capacity/ncell;
}
void
histogram (ivec ncelld, dvec lcell, dvec * pos,bool *par,
		 int *location,ivec *type)
{
  int i, cell;
  for (i = 0; i < ncell; i++) {
    type[i].x = 0;
    type[i].y = 0;
  }
  for (i = 0; i < npar; i++) {
    cell =
      (int) (floor (pos[i].x / lcell.x) +
	     ncelld.x * floor (pos[i].y / lcell.y));
    location[i] = cell;
    if (par[i] == chip)
      type[cell].x += 1;
    else
      type[cell].y += 1;
  }
}
	void
reaction (ivec ncelld,dvec rate, bool * par,
		int *location, ivec * type,int checkid)
{
	int i, cell;
	double r, prob;
	for (i = 0; i < npar; i++) {
		/*---------------A+B ==> A+A--------------*/
		cell = location[i];
		if((cell%ncelld.x)<=checkid){
			prob = rate.x * type[cell].x * dt;
			srand48 (rand ());
			r = drand48 ();
			if (par[i] == dale && prob > r) {
				par[i] = chip;
			}
		}
		/*---------------A+B ==> B+B--------------*/
		/*
		if (ifhappened) {
			prob = rate.y * type[cell].y * dt;
			srand48 (rand ());
			r = drand48 ();
			if (par[i] == chip && prob > r) {
				par[i] = dale;
			}
		}
		*/
	}
}
void
array_rand_std_norm(int nran,double *rands)
{
	srand48 (rand());
	int idx=0;
	double x,y,r2;
	while(idx<nran){
		do{
			x = -1 + 2*drand48();
			y = -1 + 2*drand48();
			r2 = x*x + y*y;
		}while(r2 > 1.e0 || r2 ==0);
		rands[idx] = x*sqrt(-2.0*log(r2)/r2);
		rands[idx+1] = y*sqrt(-2.0*log(r2)/r2);
		idx +=2;
	}
}
	void
brownian_motion_2d (int flag,dvec step, dvec * array, bool *par,ivec *type, int *location,double *rands)
{
	int i,idx=0,id;
	double cchip;
	if(flag){
		for (i = 0; i < npar; i++) {
			if(par[i]==chip){
				id = location[i];
				cchip = type[id].y/(double)(type[id].x+type[id].y); 
				array[i].x += cchip*step.x * rands[idx];
				array[i].y += cchip*step.x * rands[idx+1];
			}
			if(par[i]==dale){
				array[i].x += step.y * rands[idx];
				array[i].y += step.y * rands[idx+1];
			}
			idx += 2;
		}
	}
	else{
		for (i = 0; i < npar; i++) {
			if(par[i]==chip){
				array[i].x += step.x * rands[idx];
				array[i].y += step.x * rands[idx+1];
			}
			if(par[i]==dale){
				array[i].x += step.y * rands[idx];
				array[i].y += step.y * rands[idx+1];
			}
			idx += 2;
		}

	}
}
	void
set_asymm_boundary (dvec box, dvec * pos)
{
	int i;
	for (i = 0; i < npar; i++) {
		if (pos[i].x > box.x)
			pos[i].x = 2.e0*box.x - pos[i].x;
		if (pos[i].x < 0)
			pos[i].x = fabs(pos[i].x);
		if (pos[i].y > box.y)
			pos[i].y = pos[i].y - box.y;
		if (pos[i].y < 0)
			pos[i].y = pos[i].y + box.y;
	}
}
	int
optimiser(ivec ncelld,ivec *type,int oldid)
{
	/*------THE ZEN PIECE OF THIS ENTIRE SIMULATION--------*/
	/* The idea is that as system grows from left to right,
	 * there is no point in doing the integration for the
	 * entire space where there's nothing. This piece just finds
	 * out the point after which there is nothing.
	 */
	int ix,iy;
	int checkid;
	checkid=oldid;
	int found;
	for(ix=oldid;ix<ncelld.x;ix++){
		found=0;
		for(iy=0;iy<ncelld.y;iy++){
			if(type[ix+iy*ncelld.x].x !=0){
				found=1;
				break;
			}
		}
		if(!found){
			checkid=ix;
			break;
		}
	}
	return checkid;
}
	void
makedir(int id)
{
	struct stat st = {0};
	if (stat("data", &st) == -1)
		mkdir("data", 0700);

	char name[32];
	sprintf(name,"data/%d",id);
	if (stat(name, &st) == -1)
		mkdir(name, 0700);
}
