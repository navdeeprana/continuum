#define nchip (1024)
#define ndale (16384)
#define npar (nchip+ndale)

#define length_x (16.0e0)		// length of the box in x
#define length_y (8.0e0)		// length of the box in y

#define ncellx (256)	// number of cells in x
#define ncelly (64)		// number of cells in y:
#define ncell (ncellx*ncelly)

#define dchip (1.e-4)
#define ddale (1.e-1)

#define rchip (1.e0)	// rate of turning to chip
#define rdale (0.e0)		// rate of turning to dale

#define ntime (5E4)		// total time steps
#define dt (1.0e-2)		// size of time step
#define fraction (5.e-2)
#define onepercell (0)
#define frequency (500)

#define chipnoise (0.3e0)
#define dalenoise (0.3e0)

