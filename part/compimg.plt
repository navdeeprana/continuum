set term png font ariel 24 size 2000,2000
set xrange [0:8]
set yrange [0:16]
XTICS = "set xtics('0' 0,'4' 4,'8' 4)"
YTICS = "set ytics('0' 0,'4' 4,'8' 8,'12' 12,'16' 16)"

NOXTICS = "set xtics('' 0,'' 32,'' 64);unset xlabel"
NOYTICS = "set ytics('' 0,'' 64,'' 128,'' 192,'' 256);unset ylabel"

set palette defined (0 "black",0.5 "white", 1 "grey")
#set palette defined ( 0 '#fff000',0.5 '#00e4ff',1 '#0000ff')
folder='data/'
do for [i=0:400:1]{
		unset colorbox
		set output "img/img.".i.".png"
		print "Creating ".i
		set multiplot layout 2,4 rowsfirst
		
		set title 'rate = 1'
		@NOXTICS; @YTICS
		p folder.'0/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t '';
 
		set title 'rate = 2'
		@NOXTICS; @NOYTICS
		p folder.'1/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t '';

		set title 'rate = 3'
		@NOXTICS; @NOYTICS
		p folder.'2/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t '';

		set title 'rate = 4'
		@NOXTICS; @NOYTICS
		p folder.'3/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t '';

		set title 'rate = 5'
		@XTICS; @YTICS
		p folder.'4/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t '';

		set title 'rate = 6'
		@XTICS; @NOYTICS
		p folder.'5/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t '';

		set title 'rate = 7'
		@XTICS; @NOYTICS
		p folder.'6/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t '';

		set title 'rate = 8'
		@XTICS; @NOYTICS
		p folder.'7/snap.'.i u 2:1:3 w p pt 5 palette ps 1 t '';

		unset multiplot
		unset output
}
