#include <gsl/gsl_randist.h>
#include <fftw3.h>
#include "parameters.h"

	void
set_initial_rho (double dx, double *rho)
{
	int i, j, idx;
	for (i = 0; i < ngy; i++) {
		for (j = 0; j < ngx; j++) {
			idx = index (i, j);
			rho[idx] = 0.5e0 * (1.e0 - tanh (32.e0 * dx * (j - ngx / 32)));
		}
	}
}

	void
laplacian (double constx, double consty, double *rho, double *lap)
{
	int i, j, idx;
	for (i = 1; i < ngy - 1; i++) {
		for (j = 1; j < ngx - 1; j++) {
			idx = index (i, j);
			lap[idx] =
				constx * (rho[idx + 1] + rho[idx - 1] - 2.e0 * rho[idx]) +
				consty * (rho[idx + ngx] + rho[idx - ngx] - 2.e0 * rho[idx]);
		}
	}
}

	long long unsigned int
poisson_nrc (const gsl_rng * r, double xm)
{
	static double sq, alxm, g;
	double em, t, y;

	if (xm < 12.e0) {
		g = exp (-xm);
		em = -1.e0;
		t = 1.e0;
		do {
			++em;
			t *= gsl_rng_uniform (r);
		}
		while (t > g);
	}
	else {
		sq = sqrt (2.e0 * xm);
		alxm = log (xm);
		g = xm * alxm - lgamma (xm + 1.e0);
		do {
			do {
				y = tan (M_PI * gsl_rng_uniform (r));
				em = sq * y + xm;
			}
			while (em < 0.e0);
			em = floor (em);
			t = 0.9e0 * (1.e0 + y * y) * exp (em * alxm - lgamma (em + 1.e0) - g);
		}
		while (gsl_rng_uniform (r) > t);
	}
	return (long long unsigned int) em;
}
	
	double
stochastic (const gsl_rng * r1, const gsl_rng * r2,
		double rho, double lambda)
{
	if(rho == 0.e0){
		return 0.e0;
	}
	else{
		double poisson_mean = lambda * rho;
		long long unsigned int n = poisson_nrc(r1,poisson_mean);
//		unsigned int n = gsl_ran_poisson (r1, poisson_mean);
		double randgamma = gsl_ran_gamma (r2, (double) n, 1);
		return randgamma / lambda;
	}
}
	void
set_boundary (double *array)
{
	int i;
	for (i = 1; i < ngx - 1; i++) {
		array[i] = array[i + ngx];
		array[ng - 1 - i] = array[ng - 1 - ngx - i];
	}
	for (i = 1; i < ngy - 1; i++) {
		array[i * ngx] = array[i * ngx + 1];
		array[(i + 1) * ngx - 1] = array[(i + 1) * ngx - 2];
	}
}

	void
makedir (int vid)
{
	struct stat st = { 0 };
	if (stat ("data", &st) == -1)
		mkdir ("data", 0700);

	char name[32];
	sprintf (name, "data/%d", vid);
	if (stat (name, &st) == -1)
		mkdir (name, 0700);
}

	double
average (double *rho)
{
	int i;
	double mean = zero;
	for (i = 0; i < ng; i++)
		mean += rho[i];

	return mean / ng;
}

	double
statistics (int fc,char folder[32],double *rho, double *height)
{
	int i, j, idx;
	double dx = lenx / ngx;
	double mean = zero, var = zero;
	for (i = 0; i < ngy; i++) {
		for (j = ngx - 1; j >= 0; j--) {
			idx = index (i, j);
			if (rho[idx] >= 5.e-2) {
				height[i] = j*dx;
				break;
			}
		}
		mean += height[i];
	}
	mean = mean / ngy;
	char name[32];
	sprintf (name, "%s/height.%d", folder,fc);
	FILE *fid = fopen (name, "w");
	for (i = 0; i < ngy; i++){
		fprintf(fid,"%d\t%g\n",i,height[i]);
		
		height[i] -= mean;
		var += height[i]*height[i];
	}
	fflush(fid);
	fclose(fid);

	var /= ngy;
	return var;
}
	int
main (int argc, char **argv)
{
#ifdef MPI
	int NP, me;
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);
#else
	int me = 0;
#endif
	srand (time (NULL) + me);

	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	gsl_rng_set (r1, rand ());
	gsl_rng_set (r2, rand ());

	int i, j, idx;
	double dx = lenx / ngx;
	double dy = leny / ngy;
	double constx = D / (dx * dx);
	double consty = D / (dy * dy);
	double dt;
	if (dx <= dy)
		dt = dtfac / constx;
	else if (dy <= dx)
		dt = dtfac / consty;
	int framerate = (int) ceil (1.e0 / (intervel * dt));
	if (me == 0)
		printf ("%g\t%d\n", dt, framerate);

	double *rho = calloc (ng, sizeof (double));
	double *drho = calloc (ng, sizeof (double));
	double *height = calloc (ng, sizeof (double));

	char folder[32];
#ifdef MPI
	makedir (me);
	sprintf (folder, "data/%d", me);
#else
	sprintf (folder, "data");
#endif

	if(me==0) system ("cp parameters.h data/.");
	
	char name[32];
	sprintf (name, "%s/statistics", folder);
	FILE *sfile = fopen (name, "w");
	
	set_initial_rho (dx, rho);
	const double lambda = 2.0e0 / (sigma * sigma * dt);
	printf("%g\n",lambda);
	
	double carcap=zero;
	int fc=0;
	int itime = 0;
	while (itime * dt <= simtime) {
		if (itime % framerate == 0) {
			if (me == 0){
				printf ("\r%g", itime * dt);
				fflush (stdout);
			}
			carcap = average(rho);
			fprintf (sfile, "%g\t%g\t%g\n", dt * itime, carcap,statistics(fc,folder,rho,height));
			fflush (sfile);
			fc++;
			if (carcap > 0.99) {
				printf ("Max Carr Capacity hit.\t");
				break;
			}
		}
		for (i = 1; i < ngy - 1; i++) {
			for (j = 1; j < ngx - 1; j++) {
				idx = index (i, j);
				if (rho[idx] < 0.5e0) {
					rho[idx] = stochastic (r1, r2, rho[idx], lambda);
				}
				else{
					rho[idx] = 1.e0 - stochastic (r1, r2, 1.e0 - rho[idx], lambda);
				}
			}
		}
		laplacian (constx, consty, rho, drho);
		for (i = 1; i < ngy - 1; i++) {
			for (j = 1; j < ngx - 1; j++) {
				idx = index (i, j);
				drho[idx] += gamma * rho[idx] * (1.e0 - rho[idx]);
				rho[idx] += dt*drho[idx];
			}
		}
		set_boundary (rho);
		itime++;
	}
	free (rho);
	free (drho);
	free(height);
	printf ("EOS on %d\n.", me);
#ifdef MPI
	MPI_Finalize ();
#endif
	return 0;
}
