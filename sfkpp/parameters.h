#ifndef __parameters_h__
#define __parameters_h__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef MPI
#include <mpi.h>
#endif


#define zero (0.e0)
#define ngx (2048)
#define ngy (2048)
#define ng (ngx*ngy)

#define lenx (512.e0)
#define leny (512.e0)

#define simtime (5.e3)

#define D  (1.e-3)

#define gamma (1.e0)
#define sigma (5.e-2)

#define dtfac (1.e-2)
#define intervel (1/0.625)

#define index(i,j) (i*ngx+j)
#endif
