#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define NF 8
int main(int argc, char **argv)
{
	int i,flag=1;
	char input[64],output[64];
	sprintf(output,"data/sigma.a");
	printf("%s\n",output);
	FILE **fin = malloc(sizeof(FILE *)*NF);
	FILE *fout = fopen(output,"w");
	for(i=0;i<NF;i++)
	{
		sprintf(input,"data/%d/sigma",i);
		printf("%s\n",input);
		fin[i] = fopen(input,"r");
		if (fin[i] == NULL) printf("Fatal File not found.\n");
	}
	double timetemp,speedtemp,vartemp;
	double time,speed,var;
	double *array = calloc(NF,sizeof(double));

	int end=0;
	while(!end){
		time=0.e0;speed=0.e0;var=0.e0;
		for(i=0;i<NF;i++)
		{
			if(fin[i] != NULL)
			{
				fscanf(fin[i],"%lg\t%lg\t%lg\n",&timetemp,&speedtemp,&vartemp);
				time  = timetemp;
				speed += speedtemp;
				var += vartemp;
			}
		}
		var /= NF;
		speed /= NF;
		fprintf(fout,"%g\t%g\t%g\t%g\n",time,speed,var,sqrt(var));
		for(i=0;i<NF;i++)
		{
			if(fin[i] != NULL)
			{
				if(feof(fin[i]))
					end = 1;
			}
		}
	}
	free(fin);
	free(fout);
	return 0;
}
