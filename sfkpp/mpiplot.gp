set term png size 720,720

set output "img/sigmaave.png"
p "data/sigma.a" u 1:3 w l ,\

set output "img/sigma.png"
p "data/0/sigma" u 1:3 w l ,\
  "data/1/sigma" u 1:3 w l ,\
  "data/2/sigma" u 1:3 w l ,\
  "data/3/sigma" u 1:3 w l ,\
  "data/4/sigma" u 1:3 w l ,\
  "data/5/sigma" u 1:3 w l ,\
  "data/6/sigma" u 1:3 w l ,\
  "data/7/sigma" u 1:3 w l 

set output "img/speed.png"
p "data/0/sigma" u 1:2 w l ,\
  "data/1/sigma" u 1:2 w l ,\
  "data/2/sigma" u 1:2 w l ,\
  "data/3/sigma" u 1:2 w l ,\
  "data/4/sigma" u 1:2 w l ,\
  "data/5/sigma" u 1:2 w l ,\
  "data/6/sigma" u 1:2 w l ,\
  "data/7/sigma" u 1:2 w l 

#do for [i=0:1000:1]{
#
#	set output "img/".i.".png"
#	p "data/0/stfac.".i u 1:2 w l ,\
#  	  "data/1/stfac.".i u 1:2 w l ,\
#  	  "data/2/stfac.".i u 1:2 w l ,\
#  	  "data/3/stfac.".i u 1:2 w l ,\
#  	  "data/4/stfac.".i u 1:2 w l ,\
#  	  "data/5/stfac.".i u 1:2 w l ,\
#  	  "data/6/stfac.".i u 1:2 w l ,\
#  	  "data/7/stfac.".i u 1:2 w l
#} 
