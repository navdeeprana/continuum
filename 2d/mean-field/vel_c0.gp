set term png
f="data/"
set fit quiet
set print "vel_co"
arr= "2.e-2 1.e-2 8.e-3 5.e-3 2.e-3 1.e-3 8.e-4 5.e-4 2.e-4 1.e-4"
do for [i=1:10:1]{
	set output "data/img.".i.".png"
		f(x) = a*(x**b)
		fit f(x) f."front_velocity.".i u 1:2 via a,b
		p f."front_velocity.".i u 1:2 w p pt 7 ps 1 ,f(x) w l lt -1
		print i,"	",word(arr,i),"  ",a,"	",b
		unset output
}
