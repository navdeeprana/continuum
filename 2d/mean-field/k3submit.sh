#!/bin/bash
#SBATCH --job-name mean-field
#SBATCH --time 48:00:00
#SBATCH --nodes 1
#SBATCH -n 20
`#SBATCH --output $1.out`
#module load mpi/gcc/mvapich2/2.1.0
module load mpi/gcc/openmpi/2.0.1
#make
mpirun -np 20 $1

