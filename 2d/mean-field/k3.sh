set -e
module load mpi/gcc/openmpi/2.0.1

declare -a arr=( "2.e-2" "1.e-2" "8.e-3" "5.e-3" "2.e-3" "1.e-3" "8.e-4" "5.e-4" "2.e-4" "1.e-4")
for i in {1..10}
do
	echo "make DBAC=${arr[$i-1]} ID=$i EXE=exe.$i"
			make DBAC=${arr[$i-1]} ID=$i EXE=exe.$i
done

for i in {1..10}
do
	sbatch k3submit.sh exe.$i
done
