#include <gsl/gsl_randist.h>
	
	long long unsigned int
poisson_nrc (const gsl_rng * r, double xm)
{
	static double sq, alxm, g;
	double em, t, y;

	if (xm < 12.e0) {
		g = exp (-xm);
		em = -1.e0;
		t = 1.e0;
		do {
			++em;
			t *= gsl_rng_uniform (r);
		}
		while (t > g);
	}
	else {
		sq = sqrt (2.e0 * xm);
		alxm = log (xm);
		g = xm * alxm - lgamma (xm + 1.e0);
		do {
			do {
				y = tan (M_PI * gsl_rng_uniform (r));
				em = sq * y + xm;
			}
			while (em < 0.e0);
			em = floor (em);
			t = 0.9e0 * (1.e0 + y * y) * exp (em * alxm - lgamma (em + 1.e0) - g);
		}
		while (gsl_rng_uniform (r) > t);
	}
	return (long long unsigned int) em;
}

	double
fokker_planck_rng (const gsl_rng * r1, const gsl_rng * r2, double rho, double lambda)
{
	if (rho <= 1.e-10) {
		return 0.e0;
	}
	else {
		double poisson_mean = lambda * rho;
		//unsigned int n = gsl_ran_poisson (r1, poisson_mean);
		long long unsigned int n = poisson_nrc (r1, poisson_mean);
		double randgamma = gsl_ran_gamma (r2, (double) n, 1);
		return randgamma / lambda;
	}
}

	void
stochastic (const gsl_rng * r1, const gsl_rng * r2, double lambda,
		double *bac, double *rho)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
			if (bac[idx] < 0.5e0 * rho[idx]) {
				bac[idx] = fokker_planck_rng (r1, r2, bac[idx], lambda);
			}
			else if (bac[idx]) {
				bac[idx] = rho[idx] - fokker_planck_rng (r1, r2, rho[idx] - bac[idx], lambda);
			}
		}
	}
}
