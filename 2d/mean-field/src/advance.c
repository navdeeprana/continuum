	void
set_boundary (double *array, int flag)
{
	int i, idx;
	if (flag) {
		for (i = 1; i < NGX - 1; i++) {
			array[i] = 0.e0;
			array[NG - 1 - i] = 0.e0;
		}
		for (i = 1; i < NGY - 1; i++) {
			array[i * NGX] = 0.e0;
			array[(i + 1) * NGX - 1] = 0.e0;
		}
	}
	else {
		for (i = 1; i < NGX - 1; i++) {
			array[i] = array[i + NGX];

			idx = NG - 1 - i;
			array[idx] = array[idx - NGX];
		}
		for (i = 1; i < NGY - 1; i++) {
			idx = i * NGX;
			array[idx] = array[idx + 1];

			idx = (i + 1) * NGX - 1;
			array[idx] = array[idx - 1];
		}
	}
	array[0] = 0.5e0 * (array[1] + array[NGX]);
	array[NGX - 1] = 0.5e0 * (array[NGX - 2] + array[2 * NGX - 1]);
	array[NG - 1] = 0.5e0 * (array[NG - 2] + array[NG - 1 - NGX]);
	array[NG - NGX] = 0.5e0 * (array[NG - NGX + 1] + array[NG - 2 * NGX]);
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i, j, idx;
	// To update only the inner grid, a double loop is
	// required. Boundary are updated by the boundary
	// conditions.
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
			rho[idx] += dt * drho[idx];
		}
	}
}
	void
adam_bashford_update (double dt, double *rho, double *drho, double *drhop,unsigned int flag)
{
	int i, j, idx;
	// At t=0 we don't have previous time's rho.
	if (flag==0){
		for (j = 1; j < NGY - 1; j++) {
			for (i = 1; i < NGX - 1; i++) {
				idx = INDEX (i, j);
				rho[idx] += dt * drho[idx];
				drhop[idx] = drho[idx];
			}
		}
	}
	else{
		for (j = 1; j < NGY - 1; j++) {
			for (i = 1; i < NGX - 1; i++) {
				idx = INDEX (i, j);
				rho[idx] += 0.5e0*dt * (3.e0*drho[idx]-drhop[idx]);
				drhop[idx] = drho[idx];
			}
		}
	}
}
