	void
file_grid_double_normalised (FILE * fid, double *arr1, double *arr2,
		double *arr3)
{
	int i, j, idx;
	for (j = 0; j < NGY; j++) {
		for (i = 0; i < NGX; i++) {
			idx = INDEX (i, j);
			fprintf (fid, "%3d\t%3d\t%1.6f\t%1.6f\n", i, j,
					arr1[idx] / arr3[idx], arr2[idx] / arr3[idx]);
		}
		fprintf (fid, "\n");
	}
	fprintf (fid, "\n");
	fflush (fid);
}

	void
file_grid_double (FILE * fid, double *arr1, double *arr2)
{
	int i, j, idx;
	for (j = 0; j < NGY; j++) {
		for (i = 0; i < NGX; i++) {
			idx = INDEX (i, j);
			fprintf (fid, "%3d\t%3d\t%g\t%g\n", i, j, arr1[idx], arr2[idx]);
		}
		fprintf (fid, "\n");
	}
	fprintf (fid, "\n");
	fflush (fid);
}

	void
save_restart_snapshot (FILE * fid, char *folder, int itime, int sr, int me,
		unsigned int *count, double *bac, double *food)
{
	if (itime % sr == 0 && itime > 0) {
		char name[64];
		sprintf (name, "%s/snapshot.%d_%d", folder, me, itime);
		fid = fopen (name, "w");
		file_grid_double (fid, bac, food);
		if (me == 0)
			printf ("snap saved at itime = %d\n", itime);
		fclose (fid);
		*count = 0;
	}
}

	void
save_snap (FILE * fid, char *folder, int *fc, double *bac, double *food,
		double *rho)
{
	char name[64];
	sprintf (name, "%s/snap.%d", folder, fc[0]);
	fid = fopen (name, "w");
	file_grid_double_normalised (fid, bac, food, rho);
	fclose (fid);
	*fc += 1;
}
