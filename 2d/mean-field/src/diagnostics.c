
	double
car_cap (double *bac, double *food, double *rho,double *totalfood)
{
	int i;
	double sum = ZERO;
	*totalfood=ZERO;
	for (i = 0; i < NG; i++) {
		sum += bac[i] / rho[i];
		*totalfood += food[i];
	}
	*totalfood /= NG;
	return sum / NG;
}

	double
hull (FILE * fid, double *bac, bool * grid, double bmin)
{
	int p, q, rdx;
	static double old;
	for (p = 0; p < NGY; p++) {
		for (q = 0; q < NGX; q++) {
			rdx = INDEX (q, p);
			if (bac[rdx] >= bmin)
				grid[rdx] = 1;
			else
				grid[rdx] = 0;
		}
	}
	int i = 0, j = NGY;
	int iold = 0, jold = 0;
	int idx = INDEX (i, j);
	int count = 0, length = 0, check = 0;
	while (check == 0) {
		j--;
		idx = INDEX (i, j);
		if (grid[idx] == 1) {
			check = (grid[idx + NGX] == 1 || grid[idx - NGX] == 1
					|| grid[idx + 1] == 1);
		}
	}
#ifdef PRINT_HULL
	fprintf (fid, "%d\t%d\n", i, j);
#endif
	length++;
	count++;

	unsigned short int im;
	unsigned short int move[] = { 0, 1, 2, 3 };
	while (i < NGX - 1 && count < NG) {
		iold = i;
		jold = j;
		idx = INDEX (i, j);
		im = 0, check = 0;
		while (im < 4) {
			if (move[im] == 0 && grid[idx + 1] == 1) {
				i++;
				move[0] = 3;
				move[1] = 0;
				move[2] = 1;
				move[3] = 2;
				break;
			}
			else if (move[im] == 1 && grid[idx - NGX] == 1) {
				j--;
				move[0] = 0;
				move[1] = 1;
				move[2] = 2;
				move[3] = 3;
				break;
			}
			else if (move[im] == 2 && grid[idx - 1] == 1) {
				i--;
				move[0] = 1;
				move[1] = 2;
				move[2] = 3;
				move[3] = 0;
				break;
			}
			else if (move[im] == 3 && grid[idx + NGX] == 1) {
				j++;
				move[0] = 2;
				move[1] = 3;
				move[2] = 0;
				move[3] = 1;
				break;
			}
			im++;
		}
		if (i != iold || j != jold) {
#ifdef PRINT_HULL
			fprintf (fid, "%d\t%d\n", i, j);
#endif
			length++;
		}
		count++;
	}
	do {
		idx = INDEX (i, j);
#ifdef PRINT_HULL
		fprintf (fid, "%d\t%d\n", i, j);
#endif
		length++;
		count++;
		j++;
	} while (grid[idx + NGX] == 1);
#ifdef PRINT_HULL
	fprintf (fid, "\n\n");
	fflush (fid);
#endif
	if (i == NGX - 1)
		old = (DX * length) / LENX;
	
	return old;
}

	double
height_statistics (double *bac, double *height)
{
	int i, j, idx;
	double variance = ZERO, mean = ZERO;
	for (i = 0; i < NGX; i++) {
		for (j = NGY - 1; j >= 0; j--) {
			idx = INDEX (i, j);
			if (bac[idx] >= 5.e-2) {
				height[i] = j * DY;
				break;
			}
		}
		mean += height[i];
	}
	mean /= NGX;

	for (i = 0; i < NGX; i++)
		variance += (mean - height[i]) * (mean - height[i]);

	variance /= NGX;
	return sqrt (variance);
}
