	void
housekeeping (int me, FILE ** fin, double *cbx, double *cby, double *cfx,
		double *cfy, double *dt, int *fr, int *sr, char *folder,
		char *name)
{
	//------------------------------FOLDERS--------------------//
	struct stat st = { 0 };
	sprintf (folder, "data");
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);

#ifdef SCAN	
	sprintf (folder, "data/%d",VID);
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);
#endif
	//------------------------------FILES----------------------//
	fin[0] = stdout;
	sprintf (name, "%s/stat.%d", folder,me);
	fin[1] = fopen (name, "w");
	sprintf (name, "%s/log.%d", folder,me);
	fin[2] = fopen (name, "w");

	*cbx = DBAC / (DX * DX);
	*cby = DBAC / (DY * DY);
	*cfx = DFOO / (DX * DX);
	*cfy = DFOO / (DY * DY);

	double fac = DTFAC;
	if (DX <= DY && DBAC >= DFOO)
		*dt = fac / cbx[0];
	else if (DX <= DY && DBAC < DFOO)
		*dt = fac / cfx[0];
	else if (DY <= DX && DBAC >= DFOO)
		*dt = fac / cby[0];
	else if (DY <= DX && DBAC < DFOO)
		*dt = fac / cfy[0];
	*fr = (int) ceil (1.e0 / (INTERVEL * dt[0]));
	*sr = (int) 100 *fr[0];
	fprintf (fin[2],"DBAC=%g\tINFOO=%g\tSTEP=%g\tFR=%d\tSR=%d\n",DBAC, (double) INFOO, dt[0], fr[0],sr[0]);
}
	
	void
set_initial_rho (double *rho, double *bac, double *food,int me)
{
	int i, j, idx;
	for (j = 0; j < NGY; j++) {
		for (i = 0; i < NGX; i++) {
			idx = INDEX (i, j);
	#ifdef GAUSSIAN
			double x = (i - NGX / 2) * DX;
			double y = (j - NGY / 2) * DY;
			bac[idx] = exp (-24 * (x * x + y * y));
	#else
			bac[idx] = 0.5e0 * (1.e0 - tanh (32.e0 * DY * (j - NGY / 32.e0)));
	#endif
			food[idx] = fabs (INFOO*(me+1) * (1.e0 - bac[idx]));
			rho[idx] = bac[idx] + food[idx];
		}
	}
}
