	void
calc_grad (double *rho, double *gradx, double *grady)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
#ifdef ISOTROPIC
			int n1, n2, n3, n4;
			n1 = INDEX (i + 1, j + 1);
			n2 = INDEX (i - 1, j + 1);
			n3 = INDEX (i - 1, j - 1);
			n4 = INDEX (i + 1, j - 1);
			gradx[idx] =
				(4.e0 * (rho[idx + 1] - rho[idx - 1]) +
				 (rho[n1] - rho[n2] - rho[n3] + rho[n4])) / (12.e0 * DX);
			grady[idx] =
				(4.e0 * (rho[idx + NGX] - rho[idx - NGX]) +
				 (rho[n1] + rho[n2] - rho[n3] - rho[n4])) / (12.e0 * DY);
#else
			gradx[idx] = (rho[idx + 1] - rho[idx - 1]) / (2.e0 * DX);
			grady[idx] = (rho[idx + NGX] - rho[idx - NGX]) / (2.e0 * DY);
#endif
		}
	}
}

	void
calc_div (double *rhox, double *rhoy, double *div)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
#ifdef ISOTROPIC
			int n1, n2, n3, n4;
			n1 = INDEX (i + 1, j + 1);
			n2 = INDEX (i - 1, j + 1);
			n3 = INDEX (i - 1, j - 1);
			n4 = INDEX (i + 1, j - 1);
			div[idx] =
				(4.e0 * (rhox[idx + 1] - rhox[idx - 1]) +
				 (rhox[n1] - rhox[n2] - rhox[n3] + rhox[n4])) / (12.e0 * DX)
				+ (4.e0 * (rhoy[idx + NGX] - rhoy[idx - NGX]) +
						(rhoy[n1] + rhoy[n2] - rhoy[n3] - rhoy[n4])) / (12.e0 * DY);
#else
			div[idx] = (rhox[idx + 1] - rhox[idx - 1]) / (2.e0 * DX)
				+ (rhoy[idx + NGX] - rhoy[idx - NGX]) / (2.e0 * DY);
#endif
		}
	}
}

	void
calc_diff (double constx, double consty, double *rho, double *diff)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
#ifdef ISOTROPIC
			int n1, n2, n3, n4;
			n1 = INDEX (i + 1, j + 1);
			n2 = INDEX (i - 1, j + 1);
			n3 = INDEX (i - 1, j - 1);
			n4 = INDEX (i + 1, j - 1);
			diff[idx] =
				(constx / 12.e0) * (10.e0 *
						(rho[idx + 1] + rho[idx - 1] - 2.e0 * rho[idx]) +
						(rho[n1] + rho[n2] + rho[n3] + rho[n4] -
						 2.e0 * rho[idx + NGX] - 2.e0 * rho[idx - NGX]));
			diff[idx] +=
				(consty / 12.e0) * (10.e0 *
						(rho[idx + NGX] + rho[idx - NGX] -
						 2.e0 * rho[idx]) + (rho[n1] + rho[n2] + rho[n3] +
							 rho[n4] - 2.e0 * rho[idx +
							 1] -
							 2.e0 * rho[idx - 1]));
#else
			diff[idx] =
				constx * (rho[idx + 1] + rho[idx - 1] - 2.e0 * rho[idx]) +
				consty * (rho[idx + NGX] + rho[idx - NGX] - 2.e0 * rho[idx]);
#endif
		}
	}
}
