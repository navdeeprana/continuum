#include"parameters.h"
#include"src/advance.c"
#include"src/data.c"
#include"src/derivatives.c"
#include"src/diagnostics.c"
#include"src/houskeeping.c"
	int
main (int argc, char **argv)
{
#ifdef MPI
	int NP, me;
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);
#else
	int me = 0;
#endif
	int i, j, idx, fr,sr;
	double dt, cbx, cby, cfx, cfy;
	char folder[32], name[32];
	FILE **fin = malloc (sizeof (FILE *) * 4);
	housekeeping (me, fin, &cbx, &cby, &cfx, &cfy, &dt, &fr, &sr, folder, name);

	double *rho = calloc (NG, sizeof (double));
	double *drho = calloc (NG, sizeof (double));
	double *bac = calloc (NG, sizeof (double));
	double *dbac = calloc (NG, sizeof (double));
	double *food = calloc (NG, sizeof (double));
	double *dfood = calloc (NG, sizeof (double));

	set_initial_rho (rho, bac, food,me);

	double food_0=ZERO, food_T=ZERO,carcap=ZERO;
	carcap = car_cap(bac,food,rho,&food_0);
	int itime = 0;

	while (itime * dt <= SIMTIME) {
		if (itime % (fr) == 0) {
			carcap = car_cap (bac,food,rho,&food_T);
			fprintf (fin[1], "%g\t%g\t%g\n", dt * itime,carcap,food_T);
			fflush(fin[1]);
			if (carcap > 0.9 || food_T < 0.001*food_0) {
				printf ("Bac full or food shortage.\n");
				break;
			}
			fprintf (fin[2],"\rtime = %g\tfood_T=%2.3f", itime * dt, 100*(food_T/food_0));
			fflush (fin[2]);
		}
		calc_diff (cfx, cfy, food, dfood);
		calc_diff (cbx, cby, bac, dbac);
		for (j = 1; j < NGY - 1; j++) {
			for (i = 1; i < NGX - 1; i++) {
				idx = INDEX (i, j);
				drho[idx] = dbac[idx] + dfood[idx];
				dbac[idx] += GAMMA*bac[idx]*(rho[idx]-bac[idx]);
			}
		}
		euler_update (dt, bac, dbac);
		euler_update (dt, rho, drho);
		for (j = 1; j < NGY - 1; j++) {
			for (i = 1; i < NGX - 1; i++) {
				idx = INDEX (i, j);
				food[idx] = rho[idx] - bac[idx];
			}
		}
		set_boundary (bac,0);
		set_boundary (food,0);
		for (i = 1; i < NGX - 1; i++) {
			rho[i] = bac[i] + food[i];
			rho[NG - 1 - i] = bac[NG - 1 - i] + food[NG - 1 - i];
		}
		for (i = 1; i < NGY - 1; i++) {
			rho[i * NGX] = bac[i * NGX] + food[i * NGX];
			rho[(i + 1) * NGX - 1] =
				bac[(i + 1) * NGX - 1] + food[(i + 1) * NGX - 1];
		}

		itime++;
	}
	free (rho);
	free (drho);
	free (food);
	free (dfood);
	free (bac);
	free (dbac);
	fcloseall ();
	fprintf (fin[2],"\n%2.2f EOS. Clean exit on rank %d\n.",(food_T/food_0)*100,me);
#ifdef MPI
	MPI_Finalize ();
#endif
	return 0;
}
