#set term qt persist noraise size 600,600
set term qt noraise size 600,600
folder='data'
plot folder.'/stat.0' u 1:2 w lp ps 0.5 pt 7 t "",\
	  folder.'/stat.1' u 1:2 w lp ps 0.5 pt 7 t "",\
	  folder.'/stat.2' u 1:2 w lp ps 0.5 pt 7 t "",\
	  folder.'/stat.3' u 1:2 w lp ps 0.5 pt 7 t "",\
	  folder.'/stat.4' u 1:2 w lp ps 0.5 pt 7 t "",\
	  folder.'/stat.5' u 1:2 w lp ps 0.5 pt 7 t ""

pause -1
