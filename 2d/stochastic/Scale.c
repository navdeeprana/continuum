#include<stdio.h>
#include<stdlib.h>
#include<math.h>
int main(int argc, char **argv)
{
	int i;
	FILE *fid = fopen("slope","r");
	char input[64],output[64];
	if(fid != NULL){
		sprintf(input,"data/%d/stat.a",atoi(argv[1]));
		sprintf(output,"data/%d/stat.as",atoi(argv[1]));
		FILE *fin = fopen(input,"r");
		FILE *fout = fopen(output,"w");

		double *speed = calloc(NFOOD,sizeof(double));
		double *food = calloc(NFOOD,sizeof(double));

		double g1,g2;
		while(!feof(fid)){
			for(i=0;i<NFOOD;i++)
				fscanf(fid,"%lg\t%lg\t%lg\t%lg\n",&food[i],&speed[i],&g1,&g2);
		}
		i = atoi(argv[1]);
		double time,area,hull;
		while(!feof(fin)){
			time=0.e0;area=0.e0;hull=0.e0;
			fscanf(fin,"%lg\t%lg\t%lg\n",&time, &area,&hull);
			fprintf(stdout,"%g\t%g\t%g\n",time, area,hull);
			time  *= speed[i-1];
			fprintf(fout,"%g\t%g\t%g\n",time,area,hull);
		}
	}
}
