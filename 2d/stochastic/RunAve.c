/*
 * This version does not have any scaling and squares and roots errors and does average for files. Just define NF ( number of files) and run it with the folder as argument.
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define LEN (512.e0)
#define NGX (4096)

int main(int argc, char **argv)
{
	int i,flag=1;
	double dx=LEN/NGX;
	char input[64],output[64];
	sprintf(input,"data/%d",atoi(argv[1]));
	sprintf(output,"data/%d/stat.a",atoi(argv[1]));
#ifdef VERBOSE
	printf("%s\n",input);
	printf("%s\n",output);
#endif
	FILE **fin = malloc(sizeof(FILE *)*NF);
	FILE *fout = fopen(output,"w");
	FILE *fid = fopen("data/stat_at_t","a");
	char file[64];
	int count=0;
	for(i=0;i<NF;i++)
	{
		sprintf(file,"%s/stat.%d",input,i);
		fin[i] = fopen(file,"r");
		if (fin[i] == NULL) printf("Fatal File not found.\n");
		else count++;
	}
	printf("files to be averaged = %d\n",count);
	double timetemp,areatemp,stattemp,hulltemp;
	double time,area,stat,hull;
	int end=0;
	while(!end){
		time=0.e0;area=0.e0;stat=0.e0;hull=0.e0;
		for(i=0;i<NF;i++)
		{
			if(fin[i] != NULL)
			{
				fscanf(fin[i],"%lg\t%lg\t%lg\t%lg\n",&timetemp,&areatemp,&stattemp,&hulltemp);
				time  = timetemp;
				area += areatemp;
				stat += stattemp;
				hull += hulltemp;
			}
		}
		area /= count;
		hull /= count;
		stat = sqrt(stat/count);
		fprintf(fout,"%g\t%g\t%g\t%g\n",time,area,stat,hull);
		for(i=0;i<NF;i++)
		{
			if(fin[i] != NULL)
			{
				if(feof(fin[i]))
					end = 1;
			}
		}
	}
	free(fin);
	free(fout);
	free(fid);

	return 0;
}
