#set term qt noraise size 63,511
set term jpeg giant enhanced font arielx100 size 256,1024 
set xrange [0:79]
set yrange [0:639]
set cbrange [0:1]
set notics
set palette defined (0 "black",1 "yellow")
folder='data'
unset colorbox
do for [i=0:1000:1]{
	set output "img/".i.".jpeg"
	p folder.'/0/snap.'.i index 0 u 1:2:3 w p pt 5 palette ps 1 t "",\
}
