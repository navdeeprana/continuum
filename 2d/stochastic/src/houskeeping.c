	void
housekeeping (int me, FILE ** fin, double *cbx, double *cby, double *cfx,
		double *cfy, double *dt, int *fr, int *sr, char *folder,
		char *name)
{
	//------------------------------FOLDERS--------------------//
	struct stat st = { 0 };
	sprintf (folder, "data");
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);
#ifdef SCAN
	sprintf (folder, "data/%d", VID);
#endif
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);

	//------------------------------FILES----------------------//
	char flag[2];
#ifdef RESTART
	sprintf (flag, "a");
	sprintf (name, "%s/snapshot.%d_%d", folder, me, RTIME);
	fin[3] = fopen (name, "r");
	if (fin[3] != NULL)
		printf ("Opened file to read.\n");
	else
		exit (0);
#else
	sprintf (flag, "w");
#endif
	fin[0] = stdout;
	sprintf (name, "%s/stat.%d", folder, (int) INFOO);
	fin[1] = fopen (name, flag);
	sprintf (name, "%s/hull.%d", folder, (int) INFOO);
	fin[2] = fopen (name, flag);
	//-------------------VARIABLES--------------------//

	*cbx = DBAC / (DX * DX);
	*cby = DBAC / (DY * DY);
	*cfx = DFOO / (DX * DX);
	*cfy = DFOO / (DY * DY);

#ifdef NONLINEAR
	double fac = DTFAC / INFOO;
	printf ("Scaling dt\t%g\n", fac);
#else
	double fac = DTFAC;
#endif
	if (DX <= DY && DBAC >= DFOO)
		*dt = fac / cbx[0];
	else if (DX <= DY && DBAC < DFOO)
		*dt = fac / cfx[0];
	else if (DY <= DX && DBAC >= DFOO)
		*dt = fac / cby[0];
	else if (DY <= DX && DBAC < DFOO)
		*dt = fac / cfy[0];
	*fr = (int) ceil (1.e0 / (INTERVEL * dt[0]));
	*sr = (int) 100 *fr[0];
	if (me == 0)
		printf ("INFOO=%g\tSTEP=%g\tFR=%d\tSR=%d\n", (double) INFOO, dt[0], fr[0],
				sr[0]);
}
	
	void
set_initial_rho (double *rho, double *bac, double *food, FILE * fid)
{
#ifdef RESTART
	int i, j, idx;
	double tempbac = ZERO, tempfood = ZERO;
	printf ("Loading Snap saved at %d\n", RTIME);
	while (!feof (fid)) {
		fscanf (fid, "%d\t%d\t%lg\t%lg\n", &i, &j, &tempbac, &tempfood);
		idx = INDEX (i, j);
		bac[idx] = tempbac;
		food[idx] = tempfood;
		rho[idx] = bac[idx] + food[idx];
	}
#else
	int i, j, idx;
	for (j = 0; j < NGY; j++) {
		for (i = 0; i < NGX; i++) {
			idx = INDEX (i, j);
	#ifdef GAUSSIAN
			double x = (i - NGX / 2) * DX;
			double y = (j - NGY / 2) * DY;
			bac[idx] = exp (-24 * (x * x + y * y));
	#else
			bac[idx] = 0.5e0 * (1.e0 - tanh (32.e0 * DY * (j - NGY / 32.e0)));
	#endif
			food[idx] = fabs (INFOO * (1.e0 - bac[idx]));
			rho[idx] = bac[idx] + food[idx];
		}
	}
#endif
}
