for f in *.eps
do
	name=${f%.eps}"jpg"
	convert -density 300 $f -resize 512x512 $name
done
