#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <gsl/gsl_randist.h>

#ifdef MPI
#include <mpi.h>
#endif


#define zero (0.e0)
#define ng (160)
#define len (8.e0)
#define simtime (1.e4)

#define Dfoo  (1.e-1)
#define Dbac  (1.e-4)

#define gamma (1.e0)
#define SIGMA (5.e-2)

#define INFOO (10.e0)

#define dtfac (1.e-1)
#define INTERVEL (1)

	void
file_grid_double_normalised (FILE * fid, double *arr1, double *arr2,double *arr3)
{
	int i;
	for (i = 0; i < ng; i++) {
		fprintf (fid, "%d\t%g\t%g\n", i, arr1[i]/arr3[i],arr2[i]/arr3[i]);
	}
	fprintf (fid, "\n\n");
	fflush (fid);
}

	void
set_initial_rho (double dx, double *rho, double *bac,double *food,int fac)
{
	int i;
	for (i = 0; i < ng; i++) {
		bac[i] = 0.5e0 * (1.e0 - tanh (32.e0 * dx * (i - ng / 32)));
		food[i] = INFOO*fac*(1.e0 - bac[i]);
		rho[i] = bac[i] + food[i];
	}
}

	void
calc_grad (double dx, double *rho, double *gradx)
{
	int i;
	for (i = 1; i < ng - 1; i++) {
		gradx[i] = (rho[i + 1] - rho[i - 1]) / (2.e0 * dx);
	}
	gradx[0] = 0.e0;
	gradx[ng-1] = 0.e0;
}
	void
calc_diff (double constx, double *rho, double *diff)
{
	int i;
	for (i = 1; i < ng - 1; i++) {
		diff[i] =
			constx * (rho[i + 1] + rho[i - 1] - 2.e0 * rho[i]); 
	}
	diff[0] = 0.e0;
	diff[ng-1] = 0.e0;
}

	long long unsigned int
poisson_nrc (const gsl_rng * r, double xm)
{
	static double sq, alxm, g;
	double em, t, y;

	if (xm < 12.e0) {
		g = exp (-xm);
		em = -1.e0;
		t = 1.e0;
		do {
			++em;
			t *= gsl_rng_uniform (r);
		}
		while (t > g);
	}
	else {
		sq = sqrt (2.e0 * xm);
		alxm = log (xm);
		g = xm * alxm - lgamma (xm + 1.e0);
		do {
			do {
				y = tan (M_PI * gsl_rng_uniform (r));
				em = sq * y + xm;
			}
			while (em < 0.e0);
			em = floor (em);
			t = 0.9e0 * (1.e0 + y * y) * exp (em * alxm - lgamma (em + 1.e0) - g);
		}
		while (gsl_rng_uniform (r) > t);
	}
	return (long long unsigned int) em;
}

	double
integrate_the_stochastic_part (const gsl_rng * r1, const gsl_rng * r2,
		double rho, double lambda)
{
	if(rho==0.e0){
		return 0.e0;
	}
	else{
		double poisson_mean = lambda * rho;
		//unsigned int n = gsl_ran_poisson (r1, poisson_mean);
		long long unsigned int n = poisson_nrc(r1,poisson_mean);
		double randgamma = gsl_ran_gamma (r2, (double) n, 1);
		return randgamma / lambda;
	}
}

	double
logistic (double bac,double rho,int fac)
{
	return fac*gamma * bac * (rho - bac);
}

	void
set_boundary (double *array)
{
	array[0] = array[1];
	array[ng-1]=array[ng-2];
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i;
	for (i = 1; i < ng - 1; i++) {
		rho[i] += dt * drho[i];
	}
}
	void
makedir()
{
	struct stat st = {0};
	if (stat("data", &st) == -1) 
		mkdir("data", 0700);
}
	double
car_cap(double *bac, double *rho)
{
	int i;
	double sum=zero;
	for(i=0;i<ng;i++)
		sum += bac[i]/rho[i];

	return sum/ng;
}
	int
main (int argc, char **argv)
{
	srand(time(NULL));
	int me=0;
	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	gsl_rng_set (r1, rand ());
	gsl_rng_set (r2, rand ());

	int i;
	double dx = len / ng;
	double constbacx = Dbac / (dx * dx);
	double constfoox = Dfoo / (dx * dx);
	double dt;
	if (Dbac >= Dfoo)
		dt = dtfac / constbacx;
	else if (Dbac < Dfoo)
		dt = dtfac / constfoox;

	int framerate = (int) ceil(1.e0/(INTERVEL*dt));

	double *rho = calloc (ng, sizeof (double));
	double *drho = calloc (ng, sizeof (double));
	double *bac = calloc (ng,sizeof(double));
	double *dbac = calloc (ng,sizeof(double));	
#ifdef NONLIN
	double *gbacx = calloc (ng,sizeof(double));	
#endif
	double *food = calloc (ng, sizeof (double));
	double *dfood = calloc (ng, sizeof (double));

	char folder[32];
	makedir();
	sprintf (folder,"data");
	char stdfile[32];
	sprintf (stdfile,"%s/stddev.%d",folder,me);
	FILE *fidstd = fopen(stdfile,"w");
	set_initial_rho (dx, rho,bac, food,1);
	int itime = 0;
	double lambda = 2.0e0 / (SIGMA * SIGMA * dt);
	char file[32];
	FILE *fid;
	double carcap = 0.e0;
	int fc=0;
	while (itime * dt <= simtime) {
		if (itime % framerate == 0){
			if(me==0) printf ("\r%g", itime*dt);
			fflush (stdout);
			carcap = car_cap(bac,rho);
			sprintf ( file,"%s/snap.%d",folder,fc);
			fid = fopen(file,"w");
			fprintf(fid,"##---%d---%g---###\n\n",itime,itime*dt);
			file_grid_double_normalised(fid, bac,food,rho);
			fclose(fid);
			fc++;
			if(carcap > 0.99){
				printf("BOX FULL.\t");
				break;
			}
		}
		for (i = 0; i < ng; i++) {
			if (bac[i] < 0.5e0*rho[i]){
				bac[i] =
					integrate_the_stochastic_part (r1, r2, bac[i], lambda);
			}
			else if(bac[i]){
				bac[i] =
					rho[i] - integrate_the_stochastic_part (r1, r2,
							rho[i] - bac[i],
							lambda);
			}
		}
	calc_diff (constfoox, food, dfood);
#ifdef NONLIN
	calc_grad (dx,bac,gbacx);
	calc_diff (constbacx,bac, dbac);
	for (i = 1; i < ng - 1; i++) {
		dbac[i] = bac[i]*dbac[i]+Dbac*gbacx[i]*gbacx[i];
	}
#else
	calc_diff (constbacx,bac, dbac);
#endif
	for (i = 1; i < ng - 1; i++) {
		drho[i] = dbac[i]+dfood[i];
	}
	for (i = 1; i < ng - 1; i++) {
		dbac[i] += logistic (bac[i],rho[i],1);
	}
	euler_update (dt, bac, dbac);
	euler_update (dt, rho, drho);
	for (i = 1; i < ng - 1; i++) {
		food[i] = rho[i]-bac[i];
	}
	set_boundary (bac);
	set_boundary (food);
	set_boundary (rho);
	itime++;
	}
	free (rho);
	free (drho);
	free (food);
	free (dfood);
	free (bac);
	free (dbac);
	fcloseall();	
	printf("EOS. Clean exit on rank %d\n.",me);
#ifdef MPI
	MPI_Finalize();
#endif
	return 0;
}
