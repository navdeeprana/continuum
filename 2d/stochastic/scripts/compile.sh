args="-Wall -D_GNU_SOURCE -DMPI"
include="/usr/include"
lib="/usr/lib"
lib64="/usr/lib64"
flags="-lgsl -lsatlas -lm -O3"

src="Fisher2d.c"
mkdir data
cp compile.sh data/.
module load mpi/gcc/openmpi/2.0.1
declare -a food=("2.e0" "4.e0" "6.e0" "8.e0" "10.e0" "12.e0" "14.e0" "16.e0" "18.e0" "20.e0" "22.e0" "24.e0" "26.e0" "28.e0" "30.e0" "32.e0")
for i in {1..16}
do
	echo "mpicc $args -D INFOO=${food[$i-1]} -D VID=$i -D INTERVEL=1 -I$include/gsl/ -I$include/atlas/ -L$lib -L$lib64/atlas $src $flags -o run.$i"
			mpicc $args -D INFOO=${food[$i-1]} -D VID=$i -D INTERVEL=1 -I$include/gsl/ -I$include/atlas/ -L$lib -L$lib64/atlas $src $flags -o run.$i
done
module load mpi/gcc/openmpi/2.0.1
module list
#for i in {1..16}
#do
#	sbatch sj.sh run.$i
#done
