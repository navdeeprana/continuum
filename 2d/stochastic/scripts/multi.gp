set term jpeg giant enhanced font arielx100 size 1024,1024 
set xrange [0:159]
set yrange [0:159]
set cbrange [0:1]
#set palette defined (0 "white", 1 "#339900")
#set palette defined (0 "white", 1 "#CC9900")
f='dump'
unset colorbox
do for [i=0:2000:1]{
	set output "img/".i.".jpeg"
	set multiplot layout 2,2 rowsfirst
	
	set label 1 "1  D{/Symbol D}^2b" at 110,110 centre tc rgb "white" font ",30" front
	p f.'/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t "" 
	
	set label 1 "2  D{/Symbol D}.(c{/Symbol D}b)" at 110,110 centre tc rgb "white" front
	p '1/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t ""
	
	set label 1 "3  D{/Symbol D}.(b{/Symbol D}b)" at 110,110 centre tc rgb "white" front
	p '2/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t ""
	
	set label 1 "4  D{/Symbol D}.(cb{/Symbol D}b)" at 110,110 centre tc rgb "white" front
	p '3/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t ""
	unset multiplot
	unset output
}
