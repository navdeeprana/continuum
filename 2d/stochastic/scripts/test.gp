#set term qt noraise size 600,600
set term jpeg giant enhanced font arielx100 size 1024,1024 
set xrange [0:159]
set yrange [0:159]
set cbrange [0:0.1]
set notics
#set palette defined (0 "black",1 "yellow")
#load '~/Downloads/palettes/viridis.pal'
folder='data'
unset colorbox
do for [i=0:2000:5]{
	set output "img/".i.".jpeg"
	p folder.'/snap.'.i index 0 u 1:2:3 w p pt 5 palette ps 1 t "",\
	  folder.'/snap.'.i index 1 u 1:2 w p pt 7 lt 22 ps 0.8 t "",\
}
