set term qt noraise size 600,600
set xrange [0:160]
set yrange [0:160]
set notics
#set palette defined (0 "black", 1 "#339900")
set palette defined (0 "black", 1 "#CC9900")
folder='data'
unset colorbox
do for [i=0:2000:1]{
	#p folder.'/snap.'.i u 1:2:3 w p pt 5 palette ps 1 
	splot folder.'/snap.'.i u 1:2:3 every 5:10 w p lt 9 pt 7
	pause 0.1

}
