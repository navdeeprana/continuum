#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define LEN (1.e0)
int main(int argc, char **argv)
{
	int i;
	char input[64],output[64];
	sprintf(input,"data/%d",atoi(argv[1]));
	sprintf(output,"data/%d/stddev.a",atoi(argv[1]));
	printf("%s\n",input);
	printf("%s\n",output);
	FILE **fin = malloc(sizeof(FILE *)*NF);
	FILE *fout = fopen(output,"w");
	char file[64];
	for(i=0;i<NF;i++)
	{
		sprintf(file,"%s/stddev.%d",input,i);
		fin[i] = fopen(file,"r");
		if (fin[i] == NULL) printf("Fatal File not found.\n");
	}
	double timetemp,speedtemp,stddevtemp,temp1;
	double time,speed,stddev,temp2;
	int end=0;
	while(!end){
		time=0.e0;speed=0.e0;stddev=0.e0,temp2=0.e0;
		for(i=0;i<NF;i++)
		{
			if(fin[i] != NULL)
			{
				fscanf(fin[i],"%lg\t%lg\t%lg\n",&timetemp,&speedtemp,&stddevtemp);
				time  = timetemp;
				speed += speedtemp;
				stddev += stddevtemp;
				temp1 = (stddevtemp*stddevtemp)/LEN;
				temp2 += temp1;
			}
		}
		speed /= NF;
		stddev /= NF;
		temp2 = sqrt(temp2/NF);
		fprintf(fout,"%g\t%g\t%g\t%g\n",time,speed,stddev,temp2);
		for(i=0;i<NF;i++)
		{
			if(fin[i] != NULL)
			{
				if(feof(fin[i]))
					end = 1;
			}
		}
	}
	free(fin);
	free(fout);
	return 0;
}
