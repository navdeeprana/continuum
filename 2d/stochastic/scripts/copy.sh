mkdir copydata
dest=$PWD/copydata
mv slurm* $dest/
cp Fisher2d.c parameters.h $dest/
for i in {1..16}
do
	mkdir $dest/$i
	cd $HOME/scan/diffusion/data/$i
	cp stddev.* $dest/$i/.
done
