set term jpeg giant enhanced font arielx100 size 1024,1024
set xrange [0:159]
set yrange [0:159]
set cbrange [0:1]
set palette defined (0 "yellow",1 "black")
unset colorbox
set output "grid.jpeg"
p 'data/hull.0' index 0  u 1:2:3 w p pt 5 palette ps 1 t "",\
  'data/hull.0' index 1  u 1:2 w p pt 7 ps 0.5 t "",
#pause -1
