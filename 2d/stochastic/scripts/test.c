#include "parameters.h"
	void
housekeeping (double *cbx, double *cby, double *dt, int *fr, char *folder, char *name)
{
	struct stat st = {0};
	sprintf (folder, "data");
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);
	*cbx = DBAC / (DX * DX);
	*cby = DBAC / (DY * DY);
	if (DX <= DY)
		*dt = DTFAC / cbx[0];
	else if (DY <= DX)
		*dt = DTFAC / cby[0];
	
	*fr = (int) ceil (1.e0 / (INTERVEL * dt[0]));
	printf ("INFOO=%g\tSTEP=%g\tFRAMES=%d\n", INFOO, dt[0], fr[0]);
}

	void
file_grid_double(FILE * fid, double *arr1)
{
	int i, j, idx;
	for (j = 0; j < NGY; j++) {
		for (i = 0; i < NGX; i++) {
			idx = INDEX (i, j);
			fprintf (fid, "%3d\t%3d\t%1.6f\n", i, j, arr1[idx]);
		}
	}
	fprintf (fid, "\n\n");
	fflush (fid);
}
	
	void
set_boundary (double *array, int flag)
{
	int i,idx;
	if (flag) {
		for (i = 1; i < NGX - 1; i++) {
			array[i] = 0.e0;
			array[NG - 1 - i] = 0.e0;
		}
		for (i = 1; i < NGY - 1; i++) {
			array[i * NGX] = 0.e0;
			array[(i + 1) * NGX - 1] = 0.e0;
		}
	}
	else {
		for (i = 1; i < NGX - 1; i++) {
			array[i] = 2.e0*array[i + NGX]-array[i+2*NGX];
			idx = NG - 1 - i;
			array[idx] = 2.e0*array[idx-NGX] - array[idx - 2*NGX];
		}
		for (i = 1; i < NGY - 1; i++) {
			idx = i*NGX;
			array[idx] = 2.e0*array[idx+ 1] - array[idx+2];
			idx = (i+1)*NGX - 1;
			array[idx] = 2.e0*array[idx-1] - array[idx-2];
		}
	}
}

	void
set_initial_rho (double *bac)
{
	int i, j, idx;
	for (j = 0; j < NGY; j++) {
		for (i = 0; i < NGX; i++) {
			idx = INDEX (i, j);
			double x = (i-NGX/2)*DX;
			  double y = (j-NGY/2)*DY;
			  bac[idx] = exp(-0.5*(x*x+y*y));
		}
	}
}

	void
calc_grad (double *rho, double *gradx, double *grady)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
			gradx[idx] = (rho[idx + 1] - rho[idx - 1]) / (2.e0 * DX);
			grady[idx] = (rho[idx + NGX] - rho[idx - NGX]) / (2.e0 * DY);
		}
	}
}

	void
calc_div (double *rhox, double *rhoy, double *div)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
			div[idx] =
				(rhox[idx + 1] - rhox[idx - 1]) / (2.e0 * DX) + (rhoy[idx + NGX] -
						rhoy[idx -
						NGX]) / (2.e0 *
							DY);
		}
	}
}

	void
calc_diff (double constx, double consty, double *rho, double *diff)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
			diff[idx] =
				constx * (rho[idx + 1] + rho[idx - 1] - 2.e0 * rho[idx]) +
				consty * (rho[idx + NGX] + rho[idx - NGX] - 2.e0 * rho[idx]);
		}
	}
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
			rho[idx] += dt * drho[idx];
		}
	}
}
	int
main (int argc, char **argv)
{
	int i, j, idx, fr;
	double dt, cbx, cby;
	char folder[32], name[32];
	housekeeping (&cbx, &cby, &dt, &fr, folder, name);

	double *bac = calloc (NG, sizeof (double));
	double *dbac = calloc (NG, sizeof (double));

	set_initial_rho (bac);
	FILE *fid;
	int itime = 0, fc = 0;
	while (itime * dt <= SIMTIME) {
		if (itime % fr == 0) {
			sprintf (name, "%s/snap.%d", folder, fc);
			fid = fopen (name, "w");
			file_grid_double(fid, bac);
			fclose (fid);
			fc++;
				fprintf (stdout,"\r%g", itime * dt);
				fflush (stdout);
		}
		calc_diff (cbx, cby, bac, dbac);
		euler_update (dt, bac, dbac);
		set_boundary (bac,0);
		itime++;
	}
	free (bac);
	free (dbac);
	return 0;
}
