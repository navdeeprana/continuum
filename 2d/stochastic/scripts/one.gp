set term qt noraise size 1000,1000
set xrange [0:159]
set yrange [0:159]
set notics
set cbrange [0:1]
#set palette defined (0 "black", 1 "#339900")
#set palette defined (0 "black", 1 "#CC9900")
load '~/Downloads/palettes/'.pal
folder='data'
unset colorbox
p folder.'/snap.100' index 0 u 1:2:3 w p pt 5 palette ps 1
pause -1
