
#ifndef __parameters_h__
#define __parameters_h__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define NG (512)
#define LEN (8.e0)
#define SIMTIME (1.e3)

#define DBAC  (1.e-1)
#define INTERVEL (1)
#endif
	void
file_double (FILE * fid, double factor, double *arr1)
{
	int i;
	for (i = 0; i < NG; i++)
		fprintf (fid, "%g\t%f\t%f\n", factor * i, arr1[i]);

	fprintf (fid, "\n\n");
	fflush (fid);
}
	void
set_initial_rho (double dx,double *rho)
{
	int i;
	double x;
	for (i = 0; i < NG; i++) {
		x = (i-NG/2)*dx;
		rho[i] = exp(-x*x);
	}
}
	void
calc_diff (double constant, double *rho, double *diff)
{
	int i;
	for (i = 1; i < NG - 1; i++)
		diff[i] = constant * (rho[i + 1] + rho[i - 1] - 2.e0 * rho[i]);
}
	void
set_bound (double *end, double pend, double ppend)
{
	*end = 2.e0*pend -ppend;
}
	void
euler_update (double dt, double *rho, double *drho)
{
	int i;
	for (i = 1; i < NG - 1; i++)
		rho[i] += dt * drho[i];
}
	int
main (int argc, char **argv)
{
	int i;
	double dx = LEN / NG;
	double constbac = DBAC / (dx * dx);
	double dt = 1.e-2 / constbac;

	int freq = (int) ((1.0 / (INTERVEL * dt)));
	double *bac = calloc (NG, sizeof (double));
	double *dbac = calloc (NG, sizeof (double));
	FILE *fid;
	char file[64];
	set_initial_rho (dx,bac);
	int itime = 0,fc=0;
	while (itime * dt <= SIMTIME) {
		if (itime % (freq) == 0) {
			printf ("\r%d", itime / freq);
			fflush (stdout);
			sprintf ( file,"data/snap.%d",fc);
			fid = fopen(file,"w");
			file_double(fid,dx, bac);
			fclose(fid);
			fc++;
		}
		calc_diff (constbac, bac, dbac);
		euler_update (dt, bac, dbac);
		set_bound (&bac[0], bac[1],bac[2]);
		set_bound (&bac[NG-1], bac[NG-2],bac[NG-3]);
		itime++;
	}
	free (bac);
	free (dbac);
	return 0;
}
