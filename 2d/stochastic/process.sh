rm slope average scale data/stddev_at_t
set -e
NF=8
NFOOD=10
echo "Ensemble Averaging,set QUIET for no output"
echo "gcc -D NF=$NF -D NFOOD=$NFOOD RunAve.c -lm -o average"
gcc -D NF=$NF -D NFOOD=$NFOOD RunAve.c -lm -o average
echo "Compiled"
for i in {10..1}
do
	./average $i
done
echo "Finding Slopes"
echo "gnuplot slope.gp"
gnuplot slope.gp

echo "Scaling time with velocities."
echo "gcc -D NFOOD=$NFOOD Scale.c -lm -o scale"
gcc -D NFOOD=$NFOOD Scale.c -lm -o scale
echo "Compiled"
for i in {10..1}
do
	./scale $i
	echo $i
done
exit


