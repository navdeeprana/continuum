#!/bin/bash
#$-pe orte 20
#$ -N the.crazy.one
#$ -e error
#$ -o log
#$ -S /bin/bash
#$ -q all.q
#$ -cwd
source /hpcapps/GCC/gsl23/gsl23
/hpcapps/mpi/openmpi/gcc/1.6.5/bin/mpirun -np 16 $1

