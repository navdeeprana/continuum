set -e
#module load mpi/gcc/openmpi/2.0.1

#declare -a food=("1.e-1" "2.e0" "3.e0" "4.e0" "5.e0" "6.e0" "7.e0" "7.e0" "18.e0" "20.e0" "22.e0" "24.e0" "26.e0" "28.e0" "30.e0" "32.e0")
for i in {1..20}
do
	echo "make C0=$i ID=$i EXE=exe.$i"
			make C0=$i ID=$i EXE=exe.$i
done

#for i in {1..20}
#do
#	sbatch k3submit.sh exe.$i
#done
