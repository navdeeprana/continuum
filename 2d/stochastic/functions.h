#include <gsl/gsl_randist.h>
#include "parameters.h"
	void
housekeeping (int me, FILE ** fin, double *cbx, double *cby, double *cfx,
		double *cfy, double *dt, int *fr, int *sr, char *folder,
		char *name)
{
	//------------------------------FOLDERS--------------------//
	struct stat st = { 0 };
	sprintf (folder, "data");
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);
#ifdef SCAN
	sprintf (folder, "data/%d", VID);
#endif
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);

	//------------------------------FILES----------------------//
	char flag[2];
#ifdef RESTART
	sprintf (flag, "a");
	sprintf (name, "%s/snapshot.%d_%d", folder, me, RTIME);
	fin[3] = fopen (name, "r");
	if (fin[3] != NULL)
		printf ("Opened file to read.\n");
	else
		exit (0);
#else
	sprintf (flag, "w");
#endif
	fin[0] = stdout;
	sprintf (name, "%s/stat.%d", folder, me);
	fin[1] = fopen (name, flag);
	//#ifdef VERBOSE
	sprintf (name, "%s/hull.%d", folder, me);
	fin[2] = fopen (name, flag);
	//#endif
	//-------------------VARIABLES--------------------//

	*cbx = DBAC / (DX * DX);
	*cby = DBAC / (DY * DY);
	*cfx = DFOO / (DX * DX);
	*cfy = DFOO / (DY * DY);

#ifdef NONLINEAR
	double fac = DTFAC / INFOO;
	printf ("Scaling dt\t%g\n", fac);
#else
	double fac = DTFAC;
#endif
	if (DX <= DY && DBAC >= DFOO)
		*dt = fac / cbx[0];
	else if (DX <= DY && DBAC < DFOO)
		*dt = fac / cfx[0];
	else if (DY <= DX && DBAC >= DFOO)
		*dt = fac / cby[0];
	else if (DY <= DX && DBAC < DFOO)
		*dt = fac / cfy[0];
	*fr = (int) ceil (1.e0 / (INTERVEL * dt[0]));
	*sr = (int) 100 *fr[0];
	if (me == 0)
		printf ("INFOO=%g\tSTEP=%g\tFR=%d\tSR=%d\n", (double) INFOO, dt[0], fr[0],
				sr[0]);
}

	void
file_grid_double_normalised (FILE * fid, double *arr1, double *arr2,
		double *arr3)
{
	int i, j, idx;
	for (j = 0; j < NGY; j++) {
		for (i = 0; i < NGX; i++) {
			idx = INDEX (i, j);
			fprintf (fid, "%3d\t%3d\t%1.6f\t%1.6f\t%1.6f\n", i, j,
					arr1[idx] / arr3[idx], arr2[idx] / arr3[idx], arr3[idx]);
		}
	}
	fprintf (fid, "\n\n");
	fflush (fid);
}

	void
file_grid_double (FILE * fid, double *arr1, double *arr2)
{
	int i, j, idx;
	for (j = 0; j < NGY; j++) {
		for (i = 0; i < NGX; i++) {
			idx = INDEX (i, j);
			fprintf (fid, "%3d\t%3d\t%g\t%g\n", i, j, arr1[idx], arr2[idx]);
		}
	}
	fflush (fid);
}

	void
save_restart_snapshot (FILE * fid, char *folder, int itime, int sr, int me,
		unsigned int *count, double *bac, double *food)
{
	if (itime % sr == 0 && itime > 0) {
		char name[64];
		sprintf (name, "%s/snapshot.%d_%d", folder, me, itime);
		fid = fopen (name, "w");
		file_grid_double (fid, bac, food);
		if (me == 0)
			printf ("snap saved at itime = %d\n", itime);
		fclose (fid);
		*count = 0;
	}
}

	void
save_snap (FILE * fid, char *folder, int *fc, double *bac, double *food,
		double *rho)
{
	char name[64];
	sprintf (name, "%s/snap.%d", folder, fc[0]);
	fid = fopen (name, "w");
	file_grid_double_normalised (fid, bac, food, rho);
	fclose (fid);
	*fc += 1;
}

	void
set_boundary (double *array, int flag)
{
	int i, idx;
	if (flag) {
		for (i = 1; i < NGX - 1; i++) {
			array[i] = 0.e0;
			array[NG - 1 - i] = 0.e0;
		}
		for (i = 1; i < NGY - 1; i++) {
			array[i * NGX] = 0.e0;
			array[(i + 1) * NGX - 1] = 0.e0;
		}
	}
	else {
		for (i = 1; i < NGX - 1; i++) {
			array[i] = array[i + NGX];

			idx = NG - 1 - i;
			array[idx] = array[idx - NGX];
		}
		for (i = 1; i < NGY - 1; i++) {
			idx = i * NGX;
			array[idx] = array[idx + 1];

			idx = (i + 1) * NGX - 1;
			array[idx] = array[idx - 1];
		}
	}
	array[0] = 0.5e0 * (array[1] + array[NGX]);
	array[NGX - 1] = 0.5e0 * (array[NGX - 2] + array[2 * NGX - 1]);
	array[NG - 1] = 0.5e0 * (array[NG - 2] + array[NG - 1 - NGX]);
	array[NG - NGX] = 0.5e0 * (array[NG - NGX + 1] + array[NG - 2 * NGX]);
}

	void
set_initial_rho (double *rho, double *bac, double *food, FILE * fid)
{
#ifndef RESTART
	int i, j, idx;
	for (j = 0; j < NGY; j++) {
		for (i = 0; i < NGX; i++) {
			idx = INDEX (i, j);
#ifdef GAUSSIAN
			double x = (i - NGX / 2) * DX;
			double y = (j - NGY / 2) * DY;
			bac[idx] = exp (-24 * (x * x + y * y));
#else
			bac[idx] = 0.5e0 * (1.e0 - tanh (32.e0 * DY * (j - NGY / 32.e0)));
#endif
			food[idx] = fabs (INFOO * (1.e0 - bac[idx]));
			rho[idx] = bac[idx] + food[idx];
		}
	}
#else
	int i, j, idx;
	double tempbac = ZERO, tempfood = ZERO;
	printf ("Loading Snap saved at %d\n", RTIME);
	while (!feof (fid)) {
		fscanf (fid, "%d\t%d\t%lg\t%lg\n", &i, &j, &tempbac, &tempfood);
		idx = INDEX (i, j);
		bac[idx] = tempbac;
		food[idx] = tempfood;
		rho[idx] = bac[idx] + food[idx];
	}
#endif
}

	void
calc_grad (double *rho, double *gradx, double *grady)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
#ifdef ISOTROPIC
			int n1, n2, n3, n4;
			n1 = INDEX (i + 1, j + 1);
			n2 = INDEX (i - 1, j + 1);
			n3 = INDEX (i - 1, j - 1);
			n4 = INDEX (i + 1, j - 1);
			gradx[idx] =
				(4.e0 * (rho[idx + 1] - rho[idx - 1]) +
				 (rho[n1] - rho[n2] - rho[n3] + rho[n4])) / (12.e0 * DX);
			grady[idx] =
				(4.e0 * (rho[idx + NGX] - rho[idx - NGX]) +
				 (rho[n1] + rho[n2] - rho[n3] - rho[n4])) / (12.e0 * DY);
#else
			gradx[idx] = (rho[idx + 1] - rho[idx - 1]) / (2.e0 * DX);
			grady[idx] = (rho[idx + NGX] - rho[idx - NGX]) / (2.e0 * DY);
#endif
		}
	}
}

	void
calc_div (double *rhox, double *rhoy, double *div)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
#ifdef ISOTROPIC
			int n1, n2, n3, n4;
			n1 = INDEX (i + 1, j + 1);
			n2 = INDEX (i - 1, j + 1);
			n3 = INDEX (i - 1, j - 1);
			n4 = INDEX (i + 1, j - 1);
			div[idx] =
				(4.e0 * (rhox[idx + 1] - rhox[idx - 1]) +
				 (rhox[n1] - rhox[n2] - rhox[n3] + rhox[n4])) / (12.e0 * DX)
				+ (4.e0 * (rhoy[idx + NGX] - rhoy[idx - NGX]) +
						(rhoy[n1] + rhoy[n2] - rhoy[n3] - rhoy[n4])) / (12.e0 * DY);
#else
			div[idx] = (rhox[idx + 1] - rhox[idx - 1]) / (2.e0 * DX)
				+ (rhoy[idx + NGX] - rhoy[idx - NGX]) / (2.e0 * DY);
#endif
		}
	}
}

	void
calc_diff (double constx, double consty, double *rho, double *diff)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
#ifdef ISOTROPIC
			int n1, n2, n3, n4;
			n1 = INDEX (i + 1, j + 1);
			n2 = INDEX (i - 1, j + 1);
			n3 = INDEX (i - 1, j - 1);
			n4 = INDEX (i + 1, j - 1);
			diff[idx] =
				(constx / 12.e0) * (10.e0 *
						(rho[idx + 1] + rho[idx - 1] - 2.e0 * rho[idx]) +
						(rho[n1] + rho[n2] + rho[n3] + rho[n4] -
						 2.e0 * rho[idx + NGX] - 2.e0 * rho[idx - NGX]));
			diff[idx] +=
				(consty / 12.e0) * (10.e0 *
						(rho[idx + NGX] + rho[idx - NGX] -
						 2.e0 * rho[idx]) + (rho[n1] + rho[n2] + rho[n3] +
							 rho[n4] - 2.e0 * rho[idx +
							 1] -
							 2.e0 * rho[idx - 1]));
#else
			diff[idx] =
				constx * (rho[idx + 1] + rho[idx - 1] - 2.e0 * rho[idx]) +
				consty * (rho[idx + NGX] + rho[idx - NGX] - 2.e0 * rho[idx]);
#endif
		}
	}
}

	long long unsigned int
poisson_nrc (const gsl_rng * r, double xm)
{
	static double sq, alxm, g;
	double em, t, y;

	if (xm < 12.e0) {
		g = exp (-xm);
		em = -1.e0;
		t = 1.e0;
		do {
			++em;
			t *= gsl_rng_uniform (r);
		}
		while (t > g);
	}
	else {
		sq = sqrt (2.e0 * xm);
		alxm = log (xm);
		g = xm * alxm - lgamma (xm + 1.e0);
		do {
			do {
				y = tan (M_PI * gsl_rng_uniform (r));
				em = sq * y + xm;
			}
			while (em < 0.e0);
			em = floor (em);
			t = 0.9e0 * (1.e0 + y * y) * exp (em * alxm - lgamma (em + 1.e0) - g);
		}
		while (gsl_rng_uniform (r) > t);
	}
	return (long long unsigned int) em;
}

	double
rng (const gsl_rng * r1, const gsl_rng * r2, double rho, double lambda)
{
	if (rho <= 1.e-10) {
		return 0.e0;
	}
	else {
		double poisson_mean = lambda * rho;
		//unsigned int n = gsl_ran_poisson (r1, poisson_mean);
		long long unsigned int n = poisson_nrc (r1, poisson_mean);
		double randgamma = gsl_ran_gamma (r2, (double) n, 1);
		return randgamma / lambda;
	}
}

	void
stochastic (const gsl_rng * r1, const gsl_rng * r2, double lambda,
		double *bac, double *rho)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
			if (bac[idx] < 0.5e0 * rho[idx]) {
				bac[idx] = rng (r1, r2, bac[idx], lambda);
			}
			else if (bac[idx]) {
				bac[idx] = rho[idx] - rng (r1, r2, rho[idx] - bac[idx], lambda);
			}
		}
	}
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i, j, idx;
	for (j = 1; j < NGY - 1; j++) {
		for (i = 1; i < NGX - 1; i++) {
			idx = INDEX (i, j);
			rho[idx] += dt * drho[idx];
		}
	}
}
	void
adam_bashford_update (double dt, double *rho, double *drho, double *drhop,unsigned int flag)
{
	int i, j, idx;
	if (flag==0){
		for (j = 1; j < NGY - 1; j++) {
			for (i = 1; i < NGX - 1; i++) {
				idx = INDEX (i, j);
				rho[idx] += dt * drho[idx];
				drhop[idx] = drho[idx];
			}
		}
	}
	else{
		for (j = 1; j < NGY - 1; j++) {
			for (i = 1; i < NGX - 1; i++) {
				idx = INDEX (i, j);
				rho[idx] += 0.5e0*dt * (3.e0*drho[idx]-drhop[idx]);
				drhop[idx] = drho[idx];
			}
		}
	}
}

	double
car_cap (double *bac, double *food, double *rho,double *totalfood)
{
	int i;
	double sum = ZERO;
	*totalfood=ZERO;
	for (i = 0; i < NG; i++) {
		sum += bac[i] / rho[i];
		*totalfood += food[i];
	}
	*totalfood /= NG;
	return sum / NG;
}

	double
hull (FILE * fid, double *bac, bool * grid, double bmin)
{
	int p, q, rdx;
	static double old;
	for (p = 0; p < NGY; p++) {
		for (q = 0; q < NGX; q++) {
			rdx = INDEX (q, p);
			if (bac[rdx] >= bmin)
				grid[rdx] = 1;
			else
				grid[rdx] = 0;
		}
	}
	int i = 0, j = NGY;
	int iold = 0, jold = 0;
	int idx = INDEX (i, j);
	int count = 0, length = 0, check = 0;
	while (check == 0) {
		j--;
		idx = INDEX (i, j);
		if (grid[idx] == 1) {
			check = (grid[idx + NGX] == 1 || grid[idx - NGX] == 1
					|| grid[idx + 1] == 1);
		}
	}
#ifdef VERBOSE
	fprintf (fid, "%d\t%d\n", i, j);
#endif
	length++;
	count++;

	unsigned short int im;
	unsigned short int move[] = { 0, 1, 2, 3 };
	while (i < NGX - 1 && count < NG) {
		iold = i;
		jold = j;
		idx = INDEX (i, j);
		im = 0, check = 0;
		while (im < 4) {
			if (move[im] == 0 && grid[idx + 1] == 1) {
				i++;
				move[0] = 3;
				move[1] = 0;
				move[2] = 1;
				move[3] = 2;
				break;
			}
			else if (move[im] == 1 && grid[idx - NGX] == 1) {
				j--;
				move[0] = 0;
				move[1] = 1;
				move[2] = 2;
				move[3] = 3;
				break;
			}
			else if (move[im] == 2 && grid[idx - 1] == 1) {
				i--;
				move[0] = 1;
				move[1] = 2;
				move[2] = 3;
				move[3] = 0;
				break;
			}
			else if (move[im] == 3 && grid[idx + NGX] == 1) {
				j++;
				move[0] = 2;
				move[1] = 3;
				move[2] = 0;
				move[3] = 1;
				break;
			}
			im++;
		}
		if (i != iold || j != jold) {
#ifdef VERBOSE
			fprintf (fid, "%d\t%d\n", i, j);
#endif
			length++;
		}
		count++;
	}
	do {
		idx = INDEX (i, j);
#ifdef VERBOSE
		fprintf (fid, "%d\t%d\n", i, j);
#endif
		length++;
		count++;
		j++;
	} while (grid[idx + NGX] == 1);
#ifdef VERBOSE
	fprintf (fid, "\n\n");
	fflush (fid);
#endif
	if (i == NGX - 1)
		old = (DX * length) / LENX;
	
	return old;
}

	double
height_statistics (double *bac, double *height)
{
	int i, j, idx;
	double variance = ZERO, mean = ZERO;
	for (i = 0; i < NGX; i++) {
		for (j = NGY - 1; j >= 0; j--) {
			idx = INDEX (i, j);
			if (bac[idx] >= 5.e-2) {
				height[i] = j * DY;
				break;
			}
		}
		mean += height[i];
	}
	mean /= NGX;

	for (i = 0; i < NGX; i++)
		variance += (mean - height[i]) * (mean - height[i]);

	variance /= NGX;
	return sqrt (variance);
}
