!./average 0
set term X11 persist noraise size 1800,1000
set term jpeg giant enhanced font arielx50 size 1920,1024 
set output "stat.jpeg"
set key bottom right vertical inside
set lmargin 4.0
set rmargin 0.2
set bmargin 2.0
set tmargin 0.2
#set xrange [0:8000]
set multiplot layout 2,3 rowsfirst
plot 'data/0/stat.0'   every 4 u 1:2 w lp lw 2 t "",\
     'data/0/stat.1'   every 4 u 1:2 w lp lw 2 t "",\
     'data/0/stat.2'   every 4 u 1:2 w lp lw 2 t "",\
     'data/0/stat.3'   every 4 u 1:2 w lp lw 2 t "",\
     'data/0/stat.4'   every 4 u 1:2 w lp lw 2 t "",\
     'data/0/stat.5'   every 4 u 1:2 w lp lw 2 t "",\
     'data/0/stat.6'   every 4 u 1:2 w lp lw 2 t "",\
     'data/0/stat.7'   every 4 u 1:2 w lp lw 2 t "",\

plot 'data/0/stat.0'   every 4 u 1:3 w lp lw 2 t "",\
     'data/0/stat.1'   every 4 u 1:3 w lp lw 2 t "",\
     'data/0/stat.2'   every 4 u 1:3 w lp lw 2 t "",\
     'data/0/stat.3'   every 4 u 1:3 w lp lw 2 t "",\
     'data/0/stat.4'   every 4 u 1:3 w lp lw 2 t "",\
     'data/0/stat.5'   every 4 u 1:3 w lp lw 2 t "",\
     'data/0/stat.6'   every 4 u 1:3 w lp lw 2 t "",\
     'data/0/stat.7'   every 4 u 1:3 w lp lw 2 t "",\

plot 'data/0/stat.0'   every 4 u 1:4 w lp lw 2 t "",\
     'data/0/stat.1'   every 4 u 1:4 w lp lw 2 t "",\
     'data/0/stat.2'   every 4 u 1:4 w lp lw 2 t "",\
     'data/0/stat.3'   every 4 u 1:4 w lp lw 2 t "",\
     'data/0/stat.4'   every 4 u 1:4 w lp lw 2 t "",\
     'data/0/stat.5'   every 4 u 1:4 w lp lw 2 t "",\
     'data/0/stat.6'   every 4 u 1:4 w lp lw 2 t "",\
     'data/0/stat.7'   every 4 u 1:4 w lp lw 2 t "",\

plot 'data/0/stat.a'  every 4 u 1:2 w lp lw 2

plot 'data/0/stat.a'  every 4 u 1:3 w lp lw 2

plot 'data/0/stat.a'  every 4 u 1:4 w lp lw 2
