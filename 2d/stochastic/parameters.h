#ifndef __parameters_h__
#define __parameters_h__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef MPI
#include <mpi.h>
#endif


#define ZERO (0.e0)
#define NGX (640)
#define NGY (640)
#define NG (NGX*NGY)
#define LENX (64.e0)
#define LENY (64.e0)
#define DX (LENX/NGX)
#define DY (LENY/NGY)

#define SIMTIME (1.e5)

#define DFOO  (5.e-2)
#define DBAC  (5.e-4)

#define GAMMA (1.e0)
#define SIGMA (5.e-2)

#ifndef INFOO
#define INFOO (2.e0)
#endif

#ifndef DCOUNT
#define DCOUNT (0)
#endif

#define DTFAC (8.e-2)
#define INTERVEL (1)

#define EPSILON (1.e-4)

#ifndef VID
#define VID 0
#endif

#define INDEX(i,j) ((j)*NGX+(i))
#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))
#define MIN(X,Y) (((X) < (Y)) ? (X) : (Y))
#endif
