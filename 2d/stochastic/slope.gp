set term png
f='data/'
set fit quiet
set print "slope"
food="0.001 0.005 0.01 0.05 0.1e0 0.5e0 1.e0 2.e0 3.e0 4.e0 5.e0 6.e0 7.e0 8.e0 9.e0 10.e0 11.e0 12.e0 14.e0 16.e0"

do for [i=1:10:1]{
	set output "img.".i.".png"
	f(x) = a*x+b
	fit f(x) f.i.'/stat.a' u 1:2 via a,b
	p f.i.'/stat.a' u 1:2 w l,f(x) w l lt -1
	print i,"  ",a,"  ",log10(i),"  ",log10(a)
	unset output
	unset xrange
}
