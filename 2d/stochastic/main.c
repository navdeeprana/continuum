#include"parameters.h"
#include"src/advance.c"
#include"src/data.c"
#include"src/derivatives.c"
#include"src/diagnostics.c"
#include"src/houskeeping.c"
#include"src/stochastic.c"
	int
main (int argc, char **argv)
{
#ifdef MPI
	int NP, me;
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);
#else
	int me = 0;
#endif
	srand (time(NULL)+me);

#ifdef STOCHASTIC
	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	gsl_rng_set (r1, rand ());
	gsl_rng_set (r2, rand ());
#endif

	int i, j, idx, fr,sr;
	double dt, cbx, cby, cfx, cfy;
	char folder[32], name[32];
	FILE **fin = malloc (sizeof (FILE *) * 4);
	housekeeping (me, fin, &cbx, &cby, &cfx, &cfy, &dt, &fr, &sr, folder, name);

	double *rho = calloc (NG, sizeof (double));
	double *drho = calloc (NG, sizeof (double));
	double *bac = calloc (NG, sizeof (double));
	double *dbac = calloc (NG, sizeof (double));
	double *food = calloc (NG, sizeof (double));
	double *dfood = calloc (NG, sizeof (double));

	double *drhop = calloc (NG, sizeof (double));
	double *dbacp = calloc (NG, sizeof (double));

	double *height = calloc (NGX, sizeof (double));

#ifdef NONLINEAR
	double *gbacx = calloc (NG, sizeof (double));
	double *gbacy = calloc (NG, sizeof (double));
	double *gfoox = calloc (NG, sizeof (double));
	double *gfooy = calloc (NG, sizeof (double));
	bool *grid = calloc(NG,sizeof(bool));
#endif

	set_initial_rho (rho, bac, food,fin[3]);
	double food_0=ZERO, food_T=ZERO,carcap=ZERO;
	carcap = car_cap(bac,food,rho,&food_0);
#ifdef STOCHASTIC
	double lambda = 2.0e0 / (SIGMA * SIGMA * dt);
#endif
#ifdef RESTART 
	int itime = RTIME;
#else
	int itime = 0;
#endif
#ifdef SNAP
	int fc = itime/fr;
#endif
	unsigned int count=0, flag=0, adam=0;
	while (itime * dt <= SIMTIME) {
		if (itime % (fr*20) == 0) {
#ifdef SNAP
			if(me==0) {save_snap(fin[4],folder,&fc,bac,food,rho);
				printf("Wrote Snap\n");
		}
#endif
		}
		if (itime % (fr) == 0) {
			carcap = car_cap (bac,food,rho,&food_T);
			if(count > DCOUNT || flag== 1){
				flag=1;
#if !(defined NONLINEAR) && !(defined GAUSSIAN)
				fprintf (fin[1], "%g\t%g\t%g\n", dt * itime,carcap,height_statistics(bac,height));
#endif
#if (defined NONLINEAR) && !(defined GAUSSIAN)
				//fprintf (fin[1], "%g\t%g\t%g\n", dt * itime,carcap,hull(fin[2],bac,grid,EPSILON));
				fprintf (fin[1], "%g\t%g\t%g\t%g\n", dt * itime,carcap,height_statistics(bac,height),hull(fin[2],bac,grid,EPSILON));
#endif
#if (defined GAUSSIAN) || !(defined STOCHASTIC)
				fprintf (fin[1], "%g\t%g\n", dt * itime,carcap);
#endif
				fflush(fin[1]);
			}
			if (carcap > 0.9 || food_T < 0.001*food_0) {
				printf ("Bac full or food shortage.\n");
				break;
			}
			if (me == 0) {
				fprintf (fin[0],"\rtime = %g\tfood_T=%2.3f", itime * dt, 100*(food_T/food_0));
				fflush (fin[0]);
			}
			count++;
		}
#ifdef RESTART_SAVE
		save_restart_snapshot(fin[3],folder,itime,sr,me,&count,bac,food);
#endif
#ifdef STOCHASTIC
		stochastic (r1, r2, lambda, bac, rho);
#endif
		calc_diff (cfx, cfy, food, dfood);
		calc_diff (cbx, cby, bac, dbac);
#ifdef NONLINEAR
		calc_grad (bac, gbacx, gbacy);
		calc_grad (food, gfoox, gfooy);
		set_boundary (gbacx,1);
		set_boundary (gbacy,1);
		set_boundary (gfoox,1);
		set_boundary (gfooy,1);
		/*div(D*C*grad(B)) */
		for (j = 1; j < NGY - 1; j++) {
			for (i = 1; i < NGX - 1; i++) {
				idx = INDEX (i, j);
				dbac[idx] =
					food[idx] * dbac[idx] + DBAC * (gbacx[idx] * gfoox[idx] +
							gbacy[idx] * gfooy[idx]);
			}
		}
#endif
		for (j = 1; j < NGY - 1; j++) {
			for (i = 1; i < NGX - 1; i++) {
				idx = INDEX (i, j);
				drho[idx] = dbac[idx] + dfood[idx];
				dbac[idx] += GAMMA*bac[idx]*(rho[idx]-bac[idx]);
			}
		}
		adam_bashford_update (dt, bac, dbac,dbacp,adam);
		adam_bashford_update (dt, rho, drho,drhop,adam);
		for (j = 1; j < NGY - 1; j++) {
			for (i = 1; i < NGX - 1; i++) {
				idx = INDEX (i, j);
				food[idx] = rho[idx] - bac[idx];
			}
		}
		set_boundary (bac,0);
		set_boundary (food,0);
		for (i = 1; i < NGX - 1; i++) {
			rho[i] = bac[i] + food[i];
			rho[NG - 1 - i] = bac[NG - 1 - i] + food[NG - 1 - i];
		}
		for (i = 1; i < NGY - 1; i++) {
			rho[i * NGX] = bac[i * NGX] + food[i * NGX];
			rho[(i + 1) * NGX - 1] =
				bac[(i + 1) * NGX - 1] + food[(i + 1) * NGX - 1];
		}

		itime++;
		adam = 1;
	}
	free (rho);
	free (drho);
	free (food);
	free (dfood);
	free (bac);
	free (dbac);
#ifdef NONLINEAR
	free (gbacx);
	free (gbacy);
	free (gfoox);
	free (gfooy);
#endif
	fcloseall ();
	printf ("\n%2.2f EOS. Clean exit on rank %d\n.",(food_T/food_0)*100,me);
#ifdef MPI
	MPI_Finalize ();
#endif
	return 0;
}
