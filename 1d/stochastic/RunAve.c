#include "parameters.h"

#define nfiles 20
int main(int argc, char **argv)
{
	int i;
	printf("%d files and %d grid points.\n",nfiles,ng);
	char filename[64];
	FILE **files = malloc(sizeof(FILE *)*nfiles);
	FILE *fid = fopen("data/f_snap.a","w");
	
	for(i=0;i<nfiles;i++)
	{
		sprintf(filename,"data/f_snap.%d",i);
		files[i] = fopen(filename,"r");
		if (files[i] == NULL) printf("Fatal File not found.\n");
	}

	double *x = calloc(ng,sizeof(double));
	double *averho =  calloc(ng,sizeof(double));
	double *avefood =  calloc(ng,sizeof(double));
	int count;
	double xtemp,temprho,tempfood;
	int temp;
	int end=0;
	while (end != 1)
	{
		count = 0;
		while(count<ng)
		{
			for(i=0;i<nfiles;i++)
			{
				temp=0;
				if(files[i] != NULL)
				{
					fscanf(files[i],"%lg\t%lg\t%lg\n",&xtemp,&temprho,&tempfood);
					x[count] = xtemp;
					averho[count] += temprho;
					avefood[count] += tempfood;
					temp++;
				}
			}
			averho[count] /= nfiles;
			avefood[count] /= nfiles;
			count++;
		}
		for(i=0;i<ng;i++)
		{
			fprintf(fid,"%g\t%g\t%g\n",x[i],averho[i],avefood[i]);
			averho[i]= 0.e0;
			avefood[i] = 0.e0;
		}
		fprintf(fid,"\n\n");
		for(i=0;i<nfiles;i++)
		{
				if(files[i] != NULL)
				{
				fscanf(files[i],"\n\n");
				if(feof(files[i]))
					end = 1;
				}
		}
	}
	free(files);
	return 0;
}
