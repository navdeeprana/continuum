#ifndef __parameters_h__
#define __parameters_h__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define zero (0.0e0)
#define ng (800)
#define len (32.e0)
#define simtime (2.0e2)

#define Dbac  (1.e-4)
#define Dfoo  (1.e-1)

#define alpha (1.e0)
#define sigma (5.e-2)
#define gamma (1.e0)

#define infoo (1.e0)
#define foodshift (1.0e0)
#define dtfac (1.e-1)
#define intvl (1)

#endif
