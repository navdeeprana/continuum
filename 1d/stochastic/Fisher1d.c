#include <gsl/gsl_randist.h>
#include "parameters.h"

	void
file_double (FILE * fid, double factor, double *arr1, double *arr2)
{
	int i;
	for (i = 0; i < ng; i++)
		fprintf (fid, "%g\t%f\t%f\n", factor * i, arr1[i], arr2[i]);

	fprintf (fid, "\n\n");
	fflush (fid);
}
	void
file_double_normalised (FILE * fid, double factor, double *arr1, double *arr2)
{
	int i;

	for (i = 0; i < ng; i++)
		fprintf (fid, "%g\t%f\n", factor * i, arr1[i]/arr2[i]);

	fprintf (fid, "\n\n");
	fflush (fid);
}
	void
average_array ( FILE *fid,int itime, double *array)
{
	int i;
	double sum=zero;
	for(i=0;i<ng;i++)
		sum += array[i];

	sum /= ng;
	fprintf(fid,"%d\t%2.8f\n",itime,sum);
	fflush (fid);
}

	void
set_initial_rho (double dx,double *rho,double *bac, double *food)
{
	int i;
	for (i = 0; i < ng; i++) {
		bac[i] = 0.5e0 * (1.e0 - tanh (32.e0 * dx * (i - ng / 5)));
		food[i] = infoo*(1.e0 - bac[i]);
		rho[i] = bac[i] + food[i];
	}
}

	void
calc_grad (double dx, double *rho, double *grad)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		grad[i] = (rho[i + 1] - rho[i - 1]) / (2.e0 * dx);
}

	void
calc_diff (double constant, double *rho, double *diff)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		diff[i] = constant * (rho[i + 1] + rho[i - 1] - 2.e0 * rho[i]);
}

	double
integrate_the_stochastic_part (const gsl_rng * r1, const gsl_rng * r2,
		double rho, double lambda)
{
	double poisson_mean = lambda * rho;
	unsigned int n = gsl_ran_poisson (r1, poisson_mean);
	double randgamma = gsl_ran_gamma (r2, (double) n, 1);
	return randgamma / lambda;

}


	double
logistic (double bac,double rho)
{
	return gamma * bac * (rho-bac);
}

	void
set_bound (double *right, double *pright)
{
	*right = *pright;
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		rho[i] += dt * drho[i];
}

	double
car_cap (double dx, double *rho)
{
	int i;
	double sum = zero;
	for (i = 0; i < ng; i++)
		sum += rho[i];

	return dx * sum;
}
	int
optimiser(double *bac,int oldid)
{
	/*------THE ZEN PIECE OF THIS ENTIRE SIMULATION--------*/
	/* The idea is that as system grows from left to right,
	 * there is no point in doing the integration for the
	 * entire space where there's nothing. This piece just finds
	 * out the point after which there is nothing.
	 */
	int i=oldid,checkid;

	/* Since there can be fluctuations, starting from right most
	 * is a good idea, doesn't cost much and is failsafe.
	 */
	checkid=oldid;
	while(i>=0){
		if(bac[i]!=0.e0){
			checkid=i+1;
			break;
		}
		i--;
	}
	return checkid;
}


	int
main (int argc, char **argv)
{
#ifdef OPTIMISE
	printf("Optimiser Will be Used.\n");
#else
	printf("Optimiser Will not be Used.\n");
#endif

	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	srand (time (NULL) + rand ());
	gsl_rng_set (r1, rand ());
	srand (time (NULL) + rand ());
	gsl_rng_set (r2, rand ());

	int i;
	double dx = len / ng;
	double constbac = Dbac / (dx * dx);
	double constfoo = Dfoo / (dx * dx);

	double dt;
	if (constfoo >= constbac)
		dt = dtfac / constfoo;
	else
		dt = dtfac / constbac;

	int freq = (int) ((1.0 / (intvl * dt)));
	printf ("%g\t%d\n", dt, freq);

	double *rho = calloc (ng, sizeof (double));
	double *drho = calloc (ng, sizeof (double));
   double *bac = calloc (ng,sizeof(double));
	double *dbac = calloc (ng,sizeof(double));
	double *food = calloc (ng, sizeof (double));
	double *dfood = calloc (ng, sizeof (double));

	FILE *fid;
	char file[64],snap[32];
#ifdef OPTIMISE
	sprintf (snap, "osnap");
#else
	sprintf (snap, "snap");
#endif
	FILE *fid1 = fopen ("data/average", "w");
	set_initial_rho (dx, rho,bac, food);
	double lambda = 2.0e0 / (sigma * sigma * dt);

	int itime = 0,fc=0;
	int checkid=ng;
	while (itime * dt <= simtime) {
		if (itime % (freq) == 0) {
			printf ("\r%d", itime / freq);
			fflush (stdout);
			sprintf ( file,"data/%s.%d",snap,fc);
			fid = fopen(file,"w");
			file_double_normalised(fid,dx, bac,rho);
			average_array(fid1,itime,rho);
			fclose(fid);
			fc++;
		}
		calc_diff (constfoo, food, dfood);
		calc_diff (constbac, bac, dbac);
		for (i=1;i<ng-1;i++)
			drho[i] = dbac[i]+dfood[i];
#ifdef OPTIMISE
		checkid=optimiser(bac,checkid); // Used only for gsl-stuff. Can be done for everything else, but rng is the bottle-neck.
#endif
		for (i = 1; i <checkid; i++) {
				if (bac[i] < 0.5e0*rho[i])
					bac[i] = integrate_the_stochastic_part (r1, r2, bac[i], lambda);
				else
					bac[i] =
						rho[i] - integrate_the_stochastic_part (r1, r2,rho[i]-bac[i],lambda);
		}
		calc_diff (constbac, bac, dbac);
		for (i = 1; i < ng-1; i++)
			dbac[i] += logistic(bac[i],rho[i]);

		euler_update (dt, rho, drho);
		euler_update (dt, bac, dbac);
		for(i=1;i<ng-1;i++)
			food[i] = rho[i] - bac[i];

		set_bound (&bac[ng - 1], &bac[ng - 2]);
		set_bound (&food[ng - 1], &food[ng - 2]);
		itime++;
	}
	free (bac);
	free (dbac);
	free (rho);
	free (drho);
	free (food);
	free (dfood);
	return 0;
}
