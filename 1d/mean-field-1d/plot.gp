set term qt noraise
set xrange [12:32]
do for [i=0:1000:2]{
plot 'data/2/snap.0' index i every 1 u 1:2 w p lw 2 lt 2 pt 7 t "",\
     'data/3/snap.0' index i every 1 u 1:2 w p lw 2 lt 1 pt 7 t "",\
     'data/2/snap.0' index i every 1 u 1:3 w l lw 2 lt 2  t "",\
     'data/3/snap.0' index i every 1 u 1:3 w l lw 2 lt 1  t ""

pause 0.1
}
