set term png
f="data/"
set fit quiet
do for [i=1:12:1]{
	set print f."front_velocity.".i
	do for [j=0:19:1]{
		set output "data/img.".i.".".j.".png"
		f(x) = a*x+b
		fit f(x) f.i.'/stat.'.j u 1:2 via a,b
		p f.i.'/stat.'.j every 20 u 1:2 w p pt 7 ps 1 ,f(x) w l lt -1
		n=j+1
		print n,"  ",a
		unset output
	}
}
