#ifndef __parameters_h__
#define __parameters_h__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef MPI
#include <mpi.h>
#endif

#define ZERO (0.0E0)
#define NG (6400)
#define LEN (64.E0)
#define DX (LEN/NG)

#define SIMTIME (1.0E3)
#ifndef DBAC
#define DBAC  (1.E-4)
#endif
#define DFOOD  (1.E-2)

#define GAMMA (1.E0)

#define INFOO (1.E0)
#define DTFAC (1.E-1)
#define INTERVEL (1)

#endif
