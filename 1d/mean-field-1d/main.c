#include "parameters.h"

	void
housekeeping (int me, FILE ** fin, double *cb,double *cf, double *dt, int *fr, char *folder)
{
	//------------------------------FOLDERS--------------------//
	struct stat st = { 0 };
	sprintf (folder, "data");
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);

#ifdef SCAN	
	sprintf (folder, "data/%d",VID);
	if (stat (folder, &st) == -1)
		mkdir (folder, 0700);
#endif
	//------------------------------FILES----------------------//
	
	char name[32];
	sprintf(name,"%s/log.%d",folder,me);
	fin[0] = fopen(name,"w");
	sprintf(name,"%s/stat.%d",folder,me);
	fin[1] = fopen(name,"w");
	sprintf(name,"%s/snap.%d",folder,me);
	fin[2] = fopen(name,"w");
	
	*cb = DBAC / (DX * DX);
	*cf = DFOOD / (DX * DX);
	if (cf >= cb)
		*dt = DTFAC / cf[0];
	else
		*dt = DTFAC / cb[0];

	*fr = (int) ceil((1.0 / (INTERVEL * dt[0])));
	fprintf (fin[0],"DBAC=%g\tINFOO=%g\tSTEP=%g\tFR=%d\n",DBAC, (double) INFOO, dt[0], fr[0]);
}

	void
file_double (FILE * fid, double *arr1, double *arr2)
{
	int i;
	for (i = 0; i < NG; i++)
		fprintf (fid, "%g\t%f\t%f\n",i*DX, arr1[i], arr2[i]);

	fprintf (fid, "\n\n");
	fflush (fid);
}
	void
file_double_normalised (FILE * fid, double *arr1, double *arr2)
{
	int i;
	for (i = 0; i < NG; i++)
		fprintf (fid, "%d\t%f\n",i, arr1[i]/arr2[i]);

	fprintf (fid, "\n\n");
	fflush (fid);
}
	double
carrying_capacity( FILE *fid,int itime, double *bac,double *food,double *rho,double *C)
{
	int i;
	double sum=ZERO;
	*C = ZERO;
	for(i=NG;i--;){
		sum += bac[i]/rho[i];
		*C += food[i];
	}
	sum /= NG; 
	*C /= NG;
	fprintf(fid,"%d\t%g\t%g\n",itime,sum,C[0]);
	fflush (fid);
	return sum;
}
	
	void
set_initial_rho (double *rho,double *bac, double *food,int me)
{
	int i;
	for (i = NG; i--;) {
		bac[i] = 0.5e0 * (1.e0 - tanh (32.e0 * DX * (i - NG / 5)));
		food[i] = fabs((me+1)*INFOO*(1.e0 - bac[i]));
		rho[i] = bac[i] + food[i];
	}
}
	void
calc_diff (double constant, double *rho, double *diff)
{
	int i;
	for (i = 1; i < NG - 1; i++)
		diff[i] = constant * (rho[i + 1] + rho[i - 1] - 2.e0 * rho[i]);
}
	
	void
set_bound (double *right, double *pright)
{
	*right = *pright;
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i;
	for (i = 1; i < NG - 1; i++)
		rho[i] += dt * drho[i];
}
	int
main (int argc, char **argv)
{
#ifdef MPI
	int NP, me;
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);
#else
	int me = 0;
#endif
	int fr;
	double dt,cb,cf;
	FILE **fin = malloc(sizeof(FILE *)*3);
	char folder[64];
	housekeeping (me, fin, &cb, &cf, &dt, &fr, folder);
	int i;
	double *rho = calloc (NG, sizeof (double));
	double *drho = calloc (NG, sizeof (double));
   double *bac = calloc (NG,sizeof(double));
	double *dbac = calloc (NG,sizeof(double));
	double *food = calloc (NG, sizeof (double));
	double *dfood = calloc (NG, sizeof (double));
	
	set_initial_rho (rho,bac, food,me);
	int itime = 0;
	double C_0,C_T;
	double carcap=carrying_capacity(fin[1],itime*dt,bac,food,rho,&C_0);
	while (itime * dt <= SIMTIME) {
		if (itime % fr == 0) {
			carcap=carrying_capacity(fin[1],itime*dt,bac,food,rho,&C_T);
			fprintf(stdout,"\rtime=%g		C_T=%g",itime*dt,100*(C_T/C_0));
			fflush(stdout);
			file_double(fin[2],bac,food);
			if(carcap > 0.95){
				fprintf(fin[0],"\ndone\n");
				break;
			}
		}
		calc_diff (cf, food, dfood);
		calc_diff (cb, bac, dbac);
		for (i = 1; i < NG-1; i++){
			dbac[i] += GAMMA*bac[i]*(rho[i]-bac[i]);
		}
		euler_update (dt, rho, drho);
		euler_update (dt, bac, dbac);
		for(i=1;i<NG-1;i++){
			food[i] = rho[i] - bac[i];
		}
		set_bound (&bac[NG - 1], &bac[NG - 2]);
		set_bound (&food[NG - 1], &food[NG - 2]);
		itime++;
	}
	free (bac);
	free (dbac);
	free (rho);
	free (drho);
	free (food);
	free (dfood);
	free(fin);
#ifdef MPI
	MPI_Finalize();
#endif
	return 0;
}
