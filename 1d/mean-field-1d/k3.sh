set -e
#module load mpi/gcc/openmpi/2.0.1

declare -a arr=("1.e-2" "5.e-3" "1.e-3" "5.e-4" "1.e-4")
for i in {1..5}
do
	echo "make DBAC=${arr[$i-1]} ID=$i EXE=exe.$i"
			make DBAC=${arr[$i-1]} ID=$i EXE=exe.$i
done
for i in {1..5}
do
	echo $i
	./exe.$i
done
