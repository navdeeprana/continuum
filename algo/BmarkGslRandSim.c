#include <stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_sf_bessel.h>
#include<gsl/gsl_randist.h>

#define zero 0.e0
#define N 2048
#define NBIN N

#define NRAND 32768
#define LEN (2.0e0)

#define sigma (sqrt(2.e0))
#define simtime 10.e0
#define dt 3.e-2
#define inirho 1
#define shift 0.05
typedef double dbl;
void file_dbl(FILE *fid,dbl dx,dbl *array1,dbl *array2)
{
	int i;
	for(i=0;i<N;i++)
		fprintf(fid,"%f\t%f\t%f\n",i*dx+shift,array1[i],array2[i]);
	fprintf(fid,"\n\n");
}

	dbl
get_rand (gsl_rng *r1, gsl_rng *r2,dbl rho, dbl lambda)
{
	dbl poisson_mean = lambda*rho;
	unsigned int n = gsl_ran_poisson (r1, poisson_mean);
	dbl gamma = gsl_ran_gamma (r2, (dbl) n, 1);
	return gamma/lambda;
}
	void
histogram(dbl *array,dbl *hist)
{
	int i,binid;
	dbl width = LEN/NBIN;

	for(i=0;i<NBIN;i++)
		hist[i] = 0;

	for(i=0;i<NRAND;i++)
	{
		binid = (int) floor(array[i]/width);
		if(binid <= NBIN)
			hist[binid] +=1;
	}
}
dbl analytical(dbl time, dbl rho)
{
	dbl lambda = 2.e0/(sigma*sigma*time);
	dbl arg = zero;
	dbl value = zero;

	value = lambda*(sqrt(inirho/rho))*exp(-(inirho+rho)*lambda);
	arg = 2.0e0*sqrt(inirho*rho)*lambda;
/*	printf("%g\t%g\n",lambda,rho);
	printf("%g\t%g\n",value,arg);
*/
	value = value*gsl_sf_bessel_I1(arg);
	return value;
}
	int
main (void)
{
	int i;
	dbl dx = LEN/N;
	printf("%g\t%g\n",dt,dx);	
	dbl *pdf = calloc(N,sizeof(dbl));
	dbl *rand = calloc(NRAND,sizeof(dbl));
	dbl *hist = calloc(NBIN,sizeof(dbl));

	FILE *fid = fopen("snap","w");
	
	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	
	dbl factor = 1.e0;//NRAND;
	for(i=0;i<NRAND;i++)
		rand[i] = inirho;

	dbl lambda =  2.e0/(sigma *sigma *dt);

	int itime=0;
	dbl time = 0.e0;
/*
	while (itime*dt <= simtime){
		if(itime%100==0) printf("\r%d",itime/100);
		fflush(stdout);
		for(i=0;i<NRAND;i++)
			rand[i] = get_rand(r1,r2,rand[i],lambda);
		
		histogram(rand,hist);
		for(i=0;i<NBIN;i++)
			hist[i] = hist[i]*factor;
		
		for(i=0;i<N;i++)
			pdf[i] = analytical(dt*(itime+1),i*dx+shift);

		file_dbl(fid,dx,pdf,hist);
		itime++;
	}
*/
	itime++;
	while (itime <= 32){
		time = dt*itime;
		for(i=0;i<NRAND;i++)
			rand[i] = get_rand(r1,r2,rand[i],lambda);
		
		histogram(rand,hist);
		for(i=0;i<NBIN;i++)
			hist[i] = hist[i]*factor;
		for(i=0;i<N;i++)
			pdf[i] = analytical(time,i*dx + shift);

		file_dbl(fid,dx,pdf,hist);
		itime++;
		printf("%d\n",itime);
	}
	gsl_rng_free (r1);
	gsl_rng_free (r2);
	fclose(fid);
	free(hist);
	free(rand);
	free(pdf);
	return 0;
}

