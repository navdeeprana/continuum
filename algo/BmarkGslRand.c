#include <stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_sf_bessel.h>
#include<gsl/gsl_sf_gamma.h>
#include<gsl/gsl_randist.h>

#define NGRID (128)
#define NBIN (256)

#define NRAND (65536)
#define length (4.0e0)
#define zero (0.0e0)
#define ntime (32)
#define dt (0.03e0)
#define D (0.25e0)
#define a (1.75623e0)
#define b (1.0e0)
#define sigma (2.0e0)
#define epsilon 1.0e-8
#define inirho (1.0e0)
void file_double(FILE *fid,int LEN,double dx,double *array,int flag)
{
	int i;
	if(flag==1)
	{
		for(i=0;i<LEN;i++)
			fprintf(fid,"%f\t%f\n",i*dx,array[i]);
	}
	else
	{
		for(i=0;i<LEN;i++)
			fprintf(fid,"%f\n",array[i]);
	}
	fprintf(fid,"\n\n");
}

	double
get_rand_from_distribution (gsl_rng *r1, gsl_rng *r2,double rho,double expbetat,double lambda,double mu)
{

	// this is the mean for the poisson distribution , it's different from the mu of the problem

	double poisson_mean = lambda * rho *expbetat;
	unsigned int n = gsl_ran_poisson (r1, poisson_mean);
	double gamma = gsl_ran_gamma(r2,mu + (double) n + 1.e0,1);
	return gamma/lambda;
}
void calculate_alpha(int LEN,double constant,double *rho, double *alpha)
{
	int i;
	alpha[0] = constant * (rho[1] + rho[LEN - 1]);
	
	for(i=1;i<LEN-1;i++)
		alpha[i] = constant * (rho[i-1] + rho[i+1]);
	
	alpha[LEN-1] = constant * (rho[LEN-2] + rho[0]);
}
	void
histogram(double *randrho,int *hist)
{
	int i;
	double bin_size = length/NBIN;
	int binid;

	for(i=0;i<NBIN;i++)
		hist[i] = 0;

	for(i=0;i<NRAND;i++)
	{
		binid = (int) floor(randrho[i]/bin_size);
		if(binid <=NBIN)
			hist[binid] +=1;
	}
}
void copy_int_to_double(int LEN, int *iarray,double *darray)
{
	int i;
	for(i=0;i<LEN;i++)
		darray[i] = (double) iarray[i];
}
void rescale_array(int LEN,double factor, double *array)
{
	int i;
	for(i=0;i<LEN;i++)
		array[i] = array[i]/factor;
}

double calculate_pdfform(double lambda, double beta,double mu,double time, double rho)
{
	int i=0,fact=1;
	double value = 0.0e0,gamma = 1.0e0,poisson = 1.0e0 ;
	while ((poisson*gamma) > epsilon)
	{
		poisson = pow(lambda*inirho*exp(beta*time),i)*exp(-lambda*inirho*exp(beta*time)/fact);
		gamma = lambda*exp(-rho*lambda)*pow((lambda*rho),i+mu)/gsl_sf_gamma(mu+i+1);
		value += (poisson*gamma);
		i++;
		fact = fact*i;
	}
	return value;
}
	int
main (void)
{
	/* this is some random allocation for gsl random numbers,
	 * do not change, or the universe will be in chaos,
	 * just kidding, it will explode
########################################################################*/

	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);


	/*###################################################################### */

	/* the random deviate part */

	int i,j;
	double beta = a;
	double expbetat = exp(beta*dt);
	
	double mu,lambda;
	mu = 1.0e0;
	lambda = 2.0e0 * beta / (sigma * (expbetat - 1.0e0));
	
	double *randrho = calloc(NRAND,sizeof(double));

	double binsize = length/NBIN;
	double factor = 1/(NRAND*binsize);
	int *hist = calloc(NBIN,sizeof(int));
	double *doublehist = calloc(NBIN,sizeof(double));

	FILE *fid1 = fopen("hist","w");

	double time = zero;

	for(i=0;i<NRAND;i++)
	{
		randrho[i] = inirho;
	}
	
	for(i=0;i<ntime;i++)
	{
		for(j=0;j<NRAND;j++)
		{
			randrho[j] = get_rand_from_distribution(r1,r2,randrho[j],expbetat,lambda,mu);
		}
		histogram(randrho,hist);
		copy_int_to_double(NBIN,hist,doublehist);
		rescale_array(NBIN,1024,doublehist);
		file_double(fid1,NBIN,binsize,doublehist,1);
	}
	free(randrho);
	free(hist);
	free(doublehist);
	fclose(fid1);

	double *pdfform = calloc(NGRID,sizeof(double));
	double *rhopdf = calloc(NGRID,sizeof(double));
	FILE *fid2 = fopen("pdf","w");
	
	double dxpdf = length/NGRID;
	beta = a;
	for(j=0;j<NGRID;j++)
	{	
			rhopdf[j] = j*dxpdf;
	}
	for(i=0;i<ntime;i++)
	{
		time = (i+1)*dt;
		lambda = 2.0e0 * beta / (sigma * (exp(beta*time) - 1.0e0));
		mu = 1.0e0;
		for(j=0;j<NGRID;j++)
		{	
			pdfform[j] = calculate_pdfform(lambda,beta,mu,time,rhopdf[j]);
		}
		file_double(fid2,NGRID,dxpdf,pdfform,1);
	}
	gsl_rng_free (r1);
	gsl_rng_free (r2);
	fclose(fid2);
	free(pdfform);
	free(rhopdf);
	return 0;
}

