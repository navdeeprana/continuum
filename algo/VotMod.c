#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_randist.h>

#define zero (0.0e0)

#define ngridx (512)
#define ngridy (ngridx)

#define dx (1.0e0)
#define ntime (1E4)
#define dt (0.1e0)

#define dim (2.0e0) // dimensions.
#define D (1.0e0) // diffusion constant
#define sigma (0.5e0)
#define inirho (1.0e0) // initial density

#define takesnap (1)
#define frequency (500) // write frequency

#define index(i,j) ((j) + (i)*ngridx)

/*##########################################################*/
/* Write x, y, array[x,y] */

	void
file_grid_double (FILE * fid, int LENX, int LENY,double realtime, double *array)
{
	int i, j, index;
	fprintf (fid, "## at time t=%g\n\n",realtime);
	for (i = 0; i < LENX; i++)
	{
		for (j = 0; j < LENY; j++)
		{
			index = i * LENX + j;
			fprintf (fid, "%d\t%d\t%2.4f\n",j,i,
					array[index]);
		}
	}
	fprintf (fid, "\n");
	fflush(fid);
}
/* set here the initial condition
 * ##################################################################*/

	void
set_initial_rho (int LEN, double *rho)
{
	int i;
	srand (time (NULL));
	srand48 (rand ());
	for (i = 0; i < LEN; i++)
		rho[i] = 2.0e0 * drand48 () - 1.0e0;
}

/* calculate alpha
 * ##################################################################*/

	void
calculate_alpha (int LEN, double constant, double *rho, double *alpha)
{
	int i;
	int xnb1, xnb2, ynb1, ynb2;
	for (i = 0; i < LEN; i++)
	{
		xnb1 = i + 1;
		xnb2 = i - 1;
		ynb1 = i + ngridx;
		ynb2 = i - ngridy;
		
		/* to set PBC */

		if (i < ngridx)
			ynb2 = LEN - ngridx + i;

		if (i >= (LEN - ngridx))
			ynb1 = i % (ngridx);

		if (i % ngridx == 0)
			xnb2 = i + ngridx - 1;

		if (i % ngridx == (ngridx - 1))
			xnb1 = i - (ngridx - 1);

		alpha[i] = constant *(rho[xnb1] + rho[xnb2] + rho[ynb1] + rho[ynb2]);
			
	}
}
/*########################################################################*/
/* rho_I = 1 - <rho(r,t)rho(r+e_v,t)> */

	void
calculate_interface_density (FILE * fid, int LEN, double realtime,
		double *rho)
{
	int i;
	double indensity = zero;
	int xnb1, xnb2, ynb1, ynb2;
	for (i = 0; i < LEN; i++)
	{
		xnb1 = i + 1;
		xnb2 = i - 1;
		ynb1 = i + ngridx;
		ynb2 = i - ngridy;
		if (i < ngridx)
			ynb2 = LEN - ngridx + i;

		if (i >= (LEN - ngridx))
			ynb1 = i % (ngridx);

		if (i % ngridx == 0)
			xnb2 = i + ngridx - 1;

		if (i % ngridx == (ngridx - 1))
			xnb1 = i - (ngridx - 1);

		indensity += 0.25e0*rho[i]* (rho[xnb1] + rho[xnb2] + rho[ynb1] + rho[ynb2]);
	}
	indensity = 1.e0 - (indensity / LEN);
	fprintf (fid, "%f\t%f\n", realtime, indensity);
	fflush(fid);
}
/* integrate the stochastic part of the equation here
 * ####################################################################*/

	double
integrate_the_stochastic_part (const gsl_rng * r1, const gsl_rng * r2,
		double rho, double lambda, double mu,
		double expbetat)
{
	// this is the mean for the poisson distribution
	//  it's different from the mu of the problem.
	
	double poisson_mean = lambda * rho * expbetat;
	unsigned int n = gsl_ran_poisson (r1, poisson_mean);
	double gamma = gsl_ran_gamma (r2, mu + (double) n + 1.e0, 1);
	return gamma / lambda;
}

	int
main (int argc, char **argv)
{
	/* GSL rng setup
########################################################################*/

	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);

/*###################################################################### */

	int i,itime;
	int ngrid = ngridx * ngridy; // total points

	double constant = D / (dx * dx);
	double rhorand, realtime = 0.0e0;

	FILE *fid1 = fopen ("f_density", "w"); 	
	FILE *fid2 = fopen ("f_snapshot", "w");

	double *rho = calloc (ngrid, sizeof (double));
	double *alpha = calloc (ngrid, sizeof (double));

	srand (time (NULL));
	srand48 (rand ());
	set_initial_rho (ngrid, rho); // it sets rho in U(-1:1)

	double beta = - 2.0e0 * dim * constant; // a is zero, beta is negative

	double expbetat = exp (beta * dt);

	double mu, lambda, temprho,tempalpha;

	lambda = 2.0e0 * beta / (sigma * (expbetat - 1.0e0));
	for (itime = 0; itime < ntime; itime++)
	{
		if (itime % frequency == 0)
		{
			printf ("%d\n", itime);
			calculate_interface_density (fid1, ngrid, realtime, rho);
			if(takesnap == 1)
				file_grid_double (fid2, ngridx, ngridy,realtime, rho);
		}
		calculate_alpha (ngrid, constant, rho, alpha);
		realtime = dt * (itime + 1);
		for (i = 0; i < ngrid; i++)
		{
			/* the noise magnitude in voter's model is 1 +/- rho
			 * depending on the value of rho, transformation leads to
			 * sqrt noise, and there is no deterministic part.
			 */

			if (rho[i] > zero)
			{
				tempalpha = 4.0e0 * constant - alpha[i];
				mu = -1.0e0 + 2.0e0 * (tempalpha / sigma);
				temprho = 1 - rho[i];
				rhorand =
					integrate_the_stochastic_part (r1, r2, temprho, lambda, mu,
							expbetat);
			}
			else
			{
				tempalpha = 4.0e0 * constant + alpha[i];
				mu = -1.0e0 + 2.0e0 * (tempalpha / sigma);
				temprho = 1 + rho[i];
				rhorand =
					integrate_the_stochastic_part (r1, r2, temprho, lambda, mu,
							expbetat);
			}
			if (rho[i] > zero)
				rho[i] = 1.0e0 - rhorand;
			else
				rho[i] = rhorand - 1.0e0;
		}
	}
	free (rho);
	free(alpha);
	fclose(fid1);
	fclose(fid2);
}
