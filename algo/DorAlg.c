#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_randist.h>

#define ngrid (2048) 
#define dx (1.e0)
#define ntime (4E4)
#define dt (0.1e0)
#define D (0.25e0)
#define a (1.75623e0)
//#define a (1.0e0)

#define b (1.0e0)
#define sigma (2.0e0)
#define inirho (1.0e0)

void stdout_double(double *array)
{
	int i;

	for(i=0;i<ngrid;i++)
		printf("%d\t%f\n",i,array[i]);
}

/* set here the initial condition
 * ##################################################################*/

	void
set_initial_rho (double *rho)
{
	int i;
	for (i = 0; i < ngrid; i++)
		rho[i] = inirho;
}

/* calculate alpha
 * ##################################################################*/

void calculate_alpha(double constant,double *rho, double *alpha)
{
	int i;
	alpha[0] = constant * (rho[1] + rho[ngrid - 1]);
	
	for(i=1;i<ngrid-1;i++)
		alpha[i] = constant * (rho[i-1] + rho[i+1]);
	
	alpha[ngrid-1] = constant * (rho[ngrid-2] + rho[0]);
}
/* integrate the stochastic part of the equation here
 * ####################################################################*/

	double
integrate_the_stochastic_part (const gsl_rng * r1 ,const gsl_rng * r2,double rho, double lambda,
		double mu,double expbetat)
{
	// this is the mean for the poisson distribution , it's different from the mu of the problem
	double poisson_mean = lambda * rho *expbetat;
	unsigned int n = gsl_ran_poisson (r1, poisson_mean);
	double gamma = gsl_ran_gamma(r2,mu + (double) n + 1.e0,1);
	return gamma/lambda;
}



/* integrate the deterministic part here
 * ####################################################################*/

	double
integrate_the_deterministic_part (double rho)
{
	return rho/(1.e0+rho*b*dt);
}

/*#####################################################################*/

	void
calculate_average_rho (FILE * fid,double time, double *rho)
{
	int i;
	double sum = 0.0e0;

	for (i = 0; i < ngrid; i++)
		sum += rho[i];

	sum = sum / ngrid;
	fprintf(fid,"%f\t%f\n",time,sum);
}

	int
main (int argc, char **argv)
{
	/* this is some random allocation for gsl random numbers,
	 * do not change, or the universe will be in chaos,
	 * just kidding, it will explode
########################################################################*/

	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);

	/*###################################################################### */

	int i, j;

	double constant = D / (dx * dx);

	double rhorand,time=0.0e0;

	FILE *fid = fopen ("output", "w");

	double *rho = calloc (ngrid, sizeof (double));
	double *alpha = calloc (ngrid, sizeof (double));

	set_initial_rho (rho);
	
	double beta = a - 2.0e0*constant;

	double expbetat = exp(beta*dt);
	
	double mu,lambda;
	
	lambda = 2.0e0 * beta / (sigma * (expbetat - 1.0e0));

	for (i = 0; i < ntime; i++)
	{
		if(i%1000==0)
			printf("%d\n",i);

		calculate_average_rho(fid,time,rho);
		calculate_alpha(constant,rho,alpha);
		time = dt * (i+1);

		for (j = 0; j < ngrid; j++)
		{
			mu = -1.0e0 + 2.0e0 * (alpha[j] / sigma);

			rhorand = integrate_the_stochastic_part (r1,r2,rho[j],lambda, mu, expbetat);
			rho[j] = integrate_the_deterministic_part (rhorand);

		}
	}
	free (rho);
}
