#ifdef CHAOTIC_FLOW_JB
double nu;


typedef struct{
  double real,imag;
}cmplx;

cmplx *psi;

double *lam,*ksqr,*kx,*ky;
#endif



void gen_psi(cmplx *psi,cmplx *duxdt,cmplx *duydt,double *ksqr,double*lam,double *kx,double *ky)
{
  int i,j;
  double rnum[2];
  double rhs1,rhs2;

  psi[0].real = 0.0;
  psi[0].imag = 0.0;



  for(i=1;i<3;i++){
    gen_random(rnum,1);
    rhs1 = (-nu*ksqr[i]*psi[i].real*delta + sqrt(nu*lam[i])*rnum[0]*sqrt(delta));
    rhs2 = (-nu*ksqr[i]*psi[i].imag*delta + sqrt(nu*lam[i])*rnum[1]*sqrt(delta));
    psi[i].real +=  rhs1;
    psi[i].imag +=  rhs2;

    duxdt[i].real = -ky[i]*rhs2;
    duxdt[i].imag = ky[i]*rhs1;

    duydt[i].real =  kx[i]*rhs2;
    duydt[i].imag = -kx[i]*rhs1;

  }
  
  for(i=3;i<5;i++){
    psi[i].real = psi[i-2].real;
    psi[i].imag = -psi[i-2].imag;

    duxdt[i].real = duxdt[i-2].real;
    duxdt[i].imag = -duxdt[i-2].imag;

    duydt[i].real =  duydt[i-2].real;
    duydt[i].imag = -duydt[i-2].imag;
  }


  for(i=5;i<7;i++){
    gen_random(rnum,1);
    rhs1 = (-nu*ksqr[i]*psi[i].real*delta + sqrt(nu*lam[i])*rnum[0]*sqrt(delta));
    rhs2 = (-nu*ksqr[i]*psi[i].imag*delta + sqrt(nu*lam[i])*rnum[1]*sqrt(delta));
    
    psi[i].real += rhs1;
    psi[i].imag += rhs2;

    duxdt[i].real = -ky[i]*rhs2;
    duxdt[i].imag = ky[i]*rhs1;

    duydt[i].real =  kx[i]*rhs2;
    duydt[i].imag = -kx[i]*rhs1;
  }

  for(i=7;i<9;i++){
    psi[i].real = psi[i-2].real;
    psi[i].imag = -psi[i-2].imag;
    duxdt[i].real = duxdt[i-2].real;
    duxdt[i].imag = -duxdt[i-2].imag;

    duydt[i].real =  duydt[i-2].real;
    duydt[i].imag = -duydt[i-2].imag;

  }


}

void init_lam_kx_ky(double *lam,double *ksqr,double *kx,double *ky,cmplx *psi,cmplx *duxdt,cmplx *duydt,double famp){
  int i,j;
  double kfacx,kfacy,rnum[2];
  kfacx=(2.0*M_PI/lenx);
  kfacy=(2.0*M_PI/leny);



  /*! Set the wave-vectors */
  kx[0]=0.0; ky[0]=0.0;
  kx[1]=1.0*kfacx; ky[1]=0.0;
  kx[2]=0.0; ky[2]=1.0*kfacy;
  kx[3]=-1.0*kfacx; ky[3]=0.0;
  kx[4]=0.0; ky[4]=-1.0*kfacy;
  kx[5]=1.0*kfacx; ky[5]=1.0*kfacy;
  kx[6]=-1.0*kfacx; ky[6]=1.0*kfacy;
  kx[7]=-1.0*kfacx; ky[7]=-1.0*kfacy;
  kx[8]=1.0*kfacx; ky[8]=-1.0*kfacy;
  
  
  /*! Determine ksqr and set other variables to 0 */
  double sumlam=0.;
  for(i=0;i<9;i++){
    
    ksqr[i]=kx[i]*kx[i] + ky[i]*ky[i];
    
    psi[i].real=0.0;
    psi[i].imag=0.0;
    
    duxdt[i].real=0.0;
    duxdt[i].imag=0.0;

    lam[i] = ksqr[i]*pow((1.0+ksqr[i]),-7./3.);
    sumlam+=lam[i];

  }

  /*! Normalize lambda_k */
  /*! The energy in the system is given by E_k=0.5 \sum_k \lam_k */
  for(i=0;i<9;i++)lam[i]=2.0*famp*lam[i]/sumlam;


  /*! Now that we have properly normalize lambda we set the streamfunction */
  double fact;
  for(i=0;i<9;i++){
    gen_random(rnum,2);
    if(ksqr[i]!=0){
      fact = lam[i]/(2.0*ksqr[i]);
    }else{
      fact = 0.0;
    }
    psi[i].real = fact*rnum[0];
    psi[i].imag = fact*rnum[1];

  }

}


void chaotic_flow(double xpos,double ypos,double ux,double uy){
  int k;

  for(k=0;k<9;k++){
    
    ux += (-ky[k]*psi[k].imag*cos(kx[k]*xpos +ky[k]*ypos) + 
	   ky[k]*psi[k].real*sin(kx[k]*xpos +ky[k]*ypos));
    
    uy += (kx[k]*psi[k].imag*cos(kx[k]*xpos +ky[k]*ypos) - 
	   kx[k]*psi[k].real*sin(kx[k]*xpos +ky[k]*ypos));
  }
}
