/*! Version1 of the code to be eventually used for particle and turbulence */
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#ifdef MPI
#include<mpi.h>
#endif

#ifdef EXTVEL
#include "fftw.h"
#include "rfftw.h"
#endif

#ifdef CHAOTIC_FLOW
double B, phi;
#endif

#ifdef MUTUALISM
double lam_aa, lam_ab, lam_ba, lam_bb;
#endif

/*! Structure for complex variables */
#ifdef CHAOTIC_FLOW_JB
double nu;


typedef struct
{
	double real, imag;
} cmplx;

cmplx *psi;

cmplx *duxdt, *duydt;
double *lam, *ksqr, *kx, *ky;
#ifdef COMPRES
/* The value of compressibility */
double kappa;
#endif
#endif

#ifdef DETERMINISTIC_FORCE_COMPRES
#ifdef COMPRES
double alpha, kappa;
#endif
#endif

int NP, me, ttime, nout;
double D, delta;

double taup1, beta1;
double taup2, beta2;

double lenx, leny;
int NX, NY, NZ;

#define NXP2 (NX+2)
#define NYP2 (NY+2)
#ifdef EXTVEL
#define NZ (128)
#define NZP2 (NZ+2)
#define nprocs (16)
#define local_nx (NX/nprocs)
#define total_local_size (local_nx*NY*NZP2)
#endif
#define dx   (lenx/(double)NX)
#define dy   (leny/(double)NY)
/* Death rate */
#ifdef SIMONE
double gamma_death;
#endif
#ifdef DIRECTED_PERCOLATION
double gamma_percolation;
#endif
/* Birth rates */
double mu1, mu2;
/* Total time */
#define IDX(i,j) ((j) + NYP2*(i))
#define IDXR(i,j,k) ((k) + (j)*NZP2 + (i)*NZP2*NY)
#define xphys(i) ((i) + me*local_nx)

typedef double scalar;


typedef struct
{
	scalar x, y;
	int flag;
	scalar vx, vy;
	scalar vx0, vy0;
#ifdef INERTIAL
	scalar vxrhs0, vyrhs0;
#endif
} part;

typedef struct
{
	scalar x, y;
} bivel;

typedef struct
{
	float x, y, z;
} trivel;

#ifdef EXTVEL
rfftwnd_plan plan, iplan;

typedef struct
{
	fftw_real x, y;
} bu;

typedef struct
{
	fftw_complex x, y;
} cbu;

bu *uf = NULL;
cbu *cuf = NULL;
#endif


/* Total number of particles and death counter */
int Npart1, dcount1;
int Npart2, dcount2;
#ifdef EXTVEL
double phi;
#endif



#ifdef HETERO
	void
heterozygosity_hist (scalar * hist1, scalar * hist2, int itime, int me)
{
	/* CAUTION: THIS ROUTINE IS NOT TESTED AND IS NOT FUNCTIONAL */

	int i, j, ncenter;
	int icen, jcen, idx, idx0, irad, Nrad, ixwr;
	scalar rad, delx, dely, norm, fact1, fact2, term1, term2;
	scalar *hetero, *count;
	char fname[100];
	FILE *fid;



	/*   Nrad = ceil(sqrt(pow((scalar)(NX/2),2) + pow((scalar)(NY/2),2)))+1; */
	Nrad = NX + 1;
	hetero = (scalar *) malloc (sizeof (scalar) * Nrad);
	count = (scalar *) malloc (sizeof (scalar) * Nrad);

	for (irad = 0; irad < Nrad; irad++)
		hetero[irad] = count[irad] = 0.0;


	ncenter = 0;
	for (icen = 1; icen <= NX; icen += 1) {
		for (jcen = 1; jcen <= NY; jcen += 1) {
			idx0 = IDX (icen, jcen);
			term1 = (hist1[idx0] + hist2[idx0]);
			fact1 = hist1[idx0] / term1;
			if (term1 != 0) {
				for (i = 1; i <= NX; i++) {
					for (j = 1; j <= NY; j++) {

						ixwr =
							(i + icen) - floor ((double) (i + icen) / (double) NX) * NX;

						/*      idx = IDX(i,j); */
						idx = IDX (ixwr, j);

						/*
							 delx = fabs(i-icen);
							 dely = fabs(j-jcen);
							 if(delx>NX/2)delx=NX-delx;
							 if(dely>NY/2)dely=NY-dely;
							 rad = sqrt(delx*delx+dely*dely);
							 irad = floor(rad);
							 */

						/*      term2 = (hist1[idx]+hist2[idx]);         */
						term2 = (hist1[ixwr] + hist2[ixwr]);
						if (term2 != 0) {
							/*
								 fact2 = hist1[idx]/term2;
								 hetero[irad] += 2.0*fact1*(1.0-fact2);
								 count[irad] +=1;
								 */
							fact2 = hist1[ixwr] / term2;
							hetero[i - 1] += 2.0 * fact1 * (1.0 - fact2);
							count[i - 1] += 1;
						}
					}
				}
				ncenter++;
			}
		}
	}

	sprintf (fname, "hetero.itime.%d.me.%d.dat", itime, me);
	fid = fopen (fname, "w");
	/*   for(irad=0;irad<Nrad;irad++){ */
	for (irad = 0; irad < NX; irad++) {
		fprintf (fid, "%g %g  %g \n", irad * dx, hetero[irad] / ncenter,
				hetero[irad] / count[irad]);
	}
	fclose (fid);
	free (hetero);
}



	void
heterozygosity (part * c1, part * c2, int itime, int me)
{

	int ipart1, ipart2, Nrad, irad;
	scalar xwrp1, ywrp1, xwrp2, ywrp2, delx, dely;
	scalar *hetrr, *hetrg, *hetgr, *hetgg;
	char fname[100];
	FILE *fid;



	Nrad =
		ceil (sqrt (pow ((scalar) (NX / 2), 2) + pow ((scalar) (NY / 2), 2))) + 1;
	hetrr = (scalar *) malloc (sizeof (scalar) * Nrad);
	hetrg = (scalar *) malloc (sizeof (scalar) * Nrad);
	hetgr = (scalar *) malloc (sizeof (scalar) * Nrad);
	hetgg = (scalar *) malloc (sizeof (scalar) * Nrad);

	for (irad = 0; irad < Nrad; irad++)
		hetrr[irad] = hetrg[irad] = hetgr[irad] = hetgg[irad] = 0.;


	/* The red-green pair correlation function */
	for (ipart1 = 0; ipart1 < Npart1; ipart1++) {
		xwrp1 = c1[ipart1].x - floor (c1[ipart1].x / lenx) * lenx;
		ywrp1 = c1[ipart1].y - floor (c1[ipart1].y / leny) * leny;


		for (ipart2 = 0; ipart2 < Npart2; ipart2++) {

			xwrp2 = c2[ipart2].x - floor (c2[ipart2].x / lenx) * lenx;
			ywrp2 = c2[ipart2].y - floor (c2[ipart2].y / leny) * leny;

			delx = fabs (xwrp2 - xwrp1);
			dely = fabs (ywrp2 - ywrp1);

			if (delx > lenx / 2)
				delx = lenx - delx;
			if (dely > leny / 2)
				dely = leny - dely;

			/* We are assuming that dr=dx here */
			irad = floor (sqrt (delx * delx + dely * dely) / dx);
			if (irad != 0) {
				hetrg[irad] += 1;
			}
		}
	}


	/* The green-red pair correlation function */
	/* This can be removed as by symmetry the red-green correlation 
		 is exactly same as the green-red correlation */
	for (ipart2 = 0; ipart2 < Npart2; ipart2++) {
		xwrp1 = c2[ipart2].x - floor (c2[ipart2].x / lenx) * lenx;
		ywrp1 = c2[ipart2].y - floor (c2[ipart2].y / leny) * leny;


		for (ipart1 = 0; ipart1 < Npart1; ipart1++) {

			xwrp2 = c1[ipart1].x - floor (c1[ipart1].x / lenx) * lenx;
			ywrp2 = c1[ipart1].y - floor (c1[ipart1].y / leny) * leny;

			delx = fabs (xwrp2 - xwrp1);
			dely = fabs (ywrp2 - ywrp1);

			if (delx > lenx / 2)
				delx = lenx - delx;
			if (dely > leny / 2)
				dely = leny - dely;


			irad = floor (sqrt (delx * delx + dely * dely) / dx);
			if (irad != 0) {
				hetgr[irad] += 1;
			}
		}
	}



	/* The red-red pair correlation function */
	/* Here I am doing double counting intentionally. This might 
		 make the code slightly slower. But after tests the inner 
		 loop should be changed to (ipart1+1) and then on hetrr we 
		 should add 2. For proper normalization such that g(r) is 
		 1 for large r use the trick described in:
http://www.physics.emory.edu/~weeks/idl/gofr2.html
*/
	for (ipart1 = 0; ipart1 < Npart1; ipart1++) {
		xwrp1 = c1[ipart1].x - floor (c1[ipart1].x / lenx) * lenx;
		ywrp1 = c1[ipart1].y - floor (c1[ipart1].y / leny) * leny;


		for (ipart2 = 0; ipart2 < Npart1; ipart2++) {

			xwrp2 = c1[ipart2].x - floor (c1[ipart2].x / lenx) * lenx;
			ywrp2 = c1[ipart2].y - floor (c1[ipart2].y / leny) * leny;

			delx = fabs (xwrp2 - xwrp1);
			dely = fabs (ywrp2 - ywrp1);

			if (delx > lenx / 2)
				delx = lenx - delx;
			if (dely > leny / 2)
				dely = leny - dely;

			irad = floor (sqrt (delx * delx + dely * dely) / dx);
			if (irad != 0) {
				hetrr[irad] += 1;
			}
		}
	}



	/* The green-green pair correlation function */
	/* Here I am doing double counting intentionally. This might 
		 make the code slightly slower. But after tests the inner 
		 loop should be changed to (ipart1+1) and then on hetrr we 
		 should add 2. For proper normalization such that g(r) is 
		 1 for large r use the trick described in:
http://www.physics.emory.edu/~weeks/idl/gofr2.html
*/
	for (ipart1 = 0; ipart1 < Npart2; ipart1++) {
		xwrp1 = c2[ipart1].x - floor (c2[ipart1].x / lenx) * lenx;
		ywrp1 = c2[ipart1].y - floor (c2[ipart1].y / leny) * leny;


		for (ipart2 = 0; ipart2 < Npart2; ipart2++) {

			xwrp2 = c2[ipart2].x - floor (c2[ipart2].x / lenx) * lenx;
			ywrp2 = c2[ipart2].y - floor (c2[ipart2].y / leny) * leny;

			delx = fabs (xwrp2 - xwrp1);
			dely = fabs (ywrp2 - ywrp1);

			if (delx > lenx / 2)
				delx = lenx - delx;
			if (dely > leny / 2)
				dely = leny - dely;


			irad = floor (sqrt (delx * delx + dely * dely) / dx);
			if (irad != 0) {
				hetgg[irad] += 1;
			}
		}
	}


	sprintf (fname, "data/hetero.itime.%d.me.%d.dat", itime, me);
	fid = fopen (fname, "w");
	for (irad = 1; irad < Nrad; irad++) {
		fprintf (fid, " %g %g %g %g %g %g \n", irad * dx, hetrg[irad],
				hetgr[irad], hetrr[irad], hetgg[irad],
				(hetrg[irad] + hetgr[irad]) / (hetrr[irad] + hetrg[irad] +
					hetgr[irad] + hetgg[irad]));
	}
	fclose (fid);

	free (hetrr);
	free (hetrg);
	free (hetgr);
	free (hetgg);

}
#endif





	void
gen_random (double *rnum, int nran)
{
	int iter, ridx;
	double x, y, z, r2;
	const double sigma = 1.e0;
	double sig;

	ridx = 0;
	while (ridx < nran) {
		do {
			x = -1 + 2 * drand48 ();
			y = -1 + 2 * drand48 ();
			r2 = x * x + y * y;
		}
		while (r2 > 1.0 || r2 == 0);
		rnum[ridx] = sigma * x * sqrt (-2.0 * log (r2) / r2);
		rnum[ridx + 1] = sigma * y * sqrt (-2.0 * log (r2) / r2);
		ridx += 2;
	}
}

#ifdef CHAOTIC_FLOW_JB
	void
gen_psi (cmplx * psi, cmplx * duxdt, cmplx * duydt, double *ksqr, double *lam,
		double *kx, double *ky)
{
	int i, j;
	double rnum[2];
	double rhs1, rhs2;

	psi[0].real = 0.0;
	psi[0].imag = 0.0;



	for (i = 1; i < 3; i++) {
		gen_random (rnum, 1);
		rhs1 =
			(-nu * ksqr[i] * psi[i].real * delta +
			 sqrt (nu * lam[i]) * rnum[0] * sqrt (delta));
		rhs2 =
			(-nu * ksqr[i] * psi[i].imag * delta +
			 sqrt (nu * lam[i]) * rnum[1] * sqrt (delta));
		psi[i].real += rhs1;
		psi[i].imag += rhs2;

		duxdt[i].real = -ky[i] * rhs2;
		duxdt[i].imag = ky[i] * rhs1;

		duydt[i].real = kx[i] * rhs2;
		duydt[i].imag = -kx[i] * rhs1;

	}

	for (i = 3; i < 5; i++) {
		psi[i].real = psi[i - 2].real;
		psi[i].imag = -psi[i - 2].imag;

		duxdt[i].real = duxdt[i - 2].real;
		duxdt[i].imag = -duxdt[i - 2].imag;

		duydt[i].real = duydt[i - 2].real;
		duydt[i].imag = -duydt[i - 2].imag;
	}


	for (i = 5; i < 7; i++) {
		gen_random (rnum, 1);
		rhs1 =
			(-nu * ksqr[i] * psi[i].real * delta +
			 sqrt (nu * lam[i]) * rnum[0] * sqrt (delta));
		rhs2 =
			(-nu * ksqr[i] * psi[i].imag * delta +
			 sqrt (nu * lam[i]) * rnum[1] * sqrt (delta));

		psi[i].real += rhs1;
		psi[i].imag += rhs2;

		duxdt[i].real = -ky[i] * rhs2;
		duxdt[i].imag = ky[i] * rhs1;

		duydt[i].real = kx[i] * rhs2;
		duydt[i].imag = -kx[i] * rhs1;
	}

	for (i = 7; i < 9; i++) {
		psi[i].real = psi[i - 2].real;
		psi[i].imag = -psi[i - 2].imag;
		duxdt[i].real = duxdt[i - 2].real;
		duxdt[i].imag = -duxdt[i - 2].imag;

		duydt[i].real = duydt[i - 2].real;
		duydt[i].imag = -duydt[i - 2].imag;

	}


}

	void
init_lam_kx_ky (double *lam, double *ksqr, double *kx, double *ky,
		cmplx * psi, cmplx * duxdt, cmplx * duydt, double famp)
{
	int i, j;
	double kfacx, kfacy, rnum[2];
	kfacx = (2.0 * M_PI / lenx);
	kfacy = (2.0 * M_PI / leny);



	/*! Set the wave-vectors */
	kx[0] = 0.0;
	ky[0] = 0.0;
	kx[1] = 1.0 * kfacx;
	ky[1] = 0.0;
	kx[2] = 0.0;
	ky[2] = 1.0 * kfacy;
	kx[3] = -1.0 * kfacx;
	ky[3] = 0.0;
	kx[4] = 0.0;
	ky[4] = -1.0 * kfacy;
	kx[5] = 1.0 * kfacx;
	ky[5] = 1.0 * kfacy;
	kx[6] = -1.0 * kfacx;
	ky[6] = 1.0 * kfacy;
	kx[7] = -1.0 * kfacx;
	ky[7] = -1.0 * kfacy;
	kx[8] = 1.0 * kfacx;
	ky[8] = -1.0 * kfacy;


	/*! Determine ksqr and set other variables to 0 */
	double sumlam = 0.;
	for (i = 0; i < 9; i++) {

		ksqr[i] = kx[i] * kx[i] + ky[i] * ky[i];

		psi[i].real = 0.0;
		psi[i].imag = 0.0;

		duxdt[i].real = 0.0;
		duxdt[i].imag = 0.0;

		lam[i] = ksqr[i] * pow ((1.0 + ksqr[i]), -7. / 3.);
		sumlam += lam[i];

	}

	/*! Normalize lambda_k */
	/*! The energy in the system is given by E_k=0.5 \sum_k \lam_k */
	for (i = 0; i < 9; i++)
		lam[i] = 2.0 * famp * lam[i] / sumlam;


	/*! Now that we have properly normalize lambda we set the streamfunction */
	double fact;
	for (i = 0; i < 9; i++) {
		gen_random (rnum, 2);
		if (ksqr[i] != 0) {
			fact = lam[i] / (2.0 * ksqr[i]);
		}
		else {
			fact = 0.0;
		}
		psi[i].real = fact * rnum[0];
		psi[i].imag = fact * rnum[1];

	}

}

#ifdef COMPRES
	void
eval_compres (cmplx * psi, int itime, int me)
{
	int i, j, k;
	int idx1;
	double xpos, ypos, uxc, uyc, uxi, uyi, uxint, uyint, ene;
	double dxux, dxuy, dyux, dyuy;
	double *utmp, *vtmp;
	char fname[100];
	FILE *fid2;


	utmp = (double *) malloc (sizeof (double) * NXP2 * NYP2);
	vtmp = (double *) malloc (sizeof (double) * NXP2 * NYP2);


	sprintf (fname, "energy.me%d.dat", me);
	fid2 = fopen (fname, "a");

	ene = 0.0;
	for (i = 0; i < NX; i++) {
		for (j = 0; j < NY; j++) {
			idx1 = (j + NY * i);
			xpos = i * dx;
			ypos = j * dy;
			uxint = 0.0;
			uyint = 0.0;

			uxc = uyc = uxi = uyi = 0.0;
			/* Pure incompressible flow */
			for (k = 0; k < 9; k++) {

				uxi += (-ky[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) +
						ky[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));

				uyi += (kx[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) -
						kx[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));
			}


			/* Pure compressible flow */
			for (k = 0; k < 9; k++) {

				uxc += (kx[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) -
						kx[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));

				uyc += (ky[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) -
						ky[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));
			}

			uxint = sqrt (1.0 - kappa) * uxi + sqrt (kappa) * uxc;
			uyint = sqrt (1.0 - kappa) * uyi + sqrt (kappa) * uyc;

			utmp[IDX (i, j)] = uxint;
			vtmp[IDX (i, j)] = uyint;

			ene += 0.5 * (uxint * uxint + uyint * uyint);
		}
	}


	/* Evaluate compressibility */
	double divu2, gradu2;
	dxux = dxuy = dyux = dyuy = 0.0;
	divu2 = gradu2 = 0.0;
	for (i = 2; i < NX - 1; i++) {
		for (j = 2; j < NY - 1; j++) {
			dxux = (utmp[IDX ((i + 1), j)] - utmp[IDX ((i - 1), j)]) / (2.0 * dx);
			dxuy = (vtmp[IDX ((i + 1), j)] - vtmp[IDX ((i - 1), j)]) / (2.0 * dy);
			dyux = (utmp[IDX (i, (j + 1))] - utmp[IDX (i, (j - 1))]) / (2.0 * dx);
			dyuy = (vtmp[IDX (i, (j + 1))] - vtmp[IDX (i, (j - 1))]) / (2.0 * dy);

			divu2 += pow ((dxux + dyuy), 2);
			gradu2 += dxux * dxux + dxuy * dxuy + dyux * dyux + dyuy * dyuy;
		}
	}




	fprintf (fid2, "itime: %d Energy: %g Compres %g \n", itime,
			ene / (double) (NX * NY), (divu2 / gradu2));
	fclose (fid2);
	free (utmp);
	free (vtmp);
}
#endif



#ifdef DUMP_ENERGY
	void
snap_psi (cmplx * psi, int itime, int me)
{
	int i, j, k;
	int idx1;
	double xpos, ypos, sfun, uxint, uyint, ene;
	char fname[100];
	FILE *fid, *fid2;


	sprintf (fname, "energy.me%d.dat", me);
	fid2 = fopen (fname, "a");

	sprintf (fname, "data/psi.%d.dat.%d", itime, me);
	fid = fopen (fname, "w");

	ene = 0.0;
	for (i = 0; i < NX; i++) {
		for (j = 0; j < NY; j++) {
			idx1 = (j + NY * i);
			xpos = i * dx;
			ypos = j * dy;
			sfun = 0.0;
			uxint = 0.0;
			uyint = 0.0;

			for (k = 0; k < 9; k++) {

				sfun += (psi[k].real * cos (kx[k] * xpos + ky[k] * ypos) +
						psi[k].imag * sin (kx[k] * xpos + ky[k] * ypos));
				uxint += (-ky[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) +
						ky[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));

				uyint += (kx[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) -
						kx[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));

			}
			ene += 0.5 * (uxint * uxint + uyint * uyint);
			fprintf (fid, "%g %g %g %g %g \n", xpos, ypos, sfun, uxint, uyint);
		}
		fprintf (fid, "\n");
	}
	fclose (fid);

	fprintf (fid2, "itime: %d Energy: %g \n", itime, ene / (double) (NX * NY));
	fclose (fid2);
}
#endif
#endif



	void
initialize (int Npart, part * c1, double famp, int flagp)
{
	int ipart;
	double xpos, ypos, uxint, uyint, psi;
	int ix, iy;
	FILE *fin;

	ix = 0;
	for (ipart = 0; ipart < Npart; ipart++) {

		c1[ipart].x = lenx * drand48 ();
		c1[ipart].y = leny * drand48 ();

		c1[ipart].flag = 1;
		xpos = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
		ypos = c1[ipart].y - floor (c1[ipart].y / leny) * leny;
		//   printf("%g %g \n",c1[ipart].x,c1[ipart].y);




		c1[ipart].vx = 0.;
		c1[ipart].vy = 0.;

		c1[ipart].vx0 = 0.;
		c1[ipart].vy0 = 0.;

#ifdef INERTIAL
		c1[ipart].vxrhs0 = 0.;
		c1[ipart].vyrhs0 = 0.;
#endif
		//dump_position(Npart1,c1,hist1,0,1);  
		//dump_position(Npart2,c2,hist2,0,2);  

	}
}


	void
find_size_part (int *Npart1, int *Npart2)
{

	char fname[100];
	part dummy;
	char dum1;
	FILE *fin;
	int ck;

	//if((fin=fopen(fname,"r"))==NULL){
	//fprintf(stderr,"Unable to open file \n");
	//exit(1);
	//}
	//count=0;
	//while(!feof(fin)){
	//ck=fread(&dummy,sizeof(part),1,fin);
	*Npart1 = 0;
	*Npart2 = 0;
	sprintf (fname, "data_restart/particle_dump1.me%d.bin", me);
	fin = fopen (fname, "r");
	while (fread (&dummy, sizeof (part), 1, fin) != 0)
		*Npart1 += 1;
	fclose (fin);

	sprintf (fname, "data_restart/particle_dump2.me%d.bin", me);
	fin = fopen (fname, "r");
	while (fread (&dummy, sizeof (part), 1, fin) != 0)
		*Npart2 += 1;
	fclose (fin);

#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif
}


	void
restore_particle (int Npart1, int Npart2, part * c1, part * c2, double fampi,
		int *itime, int me)
{

	char fname[100];
	FILE *fin;

	sprintf (fname, "data_restart/particle_dump1.me%d.bin", me);
	fin = fopen (fname, "r");
	fread (c1, Npart1, sizeof (part), fin);
	fclose (fin);

	sprintf (fname, "data_restart/particle_dump2.me%d.bin", me);
	fin = fopen (fname, "r");
	fread (c2, Npart2, sizeof (part), fin);
	fclose (fin);


#ifdef CHAOTIC_FLOW_JB
	sprintf (fname, "data_restart/psi_dump.me%d.bin", me);
	fin = fopen (fname, "r");
	fread (psi, 9, sizeof (cmplx), fin);
	fclose (fin);
#endif
	sprintf (fname, "data_restart/time_index.me%d.dat", me);
	fin = fopen (fname, "r");
	fscanf (fin, "%d \n", itime);
	fclose (fin);
}



	void
initialize_tmp (int Npart1, int Npart2, part * c1, part * c2, double famp)
{
	int ipart1, ipart2;
	double xpos, ypos, uxint, uyint, psi, ran;
	int ix, iy;
	FILE *fin;


	/*
		 ipart1=ipart2=0;
		 while((ipart1+ipart2)<(Npart1+Npart2)){

		 ran=drand48();
		 if(ran<0.5){
		 c1[ipart1].x=lenx*ran;
		 c1[ipart1].y=leny*drand48();
		 c1[ipart1].flag = 1;
		 xpos = c1[ipart1].x - floor(c1[ipart1].x/lenx)*lenx;
		 ypos = c1[ipart1].y - floor(c1[ipart1].y/leny)*leny;
		 ipart1++;
		 }else{
		 c2[ipart2].x=lenx*ran;
		 c2[ipart2].y=leny*drand48();
		 c2[ipart2].flag = 1;
		 xpos = c2[ipart2].x - floor(c2[ipart2].x/lenx)*lenx;
		 ypos = c2[ipart2].y - floor(c2[ipart2].y/leny)*leny;
		 ipart2++;
		 }

		 }
		 */
	fin = fopen ("part1.dat", "r");
	for (ipart1 = 0; ipart1 < Npart1; ipart1++) {
		fscanf (fin, "%d %lf %lf \n", &ix, &c1[ipart1].x, &c1[ipart1].y);
		fprintf (stderr, "%g %g \n", c1[ipart1].x, c1[ipart1].y);
	}
	fclose (fin);

	fin = fopen ("part2.dat", "r");
	for (ipart2 = 0; ipart2 < Npart2; ipart2++) {
		fscanf (fin, "%d %lf %lf \n", &ix, &c2[ipart2].x, &c2[ipart2].y);
		fprintf (stderr, "%g %g \n", c2[ipart2].x, c1[ipart2].y);
	}
	fclose (fin);

}



	int
compare_int (const void *a, const void *b)
{

	part *c_a = (part *) a;
	part *c_b = (part *) b;

	int temp = c_b[0].flag - c_a[0].flag;

	if (temp > 0)
		return 1;
	else if (temp < 0)
		return -1;
	else
		return 0;
}


	void
flow_and_random_walk (int Npart, part * c1, bivel * u, double famp,
		int ptype_flag, int itime)
{
	int ipart;
	double rnum[2];
	double uxint, uyint;

	double xpos, ypos;
	double xg, yg, xgp1, ygp1;
	int ix, iy;
#ifdef INERTIAL
	double rhs, dxux, dyuy, dxuy, dyux, dudt, dvdt, Duxdt, Duydt;
#endif
	double invdxdy = 1.0 / (dx * dy);
	double length = lenx;
#ifdef CHAOTIC_FLOW_JB
	int k;
#endif
#ifdef COMPRES
	double uxi, uyi, uxc, uyc;
#endif

	for (ipart = 0; ipart < Npart; ipart++) {
		/* Generate gaussian random deviate with zero mean and unit 
			 standard deviation */
#ifdef SINE
		c1[ipart].vx = famp * sin (2.0 * M_PI * c1[ipart].x / lenx);
		c1[ipart].vy = 0.0;
#endif

#ifdef HARMONIC
		xpos = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
		ypos = c1[ipart].y - floor (c1[ipart].y / leny) * leny;
		c1[ipart].vx = 3.0 * famp * (0.5 - xpos);
		c1[ipart].vy = 1.0 * famp * (0.5 - ypos);
#endif

#ifdef CHAOTIC_FLOW
		xpos = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
		ypos = c1[ipart].y - floor (c1[ipart].y / leny) * leny;

		/* u_x = -\partial_y \psi, u_y = \partial_x \psi */
		uxint =
			famp *
			(sin (2.0 * M_PI * xpos / length + B * sin (phi * itime * delta)) *
			 cos (2.0 * M_PI * ypos / length));
		uyint =
			-famp *
			(cos (2.0 * M_PI * xpos / length + B * sin (phi * itime * delta)) *
			 sin (2.0 * M_PI * ypos / length));

		dudt =
			famp *
			(cos (2.0 * M_PI * xpos / length + B * sin (phi * itime * delta)) *
			 cos (2.0 * M_PI * ypos / length) * (B * phi *
				 cos (phi * itime * delta)));
		dvdt =
			famp *
			(sin (2.0 * M_PI * xpos / length + B * sin (phi * itime * delta)) *
			 sin (2.0 * M_PI * ypos / length) * (B * phi *
				 cos (phi * itime * delta)));

		dxux = famp * (2.0 * M_PI / length) * (cos (2.0 * M_PI * xpos / length +
					B * sin (phi * itime *
						delta)) * cos (2.0 *
						M_PI *
						ypos /
						length));

		dyuy = -dxux;

		dxuy = famp * (2.0 * M_PI / length) * (sin (2.0 * M_PI * xpos / length +
					B * sin (phi * itime *
						delta)) * sin (2.0 *
						M_PI *
						ypos /
						length));
		dyux = -dxuy;

		Duxdt = dudt + uxint * dxux + uyint * dyux;
		Duydt = dvdt + uxint * dxuy + uyint * dyuy;


#ifdef INERTIAL
		double rhsx, rhsy;
		if (itime == 0) {
			if (ptype_flag == 1) {
				rhsx = (-(c1[ipart].vx - uxint) / taup1 + beta1 * Duxdt);
				rhsy = (-(c1[ipart].vy - uyint) / taup1 + beta1 * Duydt);
				c1[ipart].vx += rhsx * delta;
				c1[ipart].vy += rhsy * delta;
				c1[ipart].vxrhs0 = rhsx;
				c1[ipart].vyrhs0 = rhsy;
			}
			else {
				rhsx = (-(c1[ipart].vx - uxint) / taup2 + beta2 * Duxdt);
				rhsy = (-(c1[ipart].vy - uyint) / taup2 + beta2 * Duydt);
				c1[ipart].vx += rhsx * delta;
				c1[ipart].vy += rhsy * delta;
				c1[ipart].vxrhs0 = rhsx;
				c1[ipart].vyrhs0 = rhsy;
			}
		}
		else {
			if (ptype_flag == 1) {
				rhsx = (-(c1[ipart].vx - uxint) / taup1 + beta1 * Duxdt);
				rhsy = (-(c1[ipart].vy - uyint) / taup1 + beta1 * Duydt);
				c1[ipart].vx += 0.5 * (3.0 * rhsx - c1[ipart].vxrhs0) * delta;
				c1[ipart].vy += 0.5 * (3.0 * rhsy - c1[ipart].vyrhs0) * delta;
				c1[ipart].vxrhs0 = rhsx;
				c1[ipart].vyrhs0 = rhsy;
			}
			else {
				rhsx = (-(c1[ipart].vx - uxint) / taup2 + beta2 * Duxdt);
				rhsy = (-(c1[ipart].vy - uyint) / taup2 + beta2 * Duydt);
				c1[ipart].vx += 0.5 * (3.0 * rhsx - c1[ipart].vxrhs0) * delta;
				c1[ipart].vy += 0.5 * (3.0 * rhsy - c1[ipart].vyrhs0) * delta;
				c1[ipart].vxrhs0 = rhsx;
				c1[ipart].vyrhs0 = rhsy;
			}
		}
#else
		c1[ipart].vx = uxint;
		c1[ipart].vy = uyint;
#endif
		/* --------------- */
#endif

#ifdef CHAOTIC_FLOW_JB
		uxint = 0.0;
		uyint = 0.0;
		xpos = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
		ypos = c1[ipart].y - floor (c1[ipart].y / leny) * leny;

#ifndef COMPRES
		for (k = 0; k < 9; k++) {

			uxint += (-ky[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) +
					ky[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));

			uyint += (kx[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) -
					kx[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));
		}
#else
#ifdef COMPRES
		uxc = uyc = uxi = uyi = 0.0;
		/* Pure incompressible flow */
		for (k = 0; k < 9; k++) {

			uxi += (-ky[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) +
					ky[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));

			uyi += (kx[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) -
					kx[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));
		}


		/* Pure compressible flow */
		for (k = 0; k < 9; k++) {

			uxc += (kx[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) -
					kx[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));

			uyc += (ky[k] * psi[k].imag * cos (kx[k] * xpos + ky[k] * ypos) -
					ky[k] * psi[k].real * sin (kx[k] * xpos + ky[k] * ypos));
		}

		uxint = sqrt (1.0 - kappa) * uxi + sqrt (kappa) * uxc;
		uyint = sqrt (1.0 - kappa) * uyi + sqrt (kappa) * uyc;
#endif /* COMPRESS */
#endif



#ifdef INERTIAL
		Duxdt = 0.0;
		Duydt = 0.0;
		dxux = 0.0;
		dyux = 0.0;
		dxuy = 0.0;
		dyuy = 0.0;

		for (k = 0; k < 9; k++) {

			rhs = (psi[k].real * cos (kx[k] * xpos + ky[k] * ypos) +
					psi[k].imag * sin (kx[k] * xpos + ky[k] * ypos));
			dxux += kx[k] * ky[k] * rhs;
			dyuy += -kx[k] * ky[k] * rhs;
			dxuy += -kx[k] * kx[k] * rhs;
			dyux += ky[k] * ky[k] * rhs;

			Duxdt += (duxdt[k].real * cos (kx[k] * xpos + ky[k] * ypos) +
					duxdt[k].imag * sin (kx[k] * xpos + ky[k] * ypos));

			Duydt += (duydt[k].real * cos (kx[k] * xpos + ky[k] * ypos) +
					duydt[k].imag * sin (kx[k] * xpos + ky[k] * ypos));

		}
		Duxdt += uxint * dxux + uyint * dyux;
		Duydt += uxint * dxuy + uyint * dyuy;
#endif

#ifdef INERTIAL
		double rhsx, rhsy;
		if (itime == 0) {
			if (ptype_flag == 1) {
				rhsx = (-(c1[ipart].vx - uxint) / taup1 + beta1 * Duxdt);
				rhsy = (-(c1[ipart].vy - uyint) / taup1 + beta1 * Duydt);
				c1[ipart].vx += rhsx * delta;
				c1[ipart].vy += rhsy * delta;
				c1[ipart].vxrhs0 = rhsx;
				c1[ipart].vyrhs0 = rhsy;
			}
			else {
				rhsx = (-(c1[ipart].vx - uxint) / taup2 + beta2 * Duxdt);
				rhsy = (-(c1[ipart].vy - uyint) / taup2 + beta2 * Duydt);
				c1[ipart].vx += rhsx * delta;
				c1[ipart].vy += rhsy * delta;
				c1[ipart].vxrhs0 = rhsx;
				c1[ipart].vyrhs0 = rhsy;
			}
		}
		else {
			if (ptype_flag == 1) {
				rhsx = (-(c1[ipart].vx - uxint) / taup1 + beta1 * Duxdt);
				rhsy = (-(c1[ipart].vy - uyint) / taup1 + beta1 * Duydt);
				c1[ipart].vx += 0.5 * (3.0 * rhsx - c1[ipart].vxrhs0) * delta;
				c1[ipart].vy += 0.5 * (3.0 * rhsy - c1[ipart].vyrhs0) * delta;
				c1[ipart].vxrhs0 = rhsx;
				c1[ipart].vyrhs0 = rhsy;
			}
			else {
				rhsx = (-(c1[ipart].vx - uxint) / taup2 + beta2 * Duxdt);
				rhsy = (-(c1[ipart].vy - uyint) / taup2 + beta2 * Duydt);
				c1[ipart].vx += 0.5 * (3.0 * rhsx - c1[ipart].vxrhs0) * delta;
				c1[ipart].vy += 0.5 * (3.0 * rhsy - c1[ipart].vyrhs0) * delta;
				c1[ipart].vxrhs0 = rhsx;
				c1[ipart].vyrhs0 = rhsy;
			}
		}
#else
		c1[ipart].vx = uxint;
		c1[ipart].vy = uyint;
#endif
#endif


#ifdef  DETERMINISTIC_FORCE_COMPRES
		xpos = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
		ypos = c1[ipart].y - floor (c1[ipart].y / leny) * leny;

		uxi = sin (2.0 * M_PI * ypos / leny);
		uyi = -sin (2.0 * M_PI * xpos / lenx);

		uxc = sin (2.0 * M_PI * xpos / lenx);
		uyc = sin (2.0 * M_PI * ypos / leny);

		uxint = (1.0 - alpha) * uxi + (alpha) * uxc;
		uyint = (1.0 - alpha) * uyi + (alpha) * uyc;

		c1[ipart].vx = uxint;
		c1[ipart].vy = uyint;
#endif

#ifdef EXTVEL
		/* This was wrong
			 xpos = c1[ipart].x - floor(c1[ipart].x/lenx)*delta/lenx;
			 ypos = c1[ipart].y - floor(c1[ipart].y/leny)*delta/leny;
			 */

		xpos = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
		ypos = c1[ipart].y - floor (c1[ipart].y / leny) * leny;

		xg = floor (xpos / dx) * dx;
		xgp1 = xg + dx;
		yg = floor (ypos / dy) * dy;
		ygp1 = yg + dy;

		ix = floor (xpos / dx);
		iy = floor (ypos / dy);

		uxint = invdxdy * ((xgp1 - xpos) * (ygp1 - ypos) * u[IDX (ix, iy)].x +
				(xgp1 - xpos) * (ypos - yg) * u[IDX (ix, (iy + 1))].x +
				(xpos - xg) * (ygp1 - ypos) * u[IDX ((ix + 1), iy)].x +
				(xpos - xg) * (ypos -
					yg) * u[IDX ((ix + 1), (iy + 1))].x);

		uyint = invdxdy * ((xgp1 - xpos) * (ygp1 - ypos) * u[IDX (ix, iy)].y +
				(xgp1 - xpos) * (ypos - yg) * u[IDX (ix, (iy + 1))].y +
				(xpos - xg) * (ygp1 - ypos) * u[IDX ((ix + 1), iy)].y +
				(xpos - xg) * (ypos -
					yg) * u[IDX ((ix + 1), (iy + 1))].y);
		c1[ipart].vx = uxint;
		c1[ipart].vy = uyint;
#endif

		gen_random (rnum, 2);

		c1[ipart].x += rnum[0] * sqrt (2.0 * D * delta);

		c1[ipart].y += rnum[1] * sqrt (2.0 * D * delta);

#if defined CHAOTIC_FLOW || defined SINE || defined CHAOTIC_FLOW_JB || defined HARMONIC || defined EXTVEL || defined DETERMINISTIC_FORCE_COMPRES
		if (itime == 0) {
			c1[ipart].vx0 = c1[ipart].vx;
			c1[ipart].vy0 = c1[ipart].vy;
		}
		c1[ipart].x += 0.5 * (3.0 * c1[ipart].vx - c1[ipart].vx0) * delta;
		c1[ipart].y += 0.5 * (3.0 * c1[ipart].vy - c1[ipart].vy0) * delta;
		c1[ipart].vx0 = c1[ipart].vx;
		c1[ipart].vy0 = c1[ipart].vy;
#endif
	}
}



	void
dump_position (int Npart, part * c1, scalar * hist, int itime, int specie)
{
	int ipart, ix, iy, idx;
	char fname[100];
	FILE *fid;

	sprintf (fname, "data/part_post.%d.%d.%d.dat", specie, me, itime);
	fid = fopen (fname, "w");
	for (ipart = 0; ipart < Npart; ipart++) {
		fprintf (fid, "%g %g %g \n", itime * delta, c1[ipart].x, c1[ipart].y);
	}
	fclose (fid);

	//  sprintf(fname,"data/hist.%d.%d.dat",specie,itime);
	sprintf (fname, "data/hist.%d.%d.%d.dat", specie, me, itime);
	fid = fopen (fname, "w");
	for (ix = 1; ix <= NX; ix++) {
		for (iy = 1; iy <= NY; iy++) {
			idx = IDX (ix, iy);
			fprintf (fid, "%d %d %g \n", ix, iy, hist[idx]);
		}
		fprintf (fid, "\n");
	}
	fclose (fid);
}


	void
list_neighbour (int Npart, scalar * hist, part * c1)
{
	int ipart;
	scalar xwrap, ywrap;
	int ix, iy, idx;

	for (ix = 1; ix <= NX; ix++)
		for (iy = 1; iy <= NY; iy++) {
			idx = IDX (ix, iy);
			hist[idx] = 0.0;
		}


	for (ipart = 0; ipart < Npart; ipart++) {
		/*
			 ix=(int)(floor(c1[ipart].x/dx))%NX+1;
			 iy=(int)(floor(c1[ipart].y/dy))%NY+1;
			 */
		xwrap = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
		ywrap = c1[ipart].y - floor (c1[ipart].y / leny) * leny;
		ix = floor (xwrap / dx) + 1;
		iy = floor (ywrap / dy) + 1;
		idx = IDX (ix, iy);
		//printf("ix=%d,iy=%d,idx=%d \n",ix,iy,idx);
		if (idx < 0)
			printf ("ix=%d,iy=%d,idx=%d \n", ix, iy, idx);
		if (idx > NXP2 * NYP2)
			printf ("ix=%d,iy=%d,idx=%d \n", ix, iy, idx);
		hist[idx] += 1.0;
	}
}







#ifdef EXTVEL
	void
create_plan ()
{
	plan =
		rfftw2d_create_plan (NX, NY, FFTW_REAL_TO_COMPLEX,
				FFTW_MEASURE | FFTW_IN_PLACE);
	iplan =
		rfftw2d_create_plan (NX, NY, FFTW_COMPLEX_TO_REAL,
				FFTW_MEASURE | FFTW_IN_PLACE);
}

	void
vel_divfree ()
{
	int i, j, kx, ky, idx1;
	double p11, p12, p21, p22, ksqr;
	double tux, tuy;


	for (i = 0; i < NX; i++) {
		kx = i - NX * floor (i / (NX / 2 + 1));
		for (j = 0; j <= NY / 2; j++) {
			ky = j;

			idx1 = j + (NY / 2 + 1) * i;

			ksqr = kx * kx + ky * ky;
			if (ksqr != 0) {
				p11 = (1.0 - kx * kx / ksqr);
				p12 = -kx * ky / ksqr;
				p21 = p12;
				p22 = (1.0 - ky * ky / ksqr);


				tux = p11 * cuf[idx1].x.re + p12 * cuf[idx1].y.re;
				tuy = p21 * cuf[idx1].x.re + p22 * cuf[idx1].y.re;

				cuf[idx1].x.re = tux;
				cuf[idx1].y.re = tuy;


				tux = p11 * cuf[idx1].x.im + p12 * cuf[idx1].y.im;
				tuy = p21 * cuf[idx1].x.im + p22 * cuf[idx1].y.im;

				cuf[idx1].x.im = tux;
				cuf[idx1].y.im = tuy;

			}
			else {
				cuf[idx1].x.re = 0.e0;
				cuf[idx1].y.re = 0.e0;

				cuf[idx1].x.im = 0.e0;
				cuf[idx1].y.im = 0.e0;
			}
		}
	}
}



	void
compres (bivel * u)
{
	int i, j, idx1, idx2;
	double ux, uy, uxinc, uyinc;
	double sing, cosg, sqrt2;

	/* Calculate the phase and sqrt2 */
	sing = sin (M_PI * phase / 180.e0);
	cosg = cos (M_PI * phase / 180.e0);
	sqrt2 = sqrt (2.e0);


	if (uf == NULL) {
		uf = (bu *) malloc (sizeof (bu) * NX * NYP2);
		cuf = (cbu *) uf;
	}


	for (i = 1; i <= NX; i++) {
		for (j = 1; j <= NY; j++) {
			idx1 = IDX (i, j);
			idx2 = (j - 1) + (i - 1) * NYP2;

			uf[idx2].x = u[idx1].x;
			uf[idx2].y = u[idx1].y;

		}
	}

	rfftwnd_real_to_complex (plan, 2, (fftw_real *) uf, 2, 1, NULL, 0, 0);

	vel_divfree ();

	rfftwnd_complex_to_real (iplan, 2, (fftw_complex *) cuf, 2, 1, NULL, 0, 0);

	for (i = 1; i <= NX; i++) {
		for (j = 1; j <= NY; j++) {
			idx1 = IDX (i, j);
			idx2 = (j - 1) + (i - 1) * NYP2;

			ux = u[idx1].x;
			uy = u[idx1].y;
			uxinc = uf[idx2].x / (NX * NY);
			uyinc = uf[idx2].y / (NX * NY);

			u[idx1].x = ((ux - uxinc) * sing + uxinc * cosg) * sqrt2;
			u[idx1].y = ((uy - uyinc) * sing + uyinc * cosg) * sqrt2;

		}
	}
}
#endif

	void
read_parameters (double *famp, int *particle_restore)
{
	FILE *fin;
	fin = fopen ("param.in", "r");
	fscanf (fin, "%d %d \n", &Npart1, &Npart2);
	fscanf (fin, "%lf %lf \n", &lenx, &leny);
	fscanf (fin, "%d %d \n", &NX, &NY);
	fscanf (fin, "%d \n", particle_restore);
#ifdef REACTION
	fscanf (fin, "%lf %lf \n", &mu1, &mu2);
#ifdef SIMONE
	fscanf (fin, "%lf \n", &gamma_death);
#endif
#ifdef MUTUALISM
	fscanf (fin, "%lf %lf \n", &lam_aa, &lam_ab);
	fscanf (fin, "%lf %lf \n", &lam_ba, &lam_bb);
#endif
#endif
	fscanf (fin, "%lf \n", famp);
	fscanf (fin, "%lf \n", &D);
	fscanf (fin, "%d \n", &ttime);
	fscanf (fin, "%d \n", &nout);
	fscanf (fin, "%lf \n", &delta);
#ifdef CHAOTIC_FLOW
	fscanf (fin, "%lf \n", &B);
	fscanf (fin, "%lf \n", &phi);
#endif
#ifdef EXTVEL
	fscanf (fin, "%lf \n", &phase);
#endif
#ifdef CHAOTIC_FLOW_JB
	fscanf (fin, "%lf \n", &nu);
#ifdef COMPRES
	fscanf (fin, "%lf \n", &kappa);
#endif
#endif
#ifdef DETERMINISTIC_FORCE_COMPRES
	fscanf (fin, "%lf \n", &kappa);
	alpha = (kappa - sqrt (kappa * (1.e0 - kappa))) / (2.0 * kappa - 1.e0);
#endif
#ifdef INERTIAL
	fscanf (fin, "%lf %lf \n", &taup1, &taup2);
	fscanf (fin, "%lf %lf \n", &beta1, &beta2);
#endif
	fclose (fin);
}


	void
write_parameters (double famp, int particle_restore, int me)
{
	FILE *fid;
	char fname[100];

	if ((me == 0) || (me == 4)) {
		sprintf (fname, "param.me.%d.out", me);
		fid = fopen (fname, "w");
		fprintf (fid, "Npart1= %d, Npart2= %d \n", Npart1, Npart2);
		fprintf (fid, "lenx= %lf, leny= %lf \n", lenx, leny);
		fprintf (fid, "NX= %d NY= %d \n", NX, NY);
		fprintf (fid, "famp= %g \n", famp);
		fprintf (fid, "D= %g \n", D);
		fprintf (fid, "ttime= %d \n", ttime);
		fprintf (fid, "nout= %d \n", nout);
		fprintf (fid, "delta= %lf \n", delta);
		fprintf (fid, "famp=%g \n", famp);
		fprintf (fid, "particle_restore=%d\n", particle_restore);

#ifdef REACTION
		fprintf (fid, "mu1= %g, mu2= %g \n", mu1, mu2);
#ifdef SIMONE
		fprintf (fid, "gamma_death=%g \n", gamma_death);
#endif
#ifdef MUTUALISM
		fprintf (fid, "lam_aa=%lf lam_ab=%lf \n", lam_aa, lam_ab);
		fprintf (fid, "lam_ba=%lf lam_bb=%lf \n", lam_ba, lam_bb);
		fprintf (fid, "epsilon_ab=%lf \n", (mu1 - (lam_ab / lam_bb) * mu2));
		fprintf (fid, "epsilon_ba=%lf \n", (mu2 - (lam_ba / lam_aa) * mu1));
#endif
#endif

#ifdef CHAOTIC_FLOW
		fprintf (fid, "B=%g \n", B);
		fprintf (fid, "phi=%g \n", phi);
#endif
#ifdef EXTVEL
		fprintf (fid, "%g \n", &phase);
#endif
#ifdef CHAOTIC_FLOW_JB
		fprintf (fid, "nu=%g \n", nu);
#ifdef COMPRES
		fprintf (fid, "kappa=%g \n", kappa);
#endif
#endif
#ifdef INERTIAL
		fprintf (fid, "%g %g \n", taup1, taup2);
		fprintf (fid, "%g %g \n", beta1, beta2);
#endif
#ifdef DETERMINISTIC_FORCE_COMPRES
		fprintf (fid, "%lf \n", kappa);
#endif
		fclose (fid);
	}
}



	void
restore_dump (part * c1, part * c2, int Npart1, int Npart2, int itime, int me)
{

	char fname[100];
	FILE *fid;

	sprintf (fname, "particle_dump1.me%d.bin", me);
	fid = fopen (fname, "w");
	fwrite (c1, sizeof (part), Npart1, fid);
	fclose (fid);

	sprintf (fname, "particle_dump2.me%d.bin", me);
	fid = fopen (fname, "w");
	fwrite (c2, sizeof (part), Npart2, fid);
	fclose (fid);

#ifdef CHAOTIC_FLOW_JB
	sprintf (fname, "psi_dump.me%d.bin", me);
	fid = fopen (fname, "w");
	fwrite (psi, sizeof (cmplx), 9, fid);
	fclose (fid);
#endif

	sprintf (fname, "time_index.me%d.dat", me);
	fid = fopen (fname, "w");
	fprintf (fid, "%d\n", itime);
	fclose (fid);

}






	int
main (int argc, char **argv)
{
	int ipart, ipart1, ipart2, itime;
	int count1, count2, bcount1, bcount2;
	int ipas;
	int i, j, k, idx1, idx2, istep, layer;
	int particle_restore;
	part *c1, *c2;
	bivel *u;

	scalar *hist1, *hist2;
#ifdef SIMONE
	int *reac;
#endif
	scalar xwrap, ywrap;
	int ix, iy, idx;
	scalar famp;
#ifdef EXTVEL
	bivel *pdump1;
	trivel *pdump;
#endif
	char fname[100];
	FILE *fid, *fout, *fin;
	FILE *fck;


#ifdef MPI
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);
#else
	if (argc != 2) {
		printf ("Format: ./a.out me\n");
		exit (1);
	}
	else {
		me = atof (argv[1]);
	}
#endif


	/*! Read input parameters from a file */
	int isample = 0;
	while (isample < 1) {
		read_parameters (&famp, &particle_restore);

#ifdef LOG_DUMP
		scalar lit, ljump;
		int ilit;
		lit = 0.0;
		ljump = (log10 ((ttime * delta))) / (scalar) nout;
#endif

		if (particle_restore == 1) {
			find_size_part (&Npart1, &Npart2);
		}
		/*! Write input parameters to a file */
		write_parameters (famp, particle_restore, me);






#ifdef EXTVEL
		/* Create the plan for FFTW */
		create_plan ();
#endif

		srand48 (me * 10 + time (NULL) + isample);
		/*  srand48(0); *//* Revert to the line above for production runs */


#if defined EXTVEL || defined CHAOTIC_FLOW
		sprintf (fname, "Npart_vs_time_famp.%g.phi.%g.D.%g.me.%d.isample.%d.dat",
				famp, phi, D, me, isample);
#else
#ifdef DETERMINISTIC_FORCE_COMPRES
		sprintf (fname,
				"Npart_vs_time_famp.%g.D.%g.kappa.%g.me.%d.isample.%d.dat", famp,
				D, kappa, me, isample);
#else
		sprintf (fname, "Npart_vs_time_famp.%g.D.%g.me.%d.isample.%d.dat", famp,
				D, me, isample);
#endif
#endif

		fid = fopen (fname, "w");
		c1 = (part *) malloc (sizeof (part) * Npart1);
		c2 = (part *) malloc (sizeof (part) * Npart2);
		hist1 = (scalar *) malloc (sizeof (scalar) * NXP2 * NYP2);
		hist2 = (scalar *) malloc (sizeof (scalar) * NXP2 * NYP2);
#ifdef SIMONE
		reac = (int *) malloc (sizeof (int) * NXP2 * NYP2);
#endif
		u = (bivel *) malloc (sizeof (bivel) * NXP2 * NYP2);
#ifdef EXTVEL
		pdump = (trivel *) malloc (sizeof (trivel) * total_local_size);
		pdump1 = (bivel *) malloc (sizeof (bivel) * NX * NY * NZP2);
#endif
#ifdef CHAOTIC_FLOW_JB
		psi = (cmplx *) malloc (sizeof (cmplx) * 9);
		duxdt = (cmplx *) malloc (sizeof (cmplx) * 9);
		duydt = (cmplx *) malloc (sizeof (cmplx) * 9);
		lam = (double *) malloc (sizeof (double) * 9);
		ksqr = (double *) malloc (sizeof (double) * 9);
		kx = (double *) malloc (sizeof (double) * 9);
		ky = (double *) malloc (sizeof (double) * 9);
#endif


#ifdef CHAOTIC_FLOW_JB
		init_lam_kx_ky (lam, ksqr, kx, ky, psi, duxdt, duydt, famp);
#endif

		/*---- The chunk below was implemented to check whether the restart config. is working properly or not with random numbers*/
		/*
			 short *seed,nseed[3];
			 seed=(short*) malloc(sizeof(short)*3);
			 if(particle_restore==0){
			 nseed[0]=nseed[1]=nseed[2]=2;
			 seed=seed48(nseed);
			 }else{
			 fid=fopen("seed_dump.dat","r");
			 fscanf(fid,"%hd %hd %hd",&nseed[0],&nseed[1],&nseed[2]);
			 fclose(fid);
			 seed=seed48(nseed);
			 }
			 */
		/*----- End chunk ----*/


		/* Initialize the particle positions */

		if (particle_restore == 0) {
			initialize (Npart1, c1, famp, 1);
			initialize (Npart2, c2, famp, 2);
			itime = 0;
		}
		else {
			restore_particle (Npart1, Npart2, c1, c2, famp, &itime, me);
			itime = 0;
			//    if(particle_restore==1)lit=log10(itime);
			ttime = ttime + itime;
		}


		//   while((itime<ttime)&&(Npart1!=0&&Npart2!=0)){
		while (itime < ttime) {
			/* Vary compressibility */
			/* compres(u);  */
#ifdef CHAOTIC_FLOW_JB
			gen_psi (psi, duxdt, duydt, ksqr, lam, kx, ky);
#ifdef DUMP_ENERGY
			if ((itime % nout) == 0)
				snap_psi (psi, itime, me);
#endif
#ifdef COMPRES
			if ((itime % nout) == 0)
				eval_compres (psi, itime, me);
#endif
#endif
			/* Step-I: Brownian walk */
			flow_and_random_walk (Npart1, c1, u, famp, 1, itime);
			flow_and_random_walk (Npart2, c2, u, famp, 2, itime);


			/* Create neighbour list */
			list_neighbour (Npart1, hist1, c1);
			list_neighbour (Npart2, hist2, c2);

#ifdef SIMONE
			/* Set reaction array to zero */
			for (i = 0; i < (NX + 1); i++)
				for (j = 0; j <= (NY + 1); j++) {
					idx = IDX (i, j);
					reac[idx] = 0;
				}
#endif


			/* 	   sprintf(fname,"data/hist.%d.me.%d.isample.%d.dat",itime,me,isample);
						 fck=fopen(fname,"w");
						 for(i=1;i<=NX;i++){
						 for(j=1;j<=NY;j++){
						 idx=IDX(i,j);
						 fprintf(fck,"%d %d %g %g \n",i,j,hist1[idx],hist2[idx]);
						 }
						 fprintf(fck,"\n");
						 }
						 fclose(fck);
						 MPI_Barrier(MPI_COMM_WORLD);	
						 exit(1); 
						 */
			/* Not sure its first death or birth. Similar to a saying in 
Hindi: pehle murgi ya anda (first hen or egg?). Presently 
I choose first death (as per Mogens suggestion!). */

#ifdef REACTION
#ifdef SIMONE
			/* Step-II: Death */
			/* A+A -> A */
			/* A+B -> A */
			if (c1 == NULL)
				printf ("Nothing");
			if (c2 == NULL)
				printf ("Nothing");
			dcount1 = 0;
			dcount2 = 0;

			ipart1 = 0;
			ipart2 = 0;
			while ((ipart1 + ipart2) < Npart1 + Npart2) {
				if (drand48 () <= (double) Npart2 / (double) (Npart1 + Npart2)) {
					ipart = ipart2;
					if (ipart < Npart2) {
						xwrap = c2[ipart].x - floor (c2[ipart].x / lenx) * lenx;
						ywrap = c2[ipart].y - floor (c2[ipart].y / leny) * leny;
						ix = floor (xwrap / dx) + 1;
						iy = floor (ywrap / dy) + 1;
						idx = IDX (ix, iy);
						if (gamma_death * delta * (hist1[idx] + hist2[idx] - 1.0) >
								drand48 ()) {
							c2[ipart].flag = 0;
							hist2[idx]--;
							dcount2++;
							reac[idx] = 1;
						}
						ipart2++;
					}
				}
				else {
					ipart = ipart1;
					if (ipart < Npart1) {
						xwrap = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
						ywrap = c1[ipart].y - floor (c1[ipart].y / leny) * leny;
						ix = floor (xwrap / dx) + 1;
						iy = floor (ywrap / dy) + 1;
						idx = IDX (ix, iy);
						if (gamma_death * delta * (hist1[idx] + hist2[idx] - 1.0) >
								drand48 ()) {
							c1[ipart].flag = 0;
							hist1[idx]--;
							dcount1++;
							reac[idx] = 1;
						}
						ipart1++;
					}			/*ifend */
				}
			}
			/* Remove dead animals of type A */
			qsort (c1, Npart1, sizeof (part), compare_int);
			Npart1 = Npart1 - dcount1;
			c1 = (part *) realloc (c1, Npart1 * sizeof (part));

			/* Remove dead animals of type B */
			qsort (c2, Npart2, sizeof (part), compare_int);
			Npart2 = Npart2 - dcount2;
			c2 = (part *) realloc (c2, Npart2 * sizeof (part));


			/* Step-III: Birth */
			/* Reaction A->A+A */
			count1 = Npart1;
			for (ipart = 0; ipart < Npart1; ipart++) {
				if (c1 == NULL)
					printf ("Nothing");
				if (mu1 * delta > drand48 ()) {
					count1++;
					c1 = (part *) realloc (c1, count1 * sizeof (part));
					c1[count1 - 1].x = c1[ipart].x;
					c1[count1 - 1].y = c1[ipart].y;
					c1[count1 - 1].flag = 1;
					c1[count1 - 1].vx = c1[ipart].vx;
					c1[count1 - 1].vy = c1[ipart].vy;
					c1[count1 - 1].vx0 = c1[ipart].vx0;
					c1[count1 - 1].vy0 = c1[ipart].vy0;
#ifdef INERTIAL
					c1[count1 - 1].vxrhs0 = c1[ipart].vxrhs0;
					c1[count1 - 1].vyrhs0 = c1[ipart].vyrhs0;
#endif
				}
			}
			bcount1 = count1 - Npart1;
			Npart1 = count1;

			/* Reaction B->B+B */
			count2 = Npart2;
			for (ipart = 0; ipart < Npart2; ipart++) {
				if (c2 == NULL)
					printf ("Nothing");
				if (mu2 * delta > drand48 ()) {
					count2++;
					c2 = (part *) realloc (c2, count2 * sizeof (part));
					c2[count2 - 1].x = c2[ipart].x;
					c2[count2 - 1].y = c2[ipart].y;
					c2[count2 - 1].flag = 1;
					c2[count2 - 1].vx = c2[ipart].vx;
					c2[count2 - 1].vy = c2[ipart].vy;
					c2[count2 - 1].vx0 = c2[ipart].vx0;
					c2[count2 - 1].vy0 = c2[ipart].vy0;
#ifdef INERTIAL
					c2[count2 - 1].vxrhs0 = c2[ipart].vxrhs0;
					c2[count2 - 1].vyrhs0 = c2[ipart].vyrhs0;
#endif
				}
			}
			bcount2 = count2 - Npart2;
			Npart2 = count2;
#endif



#ifdef MUTUALISM
			/* Step-II: Death */
			/* A+A -> A */
			/* A+B -> A */
			if (c1 == NULL)
				printf ("Nothing");
			if (c2 == NULL)
				printf ("Nothing");
			dcount1 = 0;
			dcount2 = 0;

			ipart1 = 0;
			ipart2 = 0;
			while ((ipart1 + ipart2) < Npart1 + Npart2) {

				ipart = ipart1;
				if (ipart < Npart1) {
					xwrap = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
					ywrap = c1[ipart].y - floor (c1[ipart].y / leny) * leny;
					ix = floor (xwrap / dx) + 1;
					iy = floor (ywrap / dy) + 1;
					idx = IDX (ix, iy);
					/* hist[idx]-1.0 to remove the self-counting */
					/*
						 if(gamma_death*delta*(hist1[idx]+hist2[idx]-1.0) > 0.1){
						 printf("The time step is too large. \n");
						 exit(1);
						 }
						 */

					/* A+A -> A */
					if ((lam_aa * delta * (hist1[idx] - 1.0) > drand48 ())
							&& (c1[ipart].flag != 0)) {
						c1[ipart].flag = 0;
						hist1[idx]--;
						dcount1++;
					}
					/* A+B -> B */
					if ((lam_ab * delta * hist2[idx] > drand48 ())
							&& (c1[ipart].flag != 0)) {
						c1[ipart].flag = 0;
						hist1[idx]--;
						dcount1++;
					}
					ipart1++;
				}

				ipart = ipart2;
				if (ipart < Npart2) {
					xwrap = c2[ipart].x - floor (c2[ipart].x / lenx) * lenx;
					ywrap = c2[ipart].y - floor (c2[ipart].y / leny) * leny;
					ix = floor (xwrap / dx) + 1;
					iy = floor (ywrap / dy) + 1;
					idx = IDX (ix, iy);
					/* hist[idx]-1.0 to remove the self-counting */
					/*
						 if(gamma_death*delta*(hist1[idx]+hist2[idx]-1.0) > 0.1){
						 printf("The time step is too large. \n");
						 exit(1);
						 }
						 */
					/* B+B -> B */
					if ((lam_bb * delta * (hist2[idx] - 1.0) > drand48 ())
							&& (c2[ipart].flag != 0)) {
						c2[ipart].flag = 0;
						hist2[idx]--;
						dcount2++;
					}
					/* B+A -> A */
					if ((lam_ba * delta * hist1[idx] > drand48 ())
							&& (c2[ipart].flag != 0)) {
						c2[ipart].flag = 0;
						hist2[idx]--;
						dcount2++;
					}
					ipart2++;
				}
			}
			/* Remove dead animals of type A */
			qsort (c1, Npart1, sizeof (part), compare_int);
			Npart1 = Npart1 - dcount1;
			c1 = (part *) realloc (c1, Npart1 * sizeof (part));

			/* Remove dead animals of type B */
			qsort (c2, Npart2, sizeof (part), compare_int);
			Npart2 = Npart2 - dcount2;
			c2 = (part *) realloc (c2, Npart2 * sizeof (part));


			/* Step-III: Birth */
			/* Reaction A->A+A */
			count1 = Npart1;
			for (ipart = 0; ipart < Npart1; ipart++) {
				if (c1 == NULL)
					printf ("Nothing");
				if (mu1 * delta > drand48 ()) {
					count1++;
					c1 = (part *) realloc (c1, count1 * sizeof (part));
					c1[count1 - 1].x = c1[ipart].x;
					c1[count1 - 1].y = c1[ipart].y;
					c1[count1 - 1].flag = 1;
					c1[count1 - 1].vx = c1[ipart].vx;
					c1[count1 - 1].vy = c1[ipart].vy;
					c1[count1 - 1].vx0 = c1[ipart].vx0;
					c1[count1 - 1].vy0 = c1[ipart].vy0;
#ifdef INERTIAL
					c1[count1 - 1].vxrhs0 = c1[ipart].vxrhs0;
					c1[count1 - 1].vyrhs0 = c1[ipart].vyrhs0;
#endif
				}
			}
			bcount1 = count1 - Npart1;
			Npart1 = count1;

			/* Reaction B->B+B */
			count2 = Npart2;
			for (ipart = 0; ipart < Npart2; ipart++) {
				if (c2 == NULL)
					printf ("Nothing");
				if (mu2 * delta > drand48 ()) {
					count2++;
					c2 = (part *) realloc (c2, count2 * sizeof (part));
					c2[count2 - 1].x = c2[ipart].x;
					c2[count2 - 1].y = c2[ipart].y;
					c2[count2 - 1].flag = 1;
					c2[count2 - 1].vx = c2[ipart].vx;
					c2[count2 - 1].vy = c2[ipart].vy;
					c2[count2 - 1].vx0 = c2[ipart].vx0;
					c2[count2 - 1].vy0 = c2[ipart].vy0;
#ifdef INERTIAL
					c2[count2 - 1].vxrhs0 = c2[ipart].vxrhs0;
					c2[count2 - 1].vyrhs0 = c2[ipart].vyrhs0;
#endif
				}
			}
			bcount2 = count2 - Npart2;
			Npart2 = count2;
#endif



#ifdef MORAN
			/* Step-II: Death */
			/* A+B -> A+A */
			/* A+B -> B+B */
			if (c1 == NULL)
				printf ("Nothing");
			if (c2 == NULL)
				printf ("Nothing");
			dcount1 = 0;
			bcount1 = Npart1;
			dcount2 = 0;
			bcount2 = Npart2;

			ipart1 = 0;
			ipart2 = 0;
			while ((ipart1 + ipart2) < Npart1 + Npart2) {

				if (drand48 () <= (double) Npart1 / (double) (Npart1 + Npart2)) {
					/*A+B -> B+B */
					ipart = ipart1;
					if (ipart < Npart1) {
						xwrap = c1[ipart].x - floor (c1[ipart].x / lenx) * lenx;
						ywrap = c1[ipart].y - floor (c1[ipart].y / leny) * leny;
						ix = floor (xwrap / dx) + 1;
						iy = floor (ywrap / dy) + 1;
						idx = IDX (ix, iy);
						/* hist[idx]-1.0 to remove the self-counting */
						if (mu1 * delta * hist2[idx] / (dx * dy) > 1) {
							printf ("The time step is too large. \n");
							sprintf (fname, "data/hist1.%d.me.%d.dat", itime, me);
							fck = fopen (fname, "w");
							for (i = 1; i <= NX; i++) {
								for (j = 1; j <= NY; j++) {
									idx = IDX (i, j);
									fprintf (fck, "%d %d %g \n", i, j, hist2[idx]);
								}
								fprintf (fck, "\n");
							}
							fclose (fck);

							exit (1);
						}
						if (mu1 * delta * hist2[idx] / (dx * dy) > drand48 ()) {
							c1[ipart].flag = 0;
							dcount1++;
							bcount2++;
							c2 = (part *) realloc (c2, (bcount2) * sizeof (part));
							c2[bcount2 - 1].x = c1[ipart].x;
							c2[bcount2 - 1].y = c1[ipart].y;
							c2[bcount2 - 1].flag = 1;
							c2[bcount2 - 1].vx = c1[ipart].vx;
							c2[bcount2 - 1].vy = c1[ipart].vy;
							c2[bcount2 - 1].vx0 = c1[ipart].vx0;
							c2[bcount2 - 1].vy0 = c1[ipart].vy0;
#ifdef INERTIAL
							c2[bcount2 - 1].vxrhs0 = c1[ipart].vxrhs0;
							c2[bcount2 - 1].vyrhs0 = c1[ipart].vyrhs0;
#endif
						}
						ipart1++;
					}
				}
				else {
					/* B+A -> A+A */
					ipart = ipart2;
					if (ipart < Npart2) {
						xwrap = c2[ipart].x - floor (c2[ipart].x / lenx) * lenx;
						ywrap = c2[ipart].y - floor (c2[ipart].y / leny) * leny;
						ix = floor (xwrap / dx) + 1;
						iy = floor (ywrap / dy) + 1;
						idx = IDX (ix, iy);
						/* hist[idx]-1.0 to remove the self-counting */
						if (mu2 * delta * hist1[idx] / (dx * dy) > 1) {
							sprintf (fname, "data/hist2.%d.me.%d.dat", itime, me);
							fck = fopen (fname, "w");
							for (i = 1; i <= NX; i++) {
								for (j = 1; j <= NY; j++) {
									idx = IDX (i, j);
									fprintf (fck, "%d %d %g \n", i, j, hist1[idx]);
								}
								fprintf (fck, "\n");
							}
							fclose (fck);
							printf ("The time step is too large. \n");
							exit (1);
						}
						if (mu2 * delta * hist1[idx] / (dx * dy) > drand48 ()) {
							c2[ipart].flag = 0;
							dcount2++;
							bcount1++;
							c1 = (part *) realloc (c1, (bcount1) * sizeof (part));
							c1[bcount1 - 1].x = c2[ipart].x;
							c1[bcount1 - 1].y = c2[ipart].y;
							c1[bcount1 - 1].flag = 1;
							c1[bcount1 - 1].vx = c2[ipart].vx;
							c1[bcount1 - 1].vy = c2[ipart].vy;
							c1[bcount1 - 1].vx0 = c2[ipart].vx0;
							c1[bcount1 - 1].vy0 = c2[ipart].vy0;
#ifdef INERTIAL
							c1[bcount1 - 1].vxrhs0 = c2[ipart].vxrhs0;
							c1[bcount1 - 1].vyrhs0 = c2[ipart].vyrhs0;
#endif
						}
						ipart2++;
					}
				}
			}
			/* Remove dead animals of type A */
			qsort (c1, bcount1, sizeof (part), compare_int);
			Npart1 = bcount1 - dcount1;
			c1 = (part *) realloc (c1, Npart1 * sizeof (part));

			/* Remove dead animals of type B */
			qsort (c2, bcount2, sizeof (part), compare_int);
			Npart2 = bcount2 - dcount2;
			c2 = (part *) realloc (c2, Npart2 * sizeof (part));
#endif
#endif

#ifdef LOG_DUMP
			ilit = floor (pow (10, lit) / delta);
			if ((itime - ilit) == 0)
				lit += ljump;
#endif


#ifndef LOG_DUMP
			if (itime % nout == 0) {
				fprintf (fid, "%g %d %d  \n", itime * delta, Npart1, Npart2);
				fflush (fid);
			}
			/*
				 if((Npart1==0)||(Npart2==0)){
				 fprintf(fid,"%g %d %d  \n",itime*delta,Npart1,Npart2);
				 fflush(fid);
				 }
				 */
#ifdef DUMP_PARTICLE
			if ((me == 0) && (isample == 0)) {
				if ((itime % nout) == 0)
					dump_position (Npart1, c1, hist1, itime, 1);
				if ((itime % nout) == 0)
					dump_position (Npart2, c2, hist2, itime, 2);
			}
#endif
#else
			if ((itime - ilit) == 0) {
				fprintf (fid, "%g %d %d  \n", itime * delta, Npart1, Npart2);
				fflush (fid);
			}
			if ((Npart1 == 0) || (Npart2 == 0)) {
				fprintf (fid, "%g %d %d  \n", itime * delta, Npart1, Npart2);
				fflush (fid);
			}

#ifdef DUMP_PARTICLE
			if ((me == 0) && (isample == 0)) {
				if ((itime % (ttime / nout)) == 0)
					dump_position (Npart1, c1, hist1, itime, 1);
				if ((itime % (ttime / nout)) == 0)
					dump_position (Npart2, c2, hist2, itime, 2);
			}
#endif
#endif



#ifdef HETERO
#ifndef LOG_DUMP
			if ((itime % nout) == 0)
				heterozygosity (c1, c2, itime, me);
#else
			if ((itime - ilit) == 0) {
				heterozygosity (c1, c2, itime, me);
			}
#endif
#endif
			itime++;
		}
		fclose (fid);
		isample++;
	}
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif
	restore_dump (c1, c2, Npart1, Npart2, itime, me);
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif
	/*
		 fid=fopen("particle_dump1.bin","w");
		 fwrite(c1,sizeof(part),Npart1,fid);
		 fclose(fid);

		 fid=fopen("particle_dump2.bin","w");
		 fwrite(c2,sizeof(part),Npart2,fid);
		 fclose(fid);

		 fid=fopen("psi_dump.bin","w");
		 fwrite(psi,sizeof(cmplx),9,fid);
		 fclose(fid);

		 fid=fopen("time_index.dat","w");
		 fprintf(fid,"%d\n",itime);
		 fclose(fid);
		 */

	/*
		 This part is to test particle restore
		 fid=fopen("seed_dump.dat","w");
		 seed=seed48(seed);
		 fprintf(fid,"%hd %hd %hd",seed[0],seed[1],seed[2]);
		 fclose(fid);
		 */

	free (c1);
	free (c2);
	free (hist1);
	free (hist2);
	free (u);

#ifdef EXTVEL
	free (pdump);
	free (pdump1);
#endif

#ifdef CHAOTIC_FLOW_JB
	free (psi);
	free (duxdt);
	free (duydt);
	free (lam);
	free (ksqr);
	free (kx);
	free (ky);
#endif
#ifdef EXTVEL
	free (uf);
	free (cuf);
#endif

	/* MPI_Finalize(); */
	exit (0);
	}
