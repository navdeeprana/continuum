#include <gsl/gsl_randist.h>
#include "parameters.h"

	void
file_grid_double (FILE * fid, double *arr1, double *arr2)
{
	int i, j, idx;
	for (i = 0; i < ngy; i++) {
		for (j = 0; j < ngx; j++) {
			idx = index (i, j);
			fprintf (fid, "%d\t%d\t%g\n", i, j, arr1[idx]);
		}
	}
	fprintf (fid, "\n\n");
	fflush (fid);
}

	void
ave_ov_onedir (FILE * fid, double *rho)
{
	int i, j, idx;
	double *ave = calloc (ngx, sizeof (double));
	for (i = 0; i < ngx; i++) {
		for (j = 0; j < ngy; j++) {
			idx = index (j, i);
			ave[i] += rho[idx];
		}
		ave[i] /= ngy;
		fprintf (fid, "%d\t%g\n", i, ave[i]);
	}
	fprintf (fid, "\n\n");
	fflush (fid);
	free (ave);
}

	void
set_initial_rho (double dx, double *rho, double *food, double factor)
{
	int i, j, idx;
	for (i = 0; i < ngy; i++) {
		for (j = 0; j < ngx; j++) {
			idx = index (i, j);
			rho[idx] = 0.5e0 * (1.e0 - tanh (32.e0 * dx * (j - ngx / 5)));
			food[idx] = factor * infoo + foodshift;
		}
	}
}

	void
calc_grad (double dx, double dy, double *rho, double *gradx, double *grady)
{
	int i, j, idx;
	for (i = 1; i < ngy - 1; i++) {
		for (j = 1; j < ngx - 1; j++) {
			idx = index (i, j);
			gradx[idx] = (rho[idx + 1] - rho[idx - 1]) / (2.e0 * dx);
			grady[idx] = (rho[idx + ngx] - rho[idx - ngx]) / (2.e0 * dy);
		}
	}
}

	void
calc_div (double dx, double dy, double *rhox, double *rhoy, double *div)
{
	int i, j, idx;
	for (i = 1; i < ngy - 1; i++) {
		for (j = 1; j < ngx - 1; j++) {
			idx = index (i, j);
			div[idx] =
				(rhox[idx + 1] - rhox[idx - 1]) / (2.e0 * dx) + (rhoy[idx + ngx] -
						rhoy[idx -
						ngx]) /
				(2.e0 * dy);
		}
	}
}

	void
calc_diff (double constx, double consty, double *rho, double *diff)
{
	int i, j, idx;
	for (i = 1; i < ngy - 1; i++) {
		for (j = 1; j < ngx - 1; j++) {
			idx = index (i, j);
			diff[idx] =
				constx * (rho[idx + 1] + rho[idx - 1] - 2.e0 * rho[idx]) +
				consty * (rho[idx + ngx] + rho[idx - ngx] - 2.e0 * rho[idx]);
		}
	}
}

	double
integrate_the_stochastic_part (const gsl_rng * r1, const gsl_rng * r2,
		double rho, double lambda)
{
	double poisson_mean = lambda * rho;
	unsigned int n = gsl_ran_poisson (r1, poisson_mean);
	double randgamma = gsl_ran_gamma (r2, (double) n, 1);
	return randgamma / lambda;
}

	double
logistic (double rho,int fac)
{
	return fac*gamma * rho * (1.e0 - rho);
}

	void
calc_monod (double *food, double *monod)
{
	int i;
	for (i = 0; i < ng; i++)
		monod[i] = food[i] / (1.e0 + food[i]);
}

	void
set_boundary (double *array, int flag1, int flag2)
{
	int i;
	if (flag2) {
		for (i = 1; i < ngx - 1; i++) {
			array[i] = 0.e0;
			array[ng - 1 - i] = 0.e0;
		}
		for (i = 1; i < ngy - 1; i++) {
			array[i * ngx] = 0.e0;
			array[(i + 1) * ngx - 1] = 0.e0;
		}
	}
	else {
		for (i = 1; i < ngx - 1; i++) {
			array[i] = array[i + ngx];
			array[ng - 1 - i] = array[ng - 1 - ngx - i];
		}
		for (i = 1; i < ngy - 1; i++) {
			if (flag1)
				array[i * ngx] = array[i * ngx + 1];
			array[(i + 1) * ngx - 1] = array[(i + 1) * ngx - 2];
		}
	}
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i, j, idx;
	for (i = 1; i < ngy - 1; i++) {
		for (j = 1; j < ngx - 1; j++) {
			idx = index (i, j);
			rho[idx] += dt * drho[idx];
		}
	}
}

	int
optimiser(double *bac,int oldid)
{
	/*------THE ZEN PIECE OF THIS ENTIRE SIMULATION--------*/
	/* The idea is that as system grows from left to right,
	 * there is no point in doing the integration for the
	 * entire space where there's nothing. This piece just finds
	 * out the point after which there is nothing.
	 */
	int ix,iy;
	int checkid;
	checkid=oldid;
	int found;
	for(ix=oldid;ix<ngx;ix++){
		found=0;
		for(iy=0;iy<ngy;iy++){
			if(bac[ix+iy*ngx] !=0.e0){
				found=1;
				break;
			}
		}
		if(!found){
			checkid=ix;
			break;
		}
	}
	return checkid;
}
	void
makedir(int id)
{
	struct stat st = {0};
	if (stat("data", &st) == -1)
		mkdir("data", 0700);

	char name[32];
	sprintf(name,"data/%d",id);
	if (stat(name, &st) == -1)
		mkdir(name, 0700);
}
	double
car_cap(double *rho)
{
	int i;
	double sum=zero;
	for(i=0;i<ng;i++)
		sum += rho[i];

	return sum/ng;
}
	int
main (int argc, char **argv)
{
#ifdef MPI
	int NP, me;
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);
#else
	int me==0;
#endif
	srand(time(NULL)+me);

	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	gsl_rng_set (r1, rand ());
	gsl_rng_set (r2, rand ());
	
	int i, j, idx;
	double dx = lenx / ngx;
	double dy = leny / ngy;
	double constbacx = Dbac / (dx * dx);
	double constbacy = Dbac / (dy * dy);
	double constfoox = Dfoo / (dx * dx);
	double constfooy = Dfoo / (dy * dy);
	double dt;
	if (dx <= dy && Dbac >= Dfoo)
		dt = dtfac / constbacx;
	else if (dx <= dy && Dbac < Dfoo)
		dt = dtfac / constfoox;
	else if (dy <= dx && Dbac >= Dfoo)
		dt = dtfac / constbacy;
	else if (dy <= dx && Dbac < Dfoo)
		dt = dtfac / constfooy;

	int freq = (int) ((1.0 / (intvl * dt)));
	if (me == 0)
		printf ("%g\t%d\n", dt, freq);

	double *rho = calloc (ng, sizeof (double));
	double *drho = calloc (ng, sizeof (double));
	double *food = calloc (ng, sizeof (double));
	double *monod = calloc (ng, sizeof (double));
	double *dfood = calloc (ng, sizeof (double));
	double *temp1 = calloc (ng, sizeof (double));
	double *temp2 = calloc (ng, sizeof (double));

	char temp[32],snap[32], average[32];
	sprintf (average, "f_ave");
#ifdef MPI
	makedir(me);
	sprintf (snap,"data/%d/%s",me,temp);
	sprintf (average,"data/%d/average",me);
#else
	sprintf (snap,"data/%s",temp);
	sprintf (average,"data/average");
#endif
//	FILE *fidave = fopen(average,"w");
	set_initial_rho (dx, rho,food,1);
	int itime = 0;
	double lambda = 2.0e0 / (sigma * sigma * dt);
	double sigma_eff, lambda_eff;
	int fc=0;
	char file[32];
	FILE *fid;
	int checkid=ngx,flag=1;
	while (itime * dt <= simtime) {
		if (itime % freq == 0){
			if(me==0) printf ("\r%d", itime / freq);
			fflush (stdout);
			sprintf ( file,"%s.%d",snap,fc);
			fid = fopen(file,"w");
			file_grid_double(fid, rho,food);
			fclose(fid);
			fc++;
			if(car_cap(rho) > 0.98){
				printf("Max Carr Capacity hit.\t");
				break;
			}
			//average_array(fidave,itime,rho);
		}
		calc_monod (food, monod);
		calc_diff (constfoox, constfooy, food, dfood);
		for (i = 1; i < ngy - 1; i++) {
			for (j = 1; j < ngx - 1; j++) {
				idx = index (i, j);
				dfood[idx] = -alpha * rho[idx] * monod[idx] + dfood[idx];
			}
		}
#ifdef OPTIMISE
		if(flag){
		  	checkid=0;
			flag=0;
		}
		checkid=optimiser(rho,checkid); // Used only for gsl-stuff. Ca    n be done for everything else, but rng is the bottle-neck.
#endif
		for (i = 1; i < ngy - 1; i++) {
			for (j = 1; j < checkid; j++) {
				idx = index (i, j);
				sigma_eff = sigma * monod[idx];
				lambda_eff = lambda / (monod[idx] * monod[idx]);
				if (sigma_eff > 5.e-3) {
					if (rho[idx] < 0.5e0)
						rho[idx] =
							integrate_the_stochastic_part (r1, r2, rho[idx], lambda_eff);
					else
						rho[idx] =
							1.e0 - integrate_the_stochastic_part (r1, r2,
									1.e0 - rho[idx],
									lambda_eff);
				}
			}
		}
		calc_diff (constfoox, constfooy, rho,drho);
		for (i = 1; i < ngy - 1; i++) {
			for (j = 1; j < ngx - 1; j++) {
				idx = index (i, j);
				drho[idx] += logistic (rho[idx],me+1) * monod[idx];
			}
		}
		euler_update (dt, rho, drho);
		set_boundary (rho, 0, 0);
		euler_update (dt, food, dfood);
		set_boundary (food, 1, 0);
		itime++;
	}
	free (rho);
	free (drho);
	free (food);
	free (dfood);
	free (temp1);
	free (temp2);
	free (monod);
	fcloseall();
	printf("EOS. Clean exit on rank %d\n.",me);
#ifdef MPI
	MPI_Finalize ();
#endif
	return 0;
}
