#include <gsl/gsl_randist.h>
#include "mpi.h"
#include "parameters.h"
	
	void
file_double (FILE * fid, double factor, double *arr1, double *arr2)
{
	int i;

	for (i = 0; i < ng; i++)
		fprintf (fid, "%g\t%f\t%f\n", factor * i, arr1[i], arr2[i]);

	fprintf (fid, "\n\n");
	fflush (fid);
}

	void
set_ini_cond (double *rho, double *food,double scalar)
{
	int i;
	double dgrid = len / ng;
	for (i = 0; i < ng; i++) {
		rho[i] = 0.5e0 * (1.e0 - tanh (32.e0 * dgrid * (i - ng / 5)));
		food[i] = infoo*scalar + foodshift;
	}
}

	void
calc_grad (double dgrid, double *rho, double *grad)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		grad[i] = (rho[i + 1] - rho[i - 1]) / (2.e0 * dgrid);
}

	void
calc_diff (double constant, double *rho, double *diff)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		diff[i] = constant * (rho[i + 1] + rho[i - 1] - 2.e0 * rho[i]);
}

	double
integrate_the_stochastic_part (const gsl_rng * r1, const gsl_rng * r2,
		double rho, double lambda)
{
	double poisson_mean = lambda * rho;
	unsigned int n = gsl_ran_poisson (r1, poisson_mean);
	double randgamma = gsl_ran_gamma (r2, (double) n, 1);
	return randgamma / lambda;

}


	double
function (double rho)
{
	return gamma * rho * (1.e0 - rho);
}

	void
calc_monod (double *food, double *monod)
{
	int i;
	for (i = 0; i < ng; i++)
		monod[i] = food[i] / (0.5e0*infoo + food[i]);
	//	monod[i] = food[i];
}

	void
set_bound (double *right, double *pright)
{
	*right = *pright;
}

	void
euler_update (double dt, double *rho, double *drho)
{
	int i;
	for (i = 1; i < ng - 1; i++)
		rho[i] += dt * drho[i];
}

	double
car_cap (double dx, double *rho)
{
	int i;
	double sum = zero;
	for (i = 0; i < ng; i++)
		sum += rho[i];

	return dx * sum;
}

	int
main (int argc, char **argv)
{
	

	int NP, me; 
	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &NP);
	MPI_Comm_rank (MPI_COMM_WORLD, &me);
	
	const gsl_rng_type *type;
	gsl_rng *r1, *r2;
	gsl_rng_env_setup ();
	type = gsl_rng_default;
	r1 = gsl_rng_alloc (type);
	r2 = gsl_rng_alloc (type);
	srand (time (NULL) + rand ()+me);
	gsl_rng_set (r1, rand ());
	srand (time (NULL) + rand ()+me);
	gsl_rng_set (r2, rand ());

	int i;
	double dx = len / ng;
	double constbac = Dbac / (dx * dx);
	double constfoo = Dfoo / (dx * dx);

	double dt;
	if (constfoo >= constbac)
		dt = dtfac / constfoo;
	else
		dt = dtfac / constbac;

	int freq = (int) ((1.0 / (intvl * dt)));
	if (me==0) printf ("%g\t%d\n", dt, freq);

	double *rho = calloc (ng, sizeof (double));
	double *drho = calloc (ng, sizeof (double));
	double *food = calloc (ng, sizeof (double));
	double *dfood = calloc (ng, sizeof (double));
	double *monod = calloc (ng, sizeof (double));
	double *temp = calloc (ng, sizeof (double));

	char filename[64];
   sprintf (filename, "data/f_snap.%d", me);

	FILE *fid1 = fopen (filename, "w");
	//FILE *fid2 = fopen ("f_speed", "w");

	set_ini_cond (rho, food,me);
	fprintf(fid1,"## initial food : %g\n\n",foodshift+infoo*me);
	int itime = 0;
	double lambda = 2.e0 / (sigma * sigma * dt);
	double sigma_eff, lambda_eff;

	while (itime * dt <= simtime) {
		if (itime % (freq) == 0) {
			if (me==0){printf ("\r%d", itime / freq);
			fflush (stdout);}
			//fprintf (fid2, "%g\t%g\n", itime * dt, car_cap (dx, rho));
			file_double (fid1, dx, rho, food);
		}
		calc_monod (food, monod);

		/* RHS  FOR FOOD */
		calc_diff (constfoo, food, dfood);
		for (i = 0; i < ng; i++)
			dfood[i] = -alpha*rho[i] * monod[i] + dfood[i];

		/* RHS FOR BACTERIA */
		for (i = 1; i < ng - 1; i++) {
			sigma_eff = sigma * monod[i];
			lambda_eff = lambda / (monod[i]*monod[i]);
			if (sigma_eff > 5.e-3) {
				if (rho[i] < 0.5e0)
					rho[i] = integrate_the_stochastic_part (r1, r2, rho[i], lambda_eff);
				else
					rho[i] =
						1.e0 - integrate_the_stochastic_part (r1, r2, 1.e0 - rho[i],
								lambda_eff);
			}
		}
		calc_grad (dx, rho, drho);
		drho[0] = 0.e0;
		drho[ng - 1] = 0.e0;

		for (i = 0; i < ng; i++)
			temp[i] = Dbac * monod[i] * drho[i];

		calc_grad (dx, temp, drho);
		for (i = 0; i < ng; i++)
			drho[i] = function (rho[i]) * monod[i] + drho[i];

		euler_update (dt, rho, drho);
		euler_update (dt, food, dfood);
		/*      
				  set_bound (&rho[ng - 1], &rho[ng - 3]);
				  set_bound (&food[ng - 1], &food[ng - 3]);
				  set_bound (&food[0], &food[2]);
				  */
		set_bound (&rho[ng - 1], &rho[ng - 2]);
		set_bound (&food[ng - 1], &food[ng - 2]);
		set_bound (&food[0], &food[1]);
		itime++;
	}
	free (rho);
	free (drho);
	free (food);
	free (dfood);
	free (monod);
	fcloseall ();
	MPI_Finalize();
}
