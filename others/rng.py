import sys
import numpy as np
from scipy import stats
from math import *

NRAND=2048
mean=float(sys.argv[1])
print(mean)
rand=np.zeros((NRAND,),dtype=np.int)
#rand=[NRAND]
for i in range (0,NRAND):
    if(i%256==0):
        print(i)
    rand[i] = stats.poisson.rvs(mean)


