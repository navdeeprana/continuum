set term png font ariel 24 size 2000,2000
set xrange [0:64]
set yrange [0:256]
XTICS = "set xtics('0' 0,'4' 32,'8' 64)"
YTICS = "set ytics('0' 0,'8' 64,'16' 128)"

NOXTICS = "set xtics('' 0,'' 32,'' 64);unset xlabel"
NOYTICS = "set ytics('' 0,'' 64,'' 128);unset ylabel"

set palette defined (0 "black",0.5 "white", 1 "grey")
#set palette defined ( 0 '#fff000',0.5 '#00e4ff',1 '#0000ff')
folder='data/'
do for [i=0:500:1]{
		unset colorbox
		set output "img/img.".i.".png"
		print "Creating ".i
		set multiplot layout 2,4 rowsfirst
		
		set title 'rate = 1'
		@NOXTICS; @YTICS
		p folder.'0/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t '', folder.'0/height' index i u 1:2 w l lt 7 lw 2 t ''
 
		set title 'rate = 2'
		@NOXTICS; @NOYTICS
		p folder.'1/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t '', folder.'1/height' index i u 1:2 w l lt 7 lw 2 t ''

		set title 'rate = 3'
		@NOXTICS; @NOYTICS
		p folder.'2/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t '', folder.'2/height' index i u 1:2 w l lt 7 lw 2 t ''

		set title 'rate = 4'
		@NOXTICS; @NOYTICS
		p folder.'3/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t '', folder.'3/height' index i u 1:2 w l lt 7 lw 2 t ''

		set title 'rate = 5'
		@XTICS; @YTICS
		p folder.'4/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t '', folder.'4/height' index i u 1:2 w l lt 7 lw 2 t ''

		set title 'rate = 6'
		@XTICS; @NOYTICS
		p folder.'5/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t '', folder.'5/height' index i u 1:2 w l lt 7 lw 2 t ''

		set title 'rate = 7'
		@XTICS; @NOYTICS
		p folder.'6/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t '', folder.'6/height' index i u 1:2 w l lt 7 lw 2 t ''
		
		set title 'rate = 7'
		@XTICS; @NOYTICS
		p folder.'6/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t '', folder.'6/height' index i u 1:2 w l lt 7 lw 2 t ''

		unset multiplot
		unset output
}
