set term X11 noraise size 1600,800
set palette defined (0 "black", 1 "yellow")
do for [i=0:400:1]{
		set multiplot layout 2,1
		p 'comp/f1' index i u 1:2:3 w p pt 5 palette ps 1 t ''
		p 'comp/f2' index i u 1:2:3 w p pt 5 palette ps 2 t ''
		pause 0.3
	}
pause -1
