set term png font ariel 24 size 1920,640
set xrange [0:32]
set yrange [0:4]
set palette defined ( 0 "black",1 "yellow")	
do for [i=0:500:5]{
	set output "img/img".i.".png"
	file="data/snap.".i
	print "Creating ".i."from ".file
	p file u 1:2:3 w p pt 5 palette ps 2 t ''
}
