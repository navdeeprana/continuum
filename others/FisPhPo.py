import numpy as np
import matplotlib
matplotlib.use('GTKAgg')

import matplotlib.pyplot as plt
from matplotlib.pyplot import cm

plt.style.use('classic')
nx,ny = 256,256
lx,ly = 1.2,4.0
dx,dy = lx/nx,ly/ny

gamma,alpha = 1.0,1.0

x = np.linspace(0,lx,nx)
y = np.linspace(0,ly,ny)

X,Y = np.meshgrid(x,y)

#U,V = gamma*X*Y/(1+Y),-alpha*X*Y/(1+Y)
#U,V = gamma*X*Y,-alpha*X*Y
#U,V = gamma*X*(1.0-X)*Y/(1+Y),-alpha*X*Y/(1+Y)
U,V = gamma*X*(1.0-X)*Y,-alpha*X*Y

plt.streamplot(X, Y, U, V,density=[1,1],arrowstyle = '->',arrowsize=1.5,linewidth=1,color='DarkRed')

plt.xlim([0, lx])
plt.ylim([0, ly])
plt.title('Phase Portrait',size=24)
plt.xlabel('Bacteria',size=20)
plt.ylabel('Nutrient',size=20)
plt.text(0.45*lx, 0.9*ly,r'$\gamma=1,\ \alpha=1$', style='normal',fontsize=15,fontweight='bold',color='White',
                bbox={'facecolor':'black'})
#plt.gcf().subplots_adjust(left=0.50,bottom=0.50)
#plt.tight_layout()
#plt.show(bbox_inches='tight')
plt.show()
