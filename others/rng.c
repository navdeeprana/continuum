/* Generates 2048 random numbers from a poisson distribution*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_histogram.h>

#define NRAND 131072
#define NBIN 1000
#define STEP 1
	unsigned int
poisson_rng (const gsl_rng * r, double mean)
{
  double prod = 1.0;
  unsigned int k = 0;
  double meanleft = mean;
  double e = exp (1);
  double eSTEP = exp (STEP);
  if (mean > 10) {
    do {
      prod *= gsl_rng_uniform (r);
      if (prod < e && meanleft > 0) {
	prod *= eSTEP;
	meanleft -= STEP;
      }
      else {
	prod *= exp (meanleft);
	meanleft -= 1;
      }
      k++;
    }
    while (prod > 1);
    return k - 1;
  }
  /* small mu */

  double emu = exp (-mean);
  do {
    prod *= gsl_rng_uniform (r);
    k++;
  }
  while (prod > emu);
  return k - 1;
}

long unsigned int
poisson_nrc (const gsl_rng * r, double xm)
{
  static double sq, alxm, g;
  double em, t, y;

  if (xm < 12.e0) {
    g = exp (-xm);
    em = -1.e0;
    t = 1.e0;
    do {
      ++em;
      t *= gsl_rng_uniform (r);
    }
    while (t > g);
  }
  else {
    sq = sqrt (2.e0 * xm);
    alxm = log (xm);
    g = xm * alxm - lgamma (xm + 1.e0);
    do {
      do {
	y = tan (M_PI * gsl_rng_uniform (r));
	em = sq * y + xm;
      }
      while (em < 0.e0);
      em = floor (em);
      t = 0.9e0 * (1.e0 + y * y) * exp (em * alxm - lgamma (em + 1.e0) - g);
    }
    while (gsl_rng_uniform (r) > t);
  }
  return (long unsigned int) em;
}
void
histogram (FILE *fid,double *array)
{
  int i;
  double max = array[0];
  for (i = 0; i < NRAND; i++) {
    if (array[i] > max) {
      max = array[i];
    }
  }
  gsl_histogram *h = gsl_histogram_alloc (NBIN);
  gsl_histogram_set_ranges_uniform (h, 0, max);
  for (i = 0; i < NRAND; i++) {
//  gsl_histogram_increment (h, array[i]);
    gsl_histogram_accumulate (h, array[i], 1.e0/NRAND);
  }
  gsl_histogram_fprintf (fid, h, "%g", "%g");
  gsl_histogram_free (h);
}

int
main (int argc, char **argv)
{
  printf ("Distribution Mean = %g\n", LAMBDA);
  const gsl_rng_type *type;
  gsl_rng *r;
  gsl_rng_env_setup ();
  type = gsl_rng_mt19937;
  r = gsl_rng_alloc (type);
  gsl_rng_set (r, rand ());

  //long unsigned int *rngpois = calloc (NRAND, sizeof (long unsigned int));
  double *rngpois = calloc (NRAND, sizeof (double));
  double *rngnorm = calloc (NRAND,sizeof(double));
  int i;
  const double mean = LAMBDA;
  const double sigma = sqrt(LAMBDA);
  FILE *fid1 = fopen ("hist_poisson", "w");
  FILE *fid2 = fopen ("hist_normal", "w");
  for (i = 0; i < NRAND; i++) {
    rngpois[i] = poisson_nrc (r, LAMBDA);
	 rngnorm[i] = mean + gsl_ran_gaussian(r,sigma);
  }
  histogram (fid1,rngpois);
  histogram (fid2,rngnorm);
  return 0;
}
