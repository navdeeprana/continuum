#set term postscript eps size 5,10 color blacktext "Helvetica" 40
set term postscript eps size 5,5 color "Helvetica" 40
#set term postscript eps size 720,720 color blacktext "Helvetica" 40
set xrange [0:160]
set yrange [0:160]
set notics
#set palette defined (0 "black", 1 "#339900")
set palette defined (0 "black", 1 "#CC9900")
#folder='highresdump/'
folder='data/5'
do for [i=0:2000:3]{
		print i
		set output "img/".i.".eps"
		unset colorbox
		set label 1 "a" at 150, 150 centre tc rgb "white" font ",80" front
		p folder.'/snap.'.i u 1:2:3 w p pt 5 palette ps 1 
#		show label	
		unset output
}
#do for [i=0:1000:1]{
#	unset colorbox
#	set multiplot layout 1,2 rowsfirst
#	set title "DATA"
#	p 'data/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t ''
#	set title "DUMP"
#	p 'dump/snap.'.i u 1:2:3 w p pt 5 palette ps 1 t ''
#  }
