set term png font ariel 24 size 1000,1000
set xrange [0:160]
set yrange [0:160]
set notics
#set palette defined (0 "black", 1 "#339900")
set palette defined (0 "black", 1 "#CC9900")
#folder='highresdump/'
folder='data/5'
do for [i=0:2000:5]{
		print i
		set output "img/".i.".png"
		unset colorbox
		set label 1 "a" at 150, 150 centre tc rgb "white" font "ariel,80" front
		p folder.'/snap.'.i u 1:2:3 w p pt 5 palette ps 1 
#		show label	
		unset output
}
