import numpy as np
from scipy import stats
from math import *

ngrid=2048
simtime=500
dt=0.1e0
dx=1.0e0
D=0.25e0
a=1.75623e0
#a=2.8
b=1.0e0
sigma=2.0e0
inirho=1.0e0

rho=np.zeros((ngrid,),dtype=np.float16)
alpha=np.zeros((ngrid,),dtype=np.float16)

fid = open("pyout","w",0)

# set initial rho
for i in range (0,ngrid):
    rho[i] = inirho

constant = D/(dx*dx)
time = 0.0e0
beta = a - 2.0e0*constant
expbetat = exp(beta*dt)
lam = 2.0e0*beta/(sigma*(expbetat-1.0e0))

for i in range (0,ntime):
# average rho and write it
    if (i%100)==0:
        print i

    rhosum = 0.0e0
    for j in range(0,ngrid):
        rhosum = rhosum + rho[j]

    rhosum = rhosum/ngrid;
    fid.write('{:5.2f}'.format(time))
    fid.write('   ')
    fid.write('{:1.5f}'.format(rhosum))
    fid.write('\n')
# calculate alpha

    alpha[0] = constant * (rho[1] + rho[ngrid - 1])
	
    for j in range(1,ngrid-1):
        alpha[j] = constant * (rho[j-1] + rho[j+1])
	
    alpha[ngrid-1] = constant * (rho[ngrid-2] + rho[0])
    time = (i+1)*dt;
    for j in range (0,ngrid):
        mu = -1.0e0 + 2.0e0 *(alpha[j]/sigma)
        poisson_mean = lam * rho[j] * expbetat
        n = stats.poisson.rvs(poisson_mean)
        if n+mu+1 < 1.0e-8:
            gamma = 0.0e0
        else:
            gamma = stats.gamma.rvs(mu+n+1)

        gamma = gamma/lam
        rho[j] = gamma/(1+b*dt*gamma)


