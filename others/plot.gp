set term X11 persist noraise size 800,800
p 'hist_poisson' u (log(($1+$2)/2)):(log($3)) w l lt -1,\
  'hist_normal' u (log(($1+$2)/2)):(log($3)) w l lt 1,\
 
