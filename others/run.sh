#!/bin/bash
declare -a lambda=("1.e2" "5.e2" "1.e3" "5.e3" "1.e4" "1.e5")
	
for i in {1..5}
do
	gcc -D LAMBDA=${lambda[$i-1]} -I/usr/include/gsl/ -L/usr/lib/ rng.c -lgsl -lcblas -lm -O3 -o exe.$i
	./exe.$i
	gnuplot plot.gp
done
#for i in {1..6}
#do
#	echo "time python rng.py ${lambda[$i-1]}"
#	time python rng.py ${lambda[$i-1]}
#done
